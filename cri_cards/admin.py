from django.contrib import admin

from . import models


@admin.register(models.CardTemplate)
class CardTemplateAdmin(admin.ModelAdmin):
    list_display = ("name", "priority", "template")
    list_display_links = list_display
    search_fields = ("name",)
    filter_horizontal = ("attributes", "groups")


class CardAttributeValueInline(admin.TabularInline):
    model = models.CardAttributeValue
    filter_horizontal = ("groups",)


@admin.register(models.CardAttribute)
class CardAttributeAdmin(admin.ModelAdmin):
    list_display = ("slug", "description", "kind")
    list_display_links = list_display
    search_fields = ("slug", "description")
    list_filter = ("kind",)
    radio_fields = {"kind": admin.HORIZONTAL}
    inlines = (CardAttributeValueInline,)


@admin.register(models.Card)
class CardAdmin(admin.ModelAdmin):
    list_display = ("user", "template", "created_at", "picture")
    list_display_links = list_display
    search_fields = (
        "user__username",
        "user__first_name",
        "user__last_name",
        "user__email",
        "user__uid",
        "template__name",
    )
    list_filter = ("template",)
    readonly_fields = ("user", "template", "picture", "created_at")
    date_hierarchy = "created_at"
