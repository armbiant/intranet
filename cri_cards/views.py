from django.views import generic
from django.forms import formset_factory
from django.http import HttpResponse
from django.contrib import messages
from django.contrib.auth import get_user_model, mixins as auth_mixins

from . import forms
from .generator import CardPicture, Card


class CardIssueFormView(
    auth_mixins.LoginRequiredMixin,
    auth_mixins.PermissionRequiredMixin,
    generic.FormView,
):
    template_name = "cri_cards/issue.html"
    form_class = formset_factory(forms.CardIssueForm, extra=0)
    permission_required = "cri_cards.issue_card"

    def get_initial(self):
        users = get_user_model().objects.filter(
            pk__in=self.request.POST.getlist("user_list", [])
        )
        return [{"user_field": u} for u in users]

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        if "user_list" in self.request.POST:
            del kwargs["data"]
            del kwargs["files"]
        return kwargs

    def form_valid(self, formset):  # pylint: disable=arguments-differ
        if len(formset) == 1:
            try:
                card = formset[0].issue()
            except CardPicture.PictureNotFound:
                messages.error(self.request, "Error while fetching picture.")
                return self.get(self.request)
            except Card.MissingValue as e:
                messages.error(self.request, e)
                return self.get(self.request)

            filename = f"{card.user.fs_name}.png"
            r = HttpResponse(card.picture.read(), content_type="image/png")
            r["Content-Disposition"] = f'attachment; filename="{filename}"'
            return r

        for form in formset:
            form.issue_task(owner=self.request.user)
        messages.info(self.request, "Card issuing tasks has been started.")
        return self.get(self.request)
