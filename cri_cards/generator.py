#!/usr/bin/env python3

import subprocess
from tempfile import NamedTemporaryFile

from io import BytesIO
from base64 import b64encode

import requests
from qrcode import QRCode
from qrcode.image.svg import SvgPathFillImage


class CardPicture:
    class PictureNotFound(RuntimeError):
        pass

    def __init__(self, path=None, url=None, content=None):
        if len(list(filter(None, (path, url, content)))) != 1:
            raise TypeError(
                f"{type(self).__name__}: either path, url or content must be set"
            )
        self.path = path
        self.url = url
        self.content = content
        self._file = None

    def _get_picture_by_url(self):
        r = requests.get(self.url)
        self._file = NamedTemporaryFile(mode="wb")
        if r.status_code == 404:
            raise self.PictureNotFound(self.url)
        if r.status_code != 200:
            raise RuntimeError(f"Error while fetching {self.url}: {r.status_code}")
        self._file.write(r.content)
        self._file.flush()
        return self._file.name

    def cleanup(self):
        if self._file:
            self._file.close()
            self._file = None

    def render(self):
        if self.url:
            self.path = self._get_picture_by_url()
        if self.content:
            self._file = NamedTemporaryFile(mode="wb", delete=False)
            self._file.write(self.content)
            self._file.flush()
            self.path = self._file.name
        return self.path


class CardQRCode:
    def __init__(self, data):
        self.data = data

    def render(self):
        qrcode = QRCode(box_size=50, border=1)
        qrcode.add_data(self.data)
        qrcode.make(fit=True)
        img = qrcode.make_image(image_factory=SvgPathFillImage)

        with BytesIO() as s:
            img.save(s)
            out = b64encode(s.getvalue().split(b"\n")[1]).decode("utf-8")
        return f"data:image/svg+xml;base64,{out}"


class Card:
    class MissingValue(ValueError):
        pass

    def __init__(self, template, data=None):
        if data is None:
            data = {}
        self.template = template
        self.data = data

    def render(self):
        template = self.template
        for field, value in self.data.items():
            if hasattr(value, "render"):
                value = value.render()
            if not value:
                raise self.MissingValue(f"missing value: {field}")
            template = template.replace(f"{{{{ {field} }}}}", value)
        return template

    def cleanup(self):
        for value in self.data.values():
            if hasattr(value, "cleanup"):
                value.cleanup()

    def render_png(self):
        data = self.render()
        with NamedTemporaryFile(mode="wb") as fin:
            fin.write(bytes(data, "utf-8"))
            fin.flush()
            args = ["convert", f"svg:{fin.name}", "png:-"]
            result = subprocess.run(args, stdout=subprocess.PIPE, check=True)
        self.cleanup()
        return result.stdout
