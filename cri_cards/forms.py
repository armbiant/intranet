from django import forms
from django.db import models

from crispy_forms import layout as crispy_layout
from crispy_forms.helper import FormHelper as CrispyFormHelper

from . import models as cards_models
from cri_models import models as cri_models

_ATTR_PREFIX = "attr_"


class CardIssueFormHelper(CrispyFormHelper):
    def __init__(self, *args, fields, **kwargs):
        super().__init__(*args, **kwargs)
        self.layout = crispy_layout.Layout(
            crispy_layout.Row(
                *[
                    crispy_layout.Field(field_name, wrapper_class="mx-2")
                    for field_name in fields
                ]
            )
        )
        self.form_tag = False


class CardIssueForm(forms.Form):
    user_field = forms.ModelChoiceField(
        queryset=cri_models.CRIUser.objects.all(), widget=forms.HiddenInput()
    )

    template = forms.ModelChoiceField(
        queryset=cards_models.CardTemplate.objects.all(),
        empty_label=None,
        required=False,
    )

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.user = self._get_cleaned_value("user_field")
        self.initial.setdefault(
            "template",
            cards_models.CardTemplate.objects.filter(
                models.Q(groups__in=self.user.get_groups()) | models.Q(groups=None)
            )
            .order_by("-priority")
            .first(),
        )
        self.template = self._get_cleaned_value("template")

        if self.template:
            self._add_attributes_fields(self.template.get_choices(self.user))

        self.helper = CardIssueFormHelper(fields=self.fields)

    def _get_cleaned_value(self, field_name):
        if self.is_bound:
            value = self.fields[field_name].widget.value_from_datadict(
                self.data, self.files, self.add_prefix(field_name)
            )
            value = self.fields[field_name].clean(value)
            if not value:
                value = self.initial.get(field_name) or self.fields[field_name].initial
            if field_name.startswith(_ATTR_PREFIX):
                return cards_models.CardAttributeValue.objects.get(pk=value)
            return value
        return self.initial.get(field_name) or self.fields[field_name].initial

    def _add_attributes_fields(self, choices):
        for attribute, values in choices.items():
            field_name = f"{_ATTR_PREFIX}{attribute.slug}"
            kwargs = {
                "label": attribute.slug,
                "help_text": attribute.description,
                "initial": values[0].pk if values else None,
            }
            values_check = list((v.value, v.picture) for v in values)
            if 0 < len(values) < 1 or all(x == values_check[0] for x in values_check):
                self.fields[field_name] = forms.CharField(
                    required=False,
                    disabled=True,
                    widget=forms.widgets.HiddenInput(),
                    **kwargs,
                )
            elif len(values) > 1:
                self.fields[field_name] = forms.ChoiceField(
                    choices=[(v.pk, v.value) for v in values],
                    widget=forms.widgets.RadioSelect,
                    **kwargs,
                )

    def issue(self):
        data = {}
        for key, value in self.cleaned_data.items():
            if key.startswith(_ATTR_PREFIX):
                value = self._get_cleaned_value(key)
                if value:
                    data[value.attribute.slug] = value.get_value(self.user)
        return cards_models.Card.issue(
            template=self.template, user=self.user, values=data
        )

    def issue_task(self, owner):
        data = {}
        for key, value in self.cleaned_data.items():
            if key.startswith(_ATTR_PREFIX):
                value = self._get_cleaned_value(key)
                if value:
                    data[value.attribute] = value
        return cards_models.CardTask.issue(
            owner=owner, template=self.template, user=self.user, values=data
        )
