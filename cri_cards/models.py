from django.db import models
from django.contrib.auth import get_user_model
from django.core.files.base import ContentFile
from django.template.loader import get_template

from itertools import groupby

from . import generator
from cri_models import models as cri_models
from cri_tasks import models as cri_tasks_models

import uuid


class CardAttribute(models.Model):
    MAPPED_PREFIX = "mapped_"

    KIND_CHOICES = (
        ("text", "Text value"),
        ("qrcode", "QR Code"),
        ("picture", "Picture URL"),
        (f"{MAPPED_PREFIX}text", "text value from user data"),
        (f"{MAPPED_PREFIX}qrcode", "QR Code from user data"),
        (f"{MAPPED_PREFIX}picture", "Picture URL from user data"),
    )

    slug = models.SlugField()

    description = models.TextField(blank=True)

    kind = models.CharField(max_length=20, choices=KIND_CHOICES)

    def __str__(self):
        return self.slug


class CardAttributeValue(models.Model):
    attribute = models.ForeignKey(CardAttribute, on_delete=models.CASCADE)

    value = models.CharField(max_length=150, blank=True)

    priority = models.PositiveSmallIntegerField(default=100)

    groups = models.ManyToManyField(cri_models.CRIGroup, blank=True)

    picture = models.FileField(blank=True)

    class Meta:
        ordering = ("attribute", "-priority", "value")

    def __str__(self):
        return f"{self.value} ({self.attribute.kind})"

    def get_value(self, user):
        kind = self.attribute.kind
        value = self.value
        if kind.startswith(CardAttribute.MAPPED_PREFIX):
            value = str(getattr(user, self.value, "") or "")
            kind = kind[len(CardAttribute.MAPPED_PREFIX) :]
        if not self.value and self.picture:
            return generator.CardPicture(content=self.picture.read())
        return {
            "text": lambda v: v,
            "qrcode": generator.CardQRCode,
            "picture": lambda v: generator.CardPicture(url=v),
        }.get(kind, "")(value)


class CardTemplate(models.Model):
    def get_storage_location(self, filename):
        return f"cards/templates/{uuid.uuid4()}/{self.pk}_{filename}"

    name = models.CharField(unique=True, max_length=150)

    slug = models.SlugField(unique=True)

    template = models.FileField(upload_to=get_storage_location)

    attributes = models.ManyToManyField(CardAttribute, blank=True)

    groups = models.ManyToManyField(cri_models.CRIGroup, blank=True)

    priority = models.PositiveSmallIntegerField(default=100)

    class Meta:
        ordering = ("-priority", "name")
        permissions = (("issue_card", "Can issue cards"),)

    def __str__(self):
        return self.name

    def get_choices(self, user):
        values = (
            CardAttributeValue.objects.filter(
                models.Q(groups__in=user.get_groups()) | models.Q(groups=None),
                attribute__cardtemplate=self,
            )
            .distinct()
            .select_related()
        )
        choices = {}
        for attribute, values in groupby(values, key=lambda v: v.attribute):
            filtered_values = []
            max_priority = None
            for v in values:
                if max_priority is None or max_priority < v.priority:
                    max_priority = v.priority
                    filtered_values = [v]
                elif v.priority == max_priority:
                    filtered_values.append(v)
            choices[attribute] = filtered_values
        return choices

    def issue(self, values):
        data = {attr.slug: None for attr in self.attributes.all()}
        data.update(values)
        return generator.Card(
            template=self.template.read().decode("utf-8"), data=data
        ).render_png()

    @staticmethod
    def filename_from_user(user):
        return f"{user.fs_name}.png"


class Card(models.Model):
    user = models.ForeignKey(get_user_model(), on_delete=models.CASCADE)

    created_at = models.DateTimeField(auto_now_add=True)

    template = models.ForeignKey(CardTemplate, on_delete=models.CASCADE)

    picture = models.ImageField()

    @classmethod
    def issue(cls, user, template, values):
        picture = template.issue(values)
        card = cls(user=user, template=template)
        card.picture.save(
            f"cards/user/{uuid.uuid4()}/{user.printable_name}_{user.uid}.png",
            ContentFile(picture),
        )
        card.save()
        return card

    def delete(self, *args, **kwargs):  # pylint: disable=arguments-differ
        self.picture.delete()
        return super().delete(*args, **kwargs)


@cri_tasks_models.CRITask.register_task_class
class CardTask(cri_tasks_models.CRITask):
    class Meta:
        proxy = True

    def __str__(self):
        return self.parameters.get("user_pk")

    def get_result_display(self):
        template = get_template("cri_cards/_cardtask_result.html")
        return template.render(context={"cardtask": self})

    @staticmethod
    def serialize_values(values):
        return {k.pk: v.pk for k, v in values.items()}

    @staticmethod
    def deserialize_values(values, user):
        return {
            CardAttribute.objects.get(pk=k)
            .slug: CardAttributeValue.objects.get(pk=v)
            .get_value(user)
            for k, v in values.items()
        }

    @classmethod
    def issue(cls, owner, user, template, values):
        cls(
            parameters={
                "user_pk": user.pk,
                "template_pk": template.pk,
                "values": cls.serialize_values(values),
            },
            owner=owner,
        ).save()

    # pylint: disable=arguments-differ
    @classmethod
    def run(cls, *_args, user_pk, template_pk, values, **_kwargs):
        user = get_user_model().objects.get(pk=user_pk)
        template = CardTemplate.objects.get(pk=template_pk)
        try:
            return Card.issue(user, template, cls.deserialize_values(values, user))
        except generator.CardPicture.PictureNotFound as e:
            raise cls.TaskError(f"Picture not found: {e}")
