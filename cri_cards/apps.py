from django.apps import AppConfig


class CriCardsConfig(AppConfig):
    name = "cri_cards"
