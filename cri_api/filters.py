from django.db.models import Q
from django.utils import timezone

from django_filters import rest_framework as filters


class NumberInFilter(filters.BaseInFilter, filters.NumberFilter):
    pass


class CharInFilter(filters.BaseInFilter, filters.CharFilter):
    pass


class CRIUserFilter(filters.FilterSet):
    logins = CharInFilter(label="Logins", field_name="username")

    uids = NumberInFilter(label="UIDs", field_name="uid")

    emails = CharInFilter(label="Emails", field_name="email")

    first_names = CharInFilter(label="First names", field_name="first_name")

    last_names = CharInFilter(label="Last names", field_name="last_name")

    graduation_years = NumberInFilter(
        label="Graduation years",
        field_name="memberships__graduation_year",
        method="group_filtering",
    )

    groups = CharInFilter(
        label="Groups",
        field_name="memberships__group__slug",
        method="group_filtering",
    )

    @staticmethod
    def group_filtering(queryset, name, value):
        return queryset.filter(
            Q(memberships__end_at__gte=timezone.now())
            | Q(memberships__end_at__isnull=True),
            Q(memberships__begin_at__lte=timezone.now())
            | Q(memberships__begin_at__isnull=True),
            **{f"{name}__in": value},
        )
