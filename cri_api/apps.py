from django.apps import AppConfig


class CriApiConfig(AppConfig):
    name = "cri_api"
