from django.urls import include, path

from rest_framework import routers

from . import views

router = routers.DefaultRouter()
router.register("campus", views.CampusViewSet)
router.register("groups", views.CRIGroupViewSet)
router.register("users", views.CRIUserViewSet)

urlpatterns = [
    path(
        "groups/<slug:slug>/members/",
        views.CRIGroupMembersView.as_view(),
        name="crigroup-members",
    ),
    path(
        "groups/<slug:slug>/history/",
        views.CRIGroupHistoryView.as_view(),
        name="crigroup-history",
    ),
    path("cards/", views.CRICardsView.as_view(), name="cricards-create"),
    path("", include(router.urls)),
]
