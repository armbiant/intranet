from rest_framework.permissions import BasePermission


class CanIssueCards(BasePermission):
    def has_permission(self, request, view=None):
        return request.user.has_perm("cri_cards.issue_card")


class CanViewRelatedAccounts(BasePermission):
    def has_permission(self, request, view=None):
        return request.user.has_perm("view_criuser_legal_identity")


class CanViewLegalIdentity(BasePermission):
    def has_permission(self, request, view=None):
        return request.user.has_perm("view_criuser_legal_identity")


class CanViewPhone(BasePermission):
    def has_permission(self, request, view=None):
        return request.user.has_perm("view_criuser_phone")


class CanViewBirthdate(BasePermission):
    def has_permission(self, request, view=None):
        return request.user.has_perm("view_criuser_birthdate")
