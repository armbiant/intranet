from django.contrib.auth import get_user_model

from rest_framework import serializers

from cri_cards import (
    models as cri_cards_models,
    forms as cri_cards_forms,
    generator,
)


class CardGenerationSerializer(serializers.Serializer):
    login = serializers.CharField(write_only=True)

    template = serializers.SlugField(write_only=True, required=False)

    values = serializers.DictField(write_only=True, required=False, allow_empty=True)

    url = serializers.URLField(read_only=True)

    def save(self, **kwargs):
        try:
            card = self._form.issue()
        except generator.CardPicture.PictureNotFound as e:
            raise serializers.ValidationError({"error": f"picture not found: {e}"})
        except generator.Card.MissingValue as e:
            raise serializers.ValidationError({"error": e})
        self._validated_data["url"] = card.picture.url
        return card

    def validate_login(self, value):
        User = get_user_model()
        try:
            return User.objects.get(username=value)
        except User.DoesNotExist:
            raise serializers.ValidationError(f"user {value} not found")

    def validate_template(self, value):
        try:
            return cri_cards_models.CardTemplate.objects.get(slug=value)
        except cri_cards_models.CardTemplate.DoesNotExist:
            raise serializers.ValidationError(f"template {value} not found")

    def is_valid(self, raise_exception=False):
        if super().is_valid(raise_exception):
            data = {
                "user_field": self.validated_data["login"].pk,
            }
            if "template" in self.validated_data:
                data["template"] = self.validated_data["template"].pk
            for k, v in self.validated_data.get("values", {}).items():
                data[f"attr_{k}"] = v
            self._form = cri_cards_forms.CardIssueForm(data)
            if self._form.is_valid():
                return True
            self._errors["values"] = "unable to issue cards from values"
        if raise_exception:
            raise serializers.ValidationError(self.errors)
        return False
