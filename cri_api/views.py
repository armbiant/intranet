from django.shortcuts import get_object_or_404
from django.db.models import Q

from rest_framework import viewsets, permissions, generics
from rest_framework.decorators import action
from rest_framework.response import Response

from cri_models import models
from cri_cards import models as cri_cards_models

from . import filters, serializers, permissions as cri_api_permissions
from .serializers import cards as serializers_cards


class DynamicSerializerClassMixin:
    def get_serializer_class(self):
        """
        A class which inhertis this mixins should have variable
        `serializer_action_classes`.
        Look for serializer class in self.serializer_action_classes, which
        should be a dict mapping action name (key) to serializer class (value),
        i.e.:
        class SampleViewSet(viewsets.ViewSet):
            serializer_class = DocumentSerializer
            serializer_action_classes = {
               'upload': UploadDocumentSerializer,
               'download': DownloadDocumentSerializer,
            }
            @action
            def upload:
                ...
        If there's no entry for that action then just fallback to the regular
        get_serializer_class lookup: self.serializer_class, DefaultSerializer.
        """
        try:
            return self.serializer_action_classes[self.action]
        except (KeyError, AttributeError):
            return super().get_serializer_class()


class CRIGroupHistoryView(generics.ListAPIView):
    queryset = models.CRIGroup.objects.all()
    real_queryset = models.CRIComputedMembership.objects.all()
    serializer_class = serializers.CRIComputedMembershipSerializer
    lookup_field = "slug"
    lookup_url_kwarg = "slug"

    def get_queryset(self):
        if not self.kwargs:
            return self.queryset
        return self.real_queryset

    def get_object(self):
        kwargs = {self.lookup_field: self.kwargs[self.lookup_url_kwarg]}
        user = self.request.user
        if user.has_perm("cri_models.view_crigroup"):
            group_filter = []
        else:
            group_filter = [
                Q(private=False) | Q(pk__in=[g.pk for g in user.get_groups()])
            ]
        return get_object_or_404(models.CRIGroup, *group_filter, **kwargs)

    def filter_queryset(self, queryset):
        return queryset.filter(group=self.get_object())

    def get_serializer(self, *args, **kwargs):
        kwargs["context"] = self.get_serializer_context()
        kwargs.setdefault("excluded_fields", []).append("user")
        return self.get_serializer_class()(*args, **kwargs)


class CRIGroupMembersView(CRIGroupHistoryView):
    real_queryset = models.CRIUser.objects.all()
    serializer_class = serializers.SimpleCRIUserSerializer

    def filter_queryset(self, queryset):
        group = self.get_object()
        return queryset.filter(pk__in=[u.pk for u in group.get_members()])

    def get_serializer(self, *args, **kwargs):
        kwargs["context"] = self.get_serializer_context()
        return self.get_serializer_class()(*args, **kwargs)


class CampusViewSet(viewsets.ModelViewSet):
    queryset = models.Campus.objects.all()
    serializer_class = serializers.CampusSerializer
    lookup_field = "group__slug"
    lookup_url_kwarg = "slug"


class CRIGroupViewSet(viewsets.ModelViewSet):
    queryset = models.CRIGroup.objects.all()
    serializer_class = serializers.CRIGroupSerializer
    filterset_fields = ("kind", "private")
    lookup_field = "slug"

    def filter_queryset(self, queryset):
        queryset = super().filter_queryset(queryset)
        user = self.request.user
        if user.has_perm("cri_models.view_crigroup"):
            return queryset

        return queryset.filter(
            Q(private=False) | Q(pk__in=[g.pk for g in user.get_groups()])
        )


class CRIUserViewSet(DynamicSerializerClassMixin, viewsets.ModelViewSet):
    queryset = models.CRIUser.objects.all()
    serializer_class = serializers.CRIUserSerializer
    serializer_action_classes = {"create": serializers.CRIUserCreateSerializer}
    filterset_class = None
    lookup_field = "username"
    lookup_url_kwarg = "login"
    lookup_value_regex = "[^/]+"

    @action(
        detail=False,
        methods=["get"],
        permission_classes=[permissions.IsAuthenticated],
        serializer_class=serializers.ProfileSerializer,
        filter_backends=(),
        pagination_class=None,
    )
    def me(self, request, **_kwargs):
        serializer = serializers.ProfileSerializer(
            request.user, context={"request": request}
        )
        return Response(serializer.data)

    @action(
        detail=False,
        methods=["get"],
        filterset_class=filters.CRIUserFilter,
    )
    def search(self, request, **_kwargs):
        filtered_qs = self.filterset_class(  # pylint: disable=not-callable
            request.GET, queryset=models.CRIUser.objects.all()
        ).qs

        page = self.paginate_queryset(filtered_qs)
        if page is not None:
            serializer = serializers.CRIUserSerializer(
                page, context={"request": request}, many=True
            )
            return self.get_paginated_response(serializer.data)
        serializer = serializers.CRIUserSerializer(
            filtered_qs, context={"request": request}, many=True
        )
        return Response(serializer.data)


class CRICardsView(generics.CreateAPIView):
    queryset = cri_cards_models.Card.objects.all()
    serializer_class = serializers_cards.CardGenerationSerializer
    permission_classes = [cri_api_permissions.CanIssueCards]
