from django.conf import settings
from django.contrib import admin, messages
from django.utils.html import mark_safe
from django.urls import reverse
from django.contrib.auth import admin as auth_admin, models as auth_models

from import_export import admin as ie_admin

from . import models
from . import import_export


if settings.DEBUG:
    from . import ldap_admin  # noqa


class CRIUserInline(admin.TabularInline):
    model = auth_models.Group.user_set.through
    autocomplete_fields = ("criuser",)
    readonly_fields = ("is_staff", "is_active")
    extra = 0

    @admin.display(boolean=True)
    def is_staff(self, obj):
        return obj.criuser.is_staff

    @admin.display(boolean=True)
    def is_active(self, obj):
        return obj.criuser.is_active


auth_admin.GroupAdmin.inlines = (CRIUserInline,)


class CRIMembershipInline(admin.TabularInline):
    model = models.CRIMembership
    extra = 0
    autocomplete_fields = ("group", "user")


class KerberosPrincipalInline(admin.TabularInline):
    model = models.KerberosPrincipal
    extra = 0


class SSHPublicKeyInline(admin.TabularInline):
    model = models.SSHPublicKey
    extra = 0

    def get_readonly_fields(self, request, obj=None):
        ro_fields = super().get_readonly_fields(request, obj)
        if obj:
            return ("key_type", "key_length", *ro_fields)
        return ro_fields


@admin.register(models.CRIUser)
class CRIAccountAdmin(import_export.ExportMixin, auth_admin.UserAdmin):
    resource_class = import_export.CRIUserMoodleResource
    fieldsets = (
        (auth_admin.UserAdmin.fieldsets[0],)
        + (
            (
                "Account",
                {
                    "fields": (
                        "uid",
                        "ldap_dn",
                        "primary_group",
                        "primary_principal",
                        "old_account",
                    )
                },
            ),
            (
                auth_admin.UserAdmin.fieldsets[1][0],
                {
                    "fields": (
                        "first_name",
                        "last_name",
                        "legal_first_name",
                        "legal_last_name",
                        "birthdate",
                        "phone",
                    )
                },
            ),
            (
                "Emails",
                {"fields": ("email", "mail_aliases", "group_mail_aliases")},
            ),
        )
        + auth_admin.UserAdmin.fieldsets[2:]
    )
    add_form = auth_admin.UserAdmin.form
    add_fieldsets = (
        (None, {"fields": ("username",)}),
        ("Account", {"fields": ("uid", "primary_group", "old_account")}),
    ) + (fieldsets[2], fieldsets[3])
    inlines = (
        CRIMembershipInline,
        KerberosPrincipalInline,
        SSHPublicKeyInline,
    )
    autocomplete_fields = ("primary_group", "primary_principal", "old_account")
    filter_horizontal = ("mail_aliases",) + auth_admin.UserAdmin.filter_horizontal
    readonly_fields = ("group_mail_aliases",)

    # pylint: disable=arguments-differ
    def get_queryset(self, *args, **kwargs):
        qs = super().get_queryset(*args, **kwargs)
        return qs.filter(criserviceaccount=None)

    def get_readonly_fields(self, request, obj=None):
        ro_fields = super().get_readonly_fields(request, obj)
        if obj:
            return ("username", *ro_fields)
        return ro_fields

    @admin.display
    def group_mail_aliases(self, obj):
        TPL = '{alias} (<a href="{url}" title="{title}">{text}</a>)'
        data = []
        for membership in obj.memberships.exclude(group__mail_aliases=None):
            for alias in membership.group.mail_aliases.all():
                data.append(
                    TPL.format(
                        alias=alias.email,
                        url=reverse(
                            "admin:cri_models_crigroup_change",
                            args=(membership.group.pk,),
                        ),
                        text=membership.group.slug,
                        title=str(membership.group),
                    ),
                )
        return mark_safe(r"<br \>".join(data))


@admin.register(models.CRIServiceAccount)
class CRIServiceAccountAdmin(CRIAccountAdmin):
    list_display = ("owner", "username", "email", "is_active")

    fieldsets = (
        (
            None,
            {
                "fields": (
                    "owner",
                    "username",
                    "password",
                ),
            },
        ),
        (
            "Account",
            {
                "fields": (
                    "uid",
                    "ldap_dn",
                    "primary_group",
                    "primary_principal",
                )
            },
        ),
        (
            auth_admin.UserAdmin.fieldsets[1][0],
            {
                "classes": ("collapse",),
                "fields": (
                    "first_name",
                    "last_name",
                    "legal_first_name",
                    "legal_last_name",
                    "birthdate",
                    "phone",
                ),
            },
        ),
        (
            "Emails",
            {"fields": ("email", "mail_aliases", "group_mail_aliases")},
        ),
    ) + auth_admin.UserAdmin.fieldsets[2:]

    add_fieldsets = (
        (None, {"fields": ("owner",)}),
        ("Account", {"fields": ("username", "uid", "primary_group", "last_name")}),
    ) + (CRIAccountAdmin.fieldsets[3],)

    autocomplete_fields = CRIAccountAdmin.autocomplete_fields + ("owner",)
    readonly_fields = CRIAccountAdmin.readonly_fields + ("is_staff", "is_superuser")

    def get_queryset(self, *args, **kwargs):
        # pylint: disable=bad-super-call
        return super(CRIAccountAdmin, self).get_queryset(*args, **kwargs)


@admin.register(models.MailAlias)
class MailAliasAdmin(admin.ModelAdmin):
    list_display = ("email", "users", "groups")
    list_display_links = list_display
    search_fields = ("email", "criuser__username", "crigroup__slug", "crigroup__name")

    def get_readonly_fields(self, request, obj=None):
        ro_fields = tuple(super().get_readonly_fields(request, obj))
        if obj:
            ro_fields += ("users_with_links", "groups_with_links")
        else:
            ro_fields += ("users", "groups")
        return ro_fields

    @admin.display(description="users")
    def users_with_links(self, obj):
        TPL = '<a href="{url}">{text}</a>'
        return mark_safe(
            ", ".join(
                TPL.format(
                    url=reverse("admin:cri_models_criuser_change", args=(u.pk,)),
                    text=str(u),
                )
                for u in obj.criuser_set.all()
            )
        )

    @admin.display(description="groups")
    def groups_with_links(self, obj):
        TPL = '<a href="{url}" title="{title}">{text}</a>'
        return mark_safe(
            ", ".join(
                TPL.format(
                    url=reverse("admin:cri_models_crigroup_change", args=(g.pk,)),
                    title=str(g),
                    text=g.slug,
                )
                for g in obj.crigroup_set.all()
            )
        )

    @admin.display
    def users(self, obj):
        return ", ".join(map(str, obj.criuser_set.all()))

    @admin.display
    def groups(self, obj):
        return ", ".join(obj.crigroup_set.all().values_list("slug", flat=True))


@admin.register(models.KerberosPrincipal)
class KerberosPrincipalAdmin(admin.ModelAdmin):
    list_display = ("principal", "user", "out_of_date")
    list_display_links = list_display
    list_filter = ("out_of_date",)
    search_fields = ("principal", "user__username")
    autocomplete_fields = ("user",)


@admin.register(models.SSHPublicKey)
class SSHPublicKeyAdmin(admin.ModelAdmin):
    list_display = ("user", "title", "key_type", "key_length")
    list_display_links = list_display
    list_filter = ("key_type", "key_length")
    search_fields = ("user__username", "title", "key")
    autocomplete_fields = ("user",)

    def get_readonly_fields(self, request, obj=None):
        ro_fields = super().get_readonly_fields(request, obj)
        if obj:
            return ("key_type", "key_length", *ro_fields)
        return ro_fields


@admin.register(models.Campus)
class CampusAdmin(admin.ModelAdmin):
    list_display = ("group",)
    list_display_links = list_display
    search_fields = ("group__name", "group__slug")


@admin.register(models.CRIGroup)
class CRIGroupAdmin(admin.ModelAdmin):
    list_display = ("name", "slug", "kind", "gid", "private")
    list_display_links = list_display
    list_filter = ("private", "kind")
    search_fields = ("name", "slug", "kind", "gid")
    autocomplete_fields = ("next_group",)
    readonly_fields = ("get_computed_parents", "computed_children", "get_ldap_dn")
    radio_fields = {"kind": admin.HORIZONTAL}
    filter_horizontal = ("children", "managers", "mail_aliases")

    # We can't directly use the ldap_dn property since it does not support
    # setting the short_description attribute.
    @admin.display(description="LDAP DN")
    def get_ldap_dn(self, obj):
        return obj.ldap_dn

    # We can't directly use the computed_parent since Django does not support
    # displaying related manager as ordinary fields.
    @admin.display(description="Computed parents")
    def get_computed_parents(self, obj):
        return ", ".join(map(str, obj.computed_parents.all()))


@admin.register(models.CRIMembership)
class CRIMembershipAdmin(admin.ModelAdmin):
    list_display = ("group", "user", "graduation_year", "begin_at", "end_at")
    list_display_links = list_display
    search_fields = ("group__name", "user__username")
    list_filter = ("graduation_year",)
    date_hierarchy = "begin_at"


@admin.register(models.CRIComputedMembership)
class CRIComputedMembershipAdmin(CRIMembershipAdmin):
    def has_add_permission(self, request):
        return False

    def has_delete_permission(self, request, obj=None):
        return False

    def has_change_permission(self, request, obj=None):
        return False


@admin.register(models.CRIUserCreationRequest)
class CRIUserCreationRequestAdmin(ie_admin.ImportExportModelAdmin):
    resource_class = import_export.CRIUserCreationRequestResource
    list_display = (
        "created_user",
        "username",
        "email",
        "first_name",
        "last_name",
        "primary_group",
        "uid",
    )
    list_display_links = list_display
    list_filter = ("primary_group", ("created_at", admin.EmptyFieldListFilter))
    autocomplete_fields = ("primary_group", "old_account")
    search_fields = ("username", "email", "first_name", "last_name", "uid")
    actions = ["create"]

    def get_readonly_fields(self, request, obj=None):
        ro_fields = []
        if obj:
            if obj.created_at:
                fields = obj._meta.get_fields()
                ro_fields.extend(
                    f.name for f in fields if f.editable and f.name != "id"
                )
            ro_fields.extend(["created_user_with_link", "created_at", "created_by"])

        return ro_fields + list(super().get_readonly_fields(request, obj))

    @admin.display(description="Created user")
    def created_user_with_link(self, obj):
        TPL = '<a href="{url}">{text}</a>'
        return mark_safe(
            TPL.format(
                url=reverse(
                    "admin:cri_models_criuser_change", args=(obj.created_user.pk,)
                ),
                text=str(obj.created_user),
            )
        )

    def view_on_site(self, obj):
        if obj.created_user:
            return obj.created_user.get_absolute_url()
        return None

    def has_create_permission(self, request):
        return request.user.has_perm("cri_models.add_criuser")

    def has_delete_permission(self, request, obj=None):
        if obj and obj.created_at:
            return False
        return super().has_delete_permission(request, obj)

    def create(self, request, queryset):
        for obj in queryset:
            try:
                user = obj.create(creator=request.user)
                messages.add_message(request, messages.INFO, f"User created: {user}")
            except models.CRIUserCreationRequest.CreationFailed as e:
                messages.add_message(
                    request, messages.ERROR, f"Unable to create user: {e}"
                )

    create.short_description = "Create selected users"
    create.allowed_permissions = ("create",)


@admin.register(models.CRIUserNoteScope)
class CRIUserNoteScope(admin.ModelAdmin):
    filter_horizontal = ("readers", "writers")
    search_fields = ("name",)


@admin.register(models.CRIUserNoteLabel)
class CRIUserNoteLabel(admin.ModelAdmin):
    pass


@admin.register(models.CRIUserNote)
class CRIUserNote(admin.ModelAdmin):
    list_display = (
        "user",
        "author",
        "event_title",
        "event_kind",
        "event_date",
        "created_at",
        "updated_at",
        "scope",
        "secret",
    )
    list_display_links = list_display
    list_filter = ("event_kind", "scope", "labels", "secret")
    date_hierarchy = "created_at"
    autocomplete_fields = ("user", "author", "scope")
    filter_horizontal = ("labels",)
    radio_fields = {"event_kind": admin.HORIZONTAL}
    readonly_fields = ("created_at", "updated_at", "updated_by")
