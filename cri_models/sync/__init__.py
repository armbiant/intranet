"""Model synchronization module

This module offers various classes to deal with Djando model
synchronization.  The end goal is to easily synchronize model instances
with other data store (LDAP, Kerberos, ...).

To achieve this, we have three kind of classes:

  1. Synchronization engines, all of them are subclasses of
     ``SyncEngine``. They can tell which objects need to be added,
     updated or deleted and how to perform these actions.

  2. Synchronized model adapters. Each of these classes describe how a
     model can be synchronized with a specific data store.

  3. A model mixin. This mixin is intended to be added to the Django
     models you want to synchronize. It will enumerate and call the
     relevant adapters.

All operations are based on dictionary representing the data of a
specific object. Comparison between objects are done by looking at their
respective dictionaries.

A synchronization engine is given two mappings between objects unique id
and their dictionnaries, one for the *local objects* and one for the
*remote objects*. *Local_objects* are simply the one from which the
synchronization is initiated. It is also given a mapping between local
objects IDs and remote objects IDs to know how to associate them.
"""

from .engine import SyncEngine, UnidirectionalSyncEngine
from .modelengine import (
    SyncRemoteModelEngine,
    SyncedModelAdapter,
    SyncedModelMixin,
)
from .ldapmodelengine import SyncedLDAPModelAdapter
from .kerberosmodelengine import SyncedKerberosPrincipalAdapter

__all__ = (
    "SyncEngine",
    "UnidirectionalSyncEngine",
    "SyncRemoteModelEngine",
    "SyncedModelMixin",
    "SyncedModelAdapter",
    "SyncedLDAPModelAdapter",
    "SyncedKerberosPrincipalAdapter",
)
