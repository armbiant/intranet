from django.db import models

from import_export import resources, fields, admin, widgets

from . import models as cri_models


class ExportMixin(admin.ExportMixin):
    def get_resource_kwargs(self, request, *args, **kwargs):
        return {"request": request}


class CRIUserResource(resources.ModelResource):
    username = fields.Field(attribute="username", readonly=True)
    email = fields.Field(attribute="email", readonly=True)
    gid = fields.Field(attribute="primary_group__gid")

    class Meta:
        model = cri_models.CRIUser
        import_id_fields = ["username", "uid"]
        skip_unchanged = True
        report_skipped = True
        fields = (
            "first_name",
            "last_name",
            "legal_first_name",
            "legal_last_name",
            "uid",
            "phone",
            "birthdate",
        )
        export_order = (
            "username",
            "email",
            "first_name",
            "last_name",
            "legal_first_name",
            "legal_last_name",
            "uid",
            "gid",
            "phone",
            "birthdate",
        )
        export_permission_map = {
            "legal_first_name": "cri_models.view_criuser_legal_identity",
            "legal_last_name": "cri_models.view_criuser_legal_identity",
            "phone": "cri_models.view_criuser_phone",
            "birthdate": "cri_models.view_criuser_birthdate",
        }

    def __init__(self, request, *args, **kwargs):
        self.request = request
        super().__init__(*args, **kwargs)

    def get_export_fields(self):
        fields = []
        for field_name in self.get_export_order():
            perm_name = self.Meta.export_permission_map.get(field_name)
            if not perm_name or self.request.user.has_perm(perm_name):
                fields.append(self.fields[field_name])
        return fields


class CRIUserMoodleResource(CRIUserResource):
    class Meta(CRIUserResource.Meta):
        fields = ("first_name", "last_name", "uid")
        export_order = ("username", "email", "first_name", "last_name", "uid")
        headers_map = {
            "first_name": "firstname",
            "last_name": "lastname",
            "uid": "idnumber",
        }

    def __init__(self, *args, **kwargs):
        self._membership_cache = {}
        super().__init__(*args, **kwargs)

    def get_export_order(self):
        return self.Meta.export_order

    def membership_to_cohort_name(self, membership):
        cohort_name = membership.group.slug
        if membership.graduation_year:
            return f"{membership.graduation_year}_{cohort_name}"
        return cohort_name

    def get_cohort_name(self, row, idx):
        if row[0] not in self._membership_cache:
            memberships = cri_models.CRIComputedMembership.objects.filter(
                user__username=row[0], group__private=False
            )
            self._membership_cache[row[0]] = memberships
        else:
            memberships = self._membership_cache[row[0]]
        if idx >= len(memberships):
            return ""
        return self.membership_to_cohort_name(memberships[idx])

    def after_export(self, queryset, data, *args, **kwargs):
        count = (
            cri_models.CRIComputedMembership.objects.filter(
                user__pk__in=queryset.values_list("pk", flat=True),
                group__private=False,
            )
            .values("user")
            .annotate(gcount=models.Count("group"))
            .aggregate(models.Max("gcount"))
            .get("gcount__max", 99)
        ) or 0

        data.headers = [self.Meta.headers_map.get(h, h) for h in data.headers]
        if queryset:
            data.append_col(lambda _: "oauth2", "auth")
            for i in range(count):
                generator = lambda row, idx=i: self.get_cohort_name(row, idx)
                data.append_col(generator, f"cohort{i+1}")


class CRIUserCreationRequestResource(resources.ModelResource):
    primary_group = fields.Field(
        attribute="primary_group__slug",
        column_name="primary_group",
        widget=widgets.ForeignKeyWidget(cri_models.CRIGroup, "slug"),
    )

    class Meta:
        model = cri_models.CRIUserCreationRequest
        import_id_fields = ["username"]
        skip_unchanged = True
        report_skipped = True
        force_init_instance = True
        use_bulk = True
        exclude = (
            "id",
            "created_user",
            "created_at",
            "created_by",
        )
