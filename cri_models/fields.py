from django.db import models
from django.core.exceptions import ValidationError
from django.core.validators import MinValueValidator, MaxValueValidator

from sshpubkeys import SSHKey, InvalidKeyError, InvalidOptionsError


class GraduationYearField(models.PositiveIntegerField):
    def __init__(self, *args, **kwargs):
        validators = kwargs.setdefault("validators", [])
        v = MinValueValidator(1989)
        if v not in validators:
            validators.append(v)
        super().__init__(*args, **kwargs)


class UnixIDField(models.PositiveIntegerField):
    def __init__(self, *args, **kwargs):
        validators = kwargs.setdefault("validators", [])
        for v in (MinValueValidator(0), MaxValueValidator(65535)):
            if v not in validators:
                validators.append(v)
        super().__init__(*args, **kwargs)


class SSHPubKey(SSHKey):
    def __str__(self):
        return self.keydata

    @property
    def key_without_comment(self):
        return " ".join(self.keydata.split(None, 2)[:2])

    @classmethod
    def clean(cls, value, disallow_options=False):
        if isinstance(value, SSHKey):
            value = value.keydata
        try:
            key = cls(value, disallow_options=disallow_options)
            key.parse()
        except NotImplementedError:
            raise ValidationError(f"Unsupported SSH public key type: {key.key_type}")
        except InvalidOptionsError:
            raise ValidationError(f"Invalid SSH public key option: {key.options_raw}")
        except InvalidKeyError as e:
            raise ValidationError(f"Invalid SSH public key: {e}")
        return key


class SSHPublicKeyField(models.TextField):
    def sshpubkey_validator(self, value):
        return SSHPubKey.clean(value, disallow_options=not self.allow_options)

    def __init__(self, *args, **kwargs):
        validators = kwargs.setdefault("validators", [])
        validators.append(self.sshpubkey_validator)
        self.allow_options = kwargs.pop("allow_options", False)
        super().__init__(*args, **kwargs)

    def from_db_value(self, value, _expression, _connection):
        return self.to_python(value)

    def get_prep_value(self, value):
        if not value:
            return ""
        return value.keydata

    def to_python(self, value):
        return self.sshpubkey_validator(value)
