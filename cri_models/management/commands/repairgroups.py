from django.core.management.base import BaseCommand
from django.db import transaction

from ...models import CRIGroup, CRIComputedMembership


class Command(BaseCommand):
    help = "Repair group membership caches"

    def add_arguments(self, parser):
        parser.add_argument("groups", nargs="*", type=str)

    def handle(self, *args, **options):
        slugs = options.get("groups", [])
        if not slugs:
            self.stdout.write("Recomputing all group memberhips... ", ending="")
            groups = CRIGroup.objects.all()
        else:
            groups = CRIGroup.objects.filter(slug__in=slugs)
            missings = set(slugs) - set(g.slug for g in groups)
            for m in missings:
                self.stderr.write(f"Groups not found: m")
            if missings:
                return
            self.stdout.write("Recomputing group memberhips... ", ending="")
        with transaction.atomic():
            CRIComputedMembership.compute_entries(groups=groups)
        self.stdout.write("OK")
