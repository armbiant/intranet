from django.conf import settings


class AlgoliaModelMixin:
    def type(self):
        return super()._meta.model_name

    def url(self):
        return self.get_absolute_url()


class CRIIndexMixin:
    @classmethod
    def get_index_name(cls):
        algolia_settings = settings.ALGOLIA
        return "_".join(
            filter(
                None,
                (
                    algolia_settings.get("INDEX_PREFIX"),
                    cls.index_name,
                    algolia_settings.get("INDEX_SUFFIX"),
                ),
            )
        )
