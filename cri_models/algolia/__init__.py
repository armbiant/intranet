from urllib.parse import urlencode
from operator import itemgetter

import base64
import hashlib
import hmac
import json


def _serialize(query_parameters):
    for key, value in query_parameters.items():
        if isinstance(value, (list, dict)):
            value = json.dumps(value)
        elif isinstance(value, bool):
            value = "true" if value else "false"

        query_parameters[key] = value

    return urlencode(sorted(query_parameters.items(), key=itemgetter(0)))


def generate_secured_api_key(parent_api_key, restrictions):
    query_parameters = _serialize(restrictions)

    secured_key = hmac.new(
        parent_api_key.encode("utf-8"),
        query_parameters.encode("utf-8"),
        hashlib.sha256,
    ).hexdigest()

    base64encoded = base64.b64encode(
        ("{}{}".format(secured_key, query_parameters)).encode("utf-8")
    )

    return str(base64encoded.decode("utf-8"))
