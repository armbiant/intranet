from django.conf import settings
from django.utils.timezone import now

from algoliasearch_django import AlgoliaIndex
from algoliasearch_django.decorators import register
from datetime import timedelta

from .. import models as cri_models
from . import generate_secured_api_key
from .base import AlgoliaModelMixin, CRIIndexMixin


class CRIUserProxy(AlgoliaModelMixin, cri_models.CRIUser):
    class Meta:
        proxy = True

    def graduation_years(self):
        return list(
            cri_models.CRICurrentComputedMembership.objects.filter(
                group__in=self.get_groups(), graduation_year__isnull=False
            )
            .distinct("graduation_year")
            .order_by("graduation_year")
            .values_list("graduation_year", flat=True)
        )

    def picture(self):
        return f"https://photos.cri.epita.fr/square/{self.username}"

    def name(self):
        return self.get_full_name()

    def old_accounts(self):
        return list(self.get_old_accounts())

    def has_new_account(self):
        return bool(self.get_new_account())


@register(CRIUserProxy)
class CRIUserIndex(CRIIndexMixin, AlgoliaIndex):
    index_name = "users"
    fields = (
        "username",
        "name",
        "first_name",
        "last_name",
        "email",
        "uid",
        "graduation_years",
        "picture",
        "type",
        "url",
        "old_accounts",
        "has_new_account",
    )
    settings = {
        "searchableAttributes": [
            "username",
            "first_name",
            "last_name",
            "email",
            "uid",
            "old_accounts",
        ],
        "attributesForFaceting": [
            "graduation_years",
            "filterOnly(type)",
            "filterOnly(has_new_account)",
        ],
    }


class CRIGroupProxy(AlgoliaModelMixin, cri_models.CRIGroup):
    class Meta:
        proxy = True

    def members(self):
        if not self.private:
            return []

        members = set(m.pk for m in self.get_members())
        members |= set(m.pk for m in self.get_managers())
        return list(members)


@register(CRIGroupProxy)
class CRIGroupIndex(CRIIndexMixin, AlgoliaIndex):
    index_name = "groups"
    fields = (
        "slug",
        "name",
        "gid",
        "kind",
        "private",
        "members",
        "type",
        "url",
    )
    settings = {
        "searchableAttributes": ["slug", "name", "gid"],
        "attributesForFaceting": [
            "kind",
            "private",
            "filterOnly(members)",
            "filterOnly(type)",
        ],
    }


def get_user_search_filter(user):
    filters = []

    if user.is_anonymous:
        filters.append("private:false OR NOT type:crigroup")

    if not user.has_perm("cri_models.view_crigroup"):
        filters.append(f"(private:false OR members:{user.pk}) OR NOT type:crigroup")

    if not user.has_perm("cri_models.view_criuser"):
        filters.append("has_new_account:false OR NOT type:criuser")

    if len(filters) == 1:
        return filters[0]

    return " AND ".join(f"({f})" for f in filters)


def get_user_search_key(user):
    if user.is_anonymous:
        return None

    indices = [
        (CRIUserIndex.get_index_name(), "Users"),
        (CRIGroupIndex.get_index_name(), "Groups"),
    ]

    restrictions = {
        "validUntil": int((now() + timedelta(days=1)).timestamp()),
        "userToken": user.username,
        "restrictIndices": list([i[0] for i in indices]),
        "filters": get_user_search_filter(user),
    }

    return generate_secured_api_key(
        settings.ALGOLIA.get("SEARCH_API_KEY"), restrictions
    )
