from ldapdb.models import fields

from .models import LDAPModel


class LDAPOrganizationalUnit(LDAPModel):
    # LDAP metadata
    object_classes = ("organizationalUnit",)

    classes = fields.ListField(
        db_column="objectClass",
        editable=False,
        verbose_name="Object class list",
        default=object_classes,
    )

    # organizationalUnit attributes
    name = fields.CharField(db_column="ou", max_length=200, primary_key=True)

    class Meta:
        verbose_name = "LDAP organizational unit"
