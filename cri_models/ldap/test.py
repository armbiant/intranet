from django.test import TestCase, override_settings
from django.conf import settings  # pylint: disable=unused-import

from .. import ldap


class LDAPTestCase(TestCase):
    TEST_OU_NAME = "tests"
    TEST_OU_CHILDREN = ("users", "groups", "services")

    @classmethod
    def _add_databases_failures(cls):
        pass

    @classmethod
    def _remove_databases_failures(cls):
        pass

    @classmethod
    def setUpClass(cls):
        ou_model = ldap.LDAPOrganizationalUnit
        cls.test_ou, _created = ou_model.objects.get_or_create(name=cls.TEST_OU_NAME)
        with override_settings(LDAP_BASE_DN=f"ou=tests,{settings.LDAP_BASE_DN}"):
            try:
                for ou_name in cls.TEST_OU_CHILDREN:
                    ou_model.objects.get_or_create(name=ou_name)
            except:  # noqa: E722
                cls.test_ou.delete_subtree()
                raise
            super().setUpClass()

    @classmethod
    def tearDownClass(cls):
        super().tearDownClass()
        cls.test_ou.delete_subtree()

    @override_settings(LDAP_BASE_DN=f"ou=tests,{settings.LDAP_BASE_DN}")
    # pylint: disable=arguments-differ
    def _setup_and_call(self, *args, **kwargs):
        return super()._setup_and_call(*args, **kwargs)
