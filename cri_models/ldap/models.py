from django.db import models
from django.conf import settings
from django.utils.functional import classproperty

from ldapdb.models import Model, fields


class DN(str):
    _obj = None

    # str is immutable and can't have an __init__ method.
    def __new__(cls, ldap_model, *args, **kwargs):
        obj = str.__new__(cls, *args, **kwargs)
        setattr(obj, "_ldap_model", ldap_model)
        return obj

    def __repr__(self):
        return f"<DN: {self}>"

    @property
    def obj(self):
        if not self._obj:
            self._obj = self._ldap_model.objects.get(dn=self)
        return self._obj


class LDAPDNField(models.CharField):
    def __init__(self, ldap_model, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.ldap_model = ldap_model

    def from_db_value(self, value, _expression, _connection):
        return self.to_python(value)

    def get_prep_value(self, value):
        return super().get_prep_value(value) or None

    def to_python(self, value):
        if not value:
            return ""
        if isinstance(value, DN):
            return value
        return DN(self.ldap_model, value)

    def deconstruct(self):
        name, _path, args, kwargs = super().deconstruct()
        return (
            name,
            f"{__name__}.{type(self).__name__}",
            (self.ldap_model, *args),
            kwargs,
        )


class LDAPModel(Model):
    DB_RO_FIELDS = ("created_at", "created_by", "updated_at", "updated_by")

    RO_FIELDS = ("dn", "classes", *DB_RO_FIELDS)

    object_classes = Model.object_classes

    relative_base_dn = None

    classes = fields.ListField(
        db_column="objectClass",
        verbose_name="Object class list",
        editable=False,
        default=object_classes,
    )

    dn = fields.CharField(
        primary_key=True, max_length=200, editable=False, verbose_name="DN"
    )

    created_at = fields.DateTimeField(db_column="createTimestamp", editable=False)

    created_by = fields.CharField(db_column="creatorsName", editable=False)

    updated_at = fields.DateTimeField(db_column="modifyTimestamp", editable=False)

    updated_by = fields.CharField(db_column="modifiersName", editable=False)

    @classproperty
    # pylint: disable=method-hidden
    def base_dn(cls):
        return cls.get_base_dn()

    class Meta:
        abstract = True

    class LDAPMeta:
        pass

    @classmethod
    def get_base_dn(cls):
        if cls.relative_base_dn:
            return f"{cls.relative_base_dn},{settings.LDAP_BASE_DN}"
        return settings.LDAP_BASE_DN

    def __str__(self):
        return self.dn

    def delete_subtree(self):
        # pylint: disable=protected-access
        objects = LDAPEntry._base_manager.all()
        key_func = lambda o: len(o.dn.split(","))
        for obj in reversed(sorted(objects, key=key_func)):
            if obj.dn.endswith(self.dn):
                obj.delete()

    def get_object_classes_map(self):
        return getattr(self.LDAPMeta, "object_classes_map", {})

    def _process_dynamic_object_classes(self):
        objcls_map = self.get_object_classes_map()
        discarded_attrs = set()
        kept_attrs = set()
        object_classes = set(self.classes) - set(objcls_map.keys())
        for objcls in self.object_classes:
            object_classes.add(objcls)
            if objcls in objcls_map:
                required, optional, *_ = tuple(objcls_map[objcls]) + ((), ())
                kept_attrs |= set(required) | set(optional)
        for objcls, attrs_list in objcls_map.items():
            if objcls in self.object_classes:
                continue
            required, optional, *_ = tuple(attrs_list) + ((), ())
            object_classes.discard(objcls)
            if not required:
                if any(getattr(self, a) and a not in kept_attrs for a in optional):
                    object_classes.add(objcls)
                    kept_attrs |= set(optional)
            elif all(getattr(self, a) for a in required):
                object_classes.add(objcls)
                kept_attrs |= set(required) | set(optional)
            else:
                discarded_attrs |= set(required) | set(optional)
        self.classes = tuple(object_classes)
        return set(
            a for a in discarded_attrs if a not in kept_attrs and getattr(self, a)
        )

    # pylint: disable=too-many-arguments
    def _save_table(
        self,
        raw=False,
        cls=None,
        force_insert=False,
        force_update=False,
        using=None,
        update_fields=None,
    ):
        if not update_fields:
            update_fields = [
                field.attname
                for field in cls._meta.get_fields(include_hidden=True)
                if field.concrete and not field.primary_key
            ]
        # We remove DB_RO_FIELDS from the set of fields to be updated
        update_fields = set(update_fields) - set(self.DB_RO_FIELDS)

        # We remove fields that can't be saved due to object classes
        # requirements
        update_fields -= set(self._process_dynamic_object_classes())

        # django-ldapdb does not support objectClass updates so we swap a few
        # values arround to make it works anyway
        object_classes = self.object_classes
        self.object_classes = self.classes
        if not self.dn or force_insert:
            # We empty self.classes to prevent duplicate objectClass add
            # operations during new instance creation
            self.classes = []
        r = super()._save_table(
            raw, cls, force_insert, force_update, using, update_fields
        )
        self.classes = self.object_classes
        self.object_classes = object_classes
        return r


# This class allows us to manipulate any LDAP entry, we can't use LDAPModel
# because it is abstract and can't be instancied.
class LDAPEntry(LDAPModel):
    pass
