from ldapdb.models import fields

from .models import LDAPModel


class LDAPUser(LDAPModel):
    SASL_PREFIX = "{SASL}"

    # LDAP metadata
    relative_base_dn = "ou=users"

    object_classes = (
        "inetOrgPerson",
        "posixAccount",
    )

    classes = fields.ListField(
        db_column="objectClass",
        editable=False,
        verbose_name="Object class list",
        default=object_classes,
    )

    # inetOrgPerson attributes
    first_name = fields.CharField(
        db_column="givenName",
        max_length=100,
        # inetOrgPerson does not require the givenName attribute
        blank=True,
    )

    last_name = fields.CharField(db_column="sn", max_length=100)

    full_name = fields.CharField(db_column="cn", max_length=201)

    email = fields.CharField(db_column="mail", max_length=254, blank=True)

    passwords = fields.ListField(db_column="userPassword", blank=True)

    # posixAccount attributes
    # The `uid` LDAP attribute is actually a username whereas the user POSIX UID
    # is stored in the `uidNumber` attribute.

    login = fields.CharField(db_column="uid", max_length=100, primary_key=True)

    gid = fields.IntegerField(db_column="gidNumber", unique=True, verbose_name="GID")

    uid = fields.IntegerField(db_column="uidNumber", unique=True, verbose_name="UID")

    home = fields.CharField(db_column="homeDirectory")

    # ldapPublicKey attributes

    ssh_keys = fields.ListField(
        db_column="sshPublicKey", blank=True, verbose_name="Public SSH keys"
    )

    # mailAliasAccount attributes

    mail_aliases = fields.ListField(
        db_column="mailalias",
        blank=True,
        null=True,
        verbose_name="Email aliases",
    )

    class Meta:
        verbose_name = "LDAP user"

    class LDAPMeta:
        object_classes_map = {
            "inetOrgPerson": (
                ("last_name", "full_name"),
                ("first_name", "email", "passwords"),
            ),
            "posixAccount": (
                ("login", "gid", "uid", "home", "full_name"),
                ("passwords"),
            ),
            "mailAccount": (("email",), ("mail_aliases",)),
            "ldapPublicKey": (
                (),
                ("ssh_keys", "uid"),
            ),
        }

    def __str__(self):
        return self.login

    def set_sasl(self, sasl_data):
        self.passwords = set((f"{self.SASL_PREFIX}{sasl_data}",))

    def is_auth_with_sasl(self):
        for password in set(self.passwords):
            if password.startswith(self.SASL_PREFIX):
                return True
        return False
