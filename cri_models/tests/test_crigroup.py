from django.test import TestCase
from django.contrib.auth import get_user_model

from ..models import CRIGroup
from ..ldap import LDAPGroup


class TestCRIGroup(TestCase):
    fixtures = ("tests/groups.yaml",)

    @classmethod
    def setUpTestData(cls):
        User = get_user_model()

        for index, name in enumerate(("user1",)):
            User(
                username=f"test/{name}",
                email="{name}@example.org",
                uid=42 + index,
            ).save()

        for index, name in enumerate(("group1", "group2", "group3", "group4")):
            CRIGroup(
                name=f"test/{name}",
                slug=f"test-{name}",
                kind="other",
                gid=42 + index,
            ).save()

    def test_str(self):
        TEST_NAME = "test"
        group = CRIGroup(name=TEST_NAME)
        self.assertEqual(str(group), TEST_NAME)

    def test_ldap_dn_empty(self):
        with self.assertRaises(ValueError):
            CRIGroup().ldap_dn  # pylint:disable=expression-not-assigned

    def test_ldap_dn(self):
        group_base_dn = LDAPGroup.base_dn
        self.assertEqual(CRIGroup(slug="test").ldap_dn, f"cn=test,{group_base_dn}")

    def test_computed_children(self):
        group1 = CRIGroup.objects.get(name="test/group1")
        group2 = CRIGroup.objects.get(name="test/group2")
        group3 = CRIGroup.objects.get(name="test/group3")

        group1.children.add(group2)
        self.assertIn(group2, group1.computed_children.all())

        group2.children.add(group3)
        self.assertIn(group3, group2.computed_children.all())
        self.assertIn(group3, group1.computed_children.all())

        group1.children.clear()
        group2.children.clear()

        self.assertNotIn(group2, group1.computed_children.all())
        self.assertNotIn(group3, group2.computed_children.all())
        self.assertNotIn(group3, group1.computed_children.all())

    def test_circular_computed_children(self):
        group1 = CRIGroup.objects.get(name="test/group1")
        group2 = CRIGroup.objects.get(name="test/group2")
        group3 = CRIGroup.objects.get(name="test/group3")

        group1.children.add(group2)

        group2.children.add(group3)
        group3.children.add(group1)

        self.assertIn(group2, group1.computed_children.all())
        self.assertIn(group3, group1.computed_children.all())

        self.assertIn(group1, group2.computed_children.all())
        self.assertIn(group3, group2.computed_children.all())

        self.assertIn(group1, group3.computed_children.all())
        self.assertIn(group2, group3.computed_children.all())

        group1.children.clear()
        group2.children.clear()
        group3.children.clear()

        self.assertNotIn(group2, group1.computed_children.all())
        self.assertNotIn(group3, group1.computed_children.all())

        self.assertNotIn(group1, group2.computed_children.all())
        self.assertNotIn(group3, group2.computed_children.all())

        self.assertNotIn(group1, group3.computed_children.all())
        self.assertNotIn(group2, group3.computed_children.all())

    def test_remove_intermediate_children(self):
        group1 = CRIGroup.objects.get(name="test/group1")
        group2 = CRIGroup.objects.get(name="test/group2")
        group3 = CRIGroup.objects.get(name="test/group3")

        group1.children.add(group2)
        group2.children.add(group3)

        self.assertIn(group3, group1.computed_children.all())

        group2.children.clear()

        self.assertNotIn(group3, group2.computed_children.all())

        group1.children.clear()

    def test_remove_intermediate_children_reversed(self):
        group1 = CRIGroup.objects.get(name="test/group1")
        group2 = CRIGroup.objects.get(name="test/group2")
        group3 = CRIGroup.objects.get(name="test/group3")

        group1.children.add(group2)
        group2.children.add(group3)

        self.assertIn(group3, group1.computed_children.all())

        group3.parents.remove(group2)

        self.assertNotIn(group1, group2.computed_children.all())

        group1.children.clear()

    def test_remove_shared_children(self):
        group1 = CRIGroup.objects.get(name="test/group1")
        group2 = CRIGroup.objects.get(name="test/group2")
        group3 = CRIGroup.objects.get(name="test/group3")
        group4 = CRIGroup.objects.get(name="test/group4")

        group1.children.add(group2)
        group1.children.add(group3)
        group2.children.add(group4)
        group3.children.add(group4)

        self.assertIn(group4, group2.computed_children.all())
        self.assertIn(group4, group3.computed_children.all())
        self.assertIn(group4, group1.computed_children.all())

        group2.children.clear()

        self.assertNotIn(group4, group2.computed_children.all())
        self.assertIn(group4, group3.computed_children.all())
        self.assertIn(group4, group1.computed_children.all())

        group1.children.clear()
        group3.children.clear()
        group4.children.clear()

    def test_get_managers(self):
        User = get_user_model()

        user1 = User.objects.get(username="test/user1")

        group1 = CRIGroup.objects.get(name="test/group1")
        group2 = CRIGroup.objects.get(name="test/group2")
        group3 = CRIGroup.objects.get(name="test/group3")

        group1.children.add(group2)
        group2.children.add(group3)

        group2.managers.add(user1)

        self.assertNotIn(user1, group1.get_managers())
        self.assertIn(user1, group2.get_managers())
        self.assertIn(user1, group3.get_managers())

        group1.children.clear()
        group2.children.clear()
        group2.managers.clear()
