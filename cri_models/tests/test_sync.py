from .. import models, fields, ldap
from ..kerberos.models import Principal

from django.utils import timezone


class TestSync(ldap.test.LDAPTestCase):
    fixtures = ("tests/groups.yaml",)
    TEST_PRINCIPAL = "testtest"
    TEST_SSH_PUBLIC_KEY = fields.SSHPubKey(
        "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIEGODBKRjsFB/"
        "1v3pDRGpA6xR+QpOJg9vat0brlbUNDD test"
    )

    @classmethod
    def tearDownClass(cls):
        try:
            super().tearDownClass()
        finally:
            try:
                principal = Principal.get(cls.TEST_PRINCIPAL)
                principal.delete()  # pragma: nocover
            except Principal.DoesNotExist:
                pass

    @classmethod
    def setUpTestDataAfterSetUp(_cls):
        pass

    def _create_and_sync_user(self, **kwargs):
        data = {
            "username": "test",
            "last_name": "test",
            "ldap_dn": ldap.LDAPUser(login="test").build_dn(),
            "uid": 42,
        }
        data.update(kwargs)
        test_user = models.CRIUser(**data)
        test_user.save()
        test_user.sync()
        ldap_user = ldap.LDAPUser.objects.get(dn=test_user.ldap_dn)
        return test_user, ldap_user

    def _create_and_sync_group(self, **kwargs):
        data = {"slug": "test", "name": "test"}
        data.update(kwargs)
        test_group = models.CRIGroup(**data)
        test_group.save()
        test_group.sync()
        ldap_group = ldap.LDAPGroup.objects.get(dn=test_group.ldap_dn)
        return test_group, ldap_group

    def _create_and_sync_principal(self, **kwargs):
        data = {"principal": self.TEST_PRINCIPAL}
        data.update(kwargs)
        test_principal = models.KerberosPrincipal(**data)
        test_principal.save()
        test_principal.sync()
        test_principal.refresh_from_db()
        kerberos_principal = Principal.get(test_principal.principal)
        return test_principal, kerberos_principal

    def _compare_user(self, test_user, ldap_user):
        self.assertEqual(test_user.email, ldap_user.email)
        self.assertEqual(test_user.first_name, ldap_user.first_name)
        self.assertEqual(test_user.get_full_name(), ldap_user.full_name)
        self.assertEqual(test_user.primary_group.gid, ldap_user.gid)
        self.assertEqual(test_user.last_name, ldap_user.last_name)
        self.assertEqual(test_user.username, ldap_user.login)
        self.assertEqual(test_user.uid, ldap_user.uid)
        self.assertEqual(test_user.uid, ldap_user.uid)
        if test_user.primary_principal:
            self.assertEqual(
                set((f"{{SASL}}{test_user.primary_principal.principal}",)),
                set(ldap_user.passwords),
            )

    def _compare_group(self, test_group, ldap_group):
        self.assertEqual(test_group.slug, ldap_group.name)
        self.assertEqual(test_group.gid, ldap_group.gid)

    def _compare_principal(self, test_principal, kerberos_principal):
        self.assertEqual(test_principal.principal, kerberos_principal.principal)

    def test_sync_user_creation(self):
        test_user, ldap_user = self._create_and_sync_user()
        self._compare_user(test_user, ldap_user)
        ldap_user.delete()
        test_user.delete()

    def test_sync_user_update(self):
        test_user, ldap_user = self._create_and_sync_user()
        test_user.last_name = "test2"
        test_user.save()
        test_user.sync()
        ldap_user.refresh_from_db()
        self._compare_user(test_user, ldap_user)
        ldap_user.delete()
        test_user.delete()

    def test_sync_user_ssh_keys_update(self):
        test_user, ldap_user = self._create_and_sync_user()
        key = test_user.sshpublickey_set.create(key=self.TEST_SSH_PUBLIC_KEY)
        test_user.sync()
        ldap_user.refresh_from_db()
        user_keys = set(k.key.keydata for k in test_user.sshpublickey_set.all())
        ldap_keys = set(ldap_user.ssh_keys)
        self.assertIn(key.key.keydata, ldap_keys)
        self.assertEqual(user_keys, ldap_keys)
        test_user.delete()
        ldap_user.delete()

    def test_sync_disabled_user_ssh_keys(self):
        test_user, ldap_user = self._create_and_sync_user(is_active=False)
        key = test_user.sshpublickey_set.create(key=self.TEST_SSH_PUBLIC_KEY)
        test_user.sync()
        ldap_user.refresh_from_db()
        ldap_keys = set(ldap_user.ssh_keys)
        self.assertNotIn(key.key.keydata, ldap_keys)
        test_user.delete()
        ldap_user.delete()

    def test_sync_disabled_user_expire_principal(self):
        test_user, ldap_user = self._create_and_sync_user()
        test_principal, kerberos_principal = self._create_and_sync_principal(
            user=test_user
        )
        self.assertIsNone(kerberos_principal.expire_at)
        test_user.is_active = False
        test_user.save()
        test_principal.sync()
        kerberos_principal.refresh_from_db()
        self.assertIsNotNone(kerberos_principal.expire_at)
        test_principal.delete()
        kerberos_principal.delete()
        test_user.delete()
        ldap_user.delete()

    def test_sync_user_deletion(self):
        test_user, ldap_user = self._create_and_sync_user()
        test_user.delete()
        test_user.sync_all()
        with self.assertRaises(ldap_user.DoesNotExist):
            ldap_user.refresh_from_db()

    def test_sync_group_creation(self):
        test_group, ldap_group = self._create_and_sync_group()
        self._compare_group(test_group, ldap_group)
        ldap_group.delete()
        test_group.delete()

    def test_sync_group_update(self):
        test_group, ldap_group = self._create_and_sync_group()
        test_group.gid = 42
        test_group.sync()
        ldap_group.refresh_from_db()
        self._compare_group(test_group, ldap_group)
        ldap_group.delete()
        test_group.delete()

    def test_sync_group_membership_update(self):
        test_user, ldap_user = self._create_and_sync_user()
        test_group, ldap_group = self._create_and_sync_group()
        test_group.memberships.create(user=test_user, begin_at=timezone.now().date())
        test_group.sync()
        ldap_group.refresh_from_db()
        self.assertIn(test_user.ldap_dn, ldap_group.members)
        test_user.delete()
        ldap_user.delete()
        test_group.delete()
        ldap_group.delete()

    def test_sync_group_deletion(self):
        test_group, ldap_group = self._create_and_sync_group()
        test_group.delete()
        test_group.sync_all()
        with self.assertRaises(ldap_group.DoesNotExist):
            ldap_group.refresh_from_db()

    def test_sync_principal_creation(self):
        test_principal, kerberos_principal = self._create_and_sync_principal()
        self._compare_principal(test_principal, kerberos_principal)
        test_principal.delete()
        kerberos_principal.delete()

    def test_sync_principal_deletion(self):
        test_principal, kerberos_principal = self._create_and_sync_principal()
        test_principal.delete()
        test_principal.sync_all()
        with self.assertRaises(kerberos_principal.DoesNotExist):
            kerberos_principal.refresh_from_db()

    def test_sync_primary_principal(self):
        test_user, ldap_user = self._create_and_sync_user()
        test_principal, kerberos_principal = self._create_and_sync_principal()
        test_user.primary_principal = test_principal
        test_user.save()
        test_user.sync()
        ldap_user.refresh_from_db()
        self._compare_user(test_user, ldap_user)
        test_principal.delete()
        kerberos_principal.delete()
        test_user.delete()
        ldap_user.delete()
