from django.test import TestCase

from ..models import CRIUser
from ..ldap import LDAPUser


class TestCRIUser(TestCase):
    fixtures = ("tests/groups.yaml",)

    @classmethod
    def setUpTestData(cls):
        for index, name in enumerate(("user1", "user2", "user3")):
            CRIUser(
                username=f"test/{name}",
                email="{name}@example.org",
                uid=42 + index,
            ).save()

    def test_str(self):
        TEST_USERNAME = "test"
        user = CRIUser(username=TEST_USERNAME)
        self.assertEqual(str(user), TEST_USERNAME)

    def test_old_accounts_empty(self):
        user1 = CRIUser.objects.get(username="test/user1")

        self.assertEqual(len(user1.get_old_accounts()), 0)
        self.assertIsNone(user1.get_new_account())

    def test_old_accounts(self):
        user1 = CRIUser.objects.get(username="test/user1")
        user2 = CRIUser.objects.get(username="test/user2")
        user3 = CRIUser.objects.get(username="test/user3")

        USERS = set([user1, user2, user2])

        user1.old_account = user2
        user1.save()
        user2.old_account = user3
        user2.save()

        self.assertIn(user2, user1.get_old_accounts())
        self.assertIn(user3, user1.get_old_accounts())
        self.assertIn(user3, user2.get_old_accounts())
        self.assertIsNone(user1.get_new_account())
        self.assertIs(user2.get_new_account(), user1)
        self.assertIs(user3.get_new_account(), user1)

        for user in USERS:
            user.old_account = None
            user.save()

    def test_old_accounts_loop(self):
        user1 = CRIUser.objects.get(username="test/user1")
        user2 = CRIUser.objects.get(username="test/user2")
        user3 = CRIUser.objects.get(username="test/user3")

        USERS = set([user1, user2, user2])

        user1.old_account = user2
        user2.old_account = user3
        user3.old_account = user1
        for user in USERS:
            user.save()

        for user in USERS:
            for u in USERS - set([user]):
                with self.assertLogs(level="WARNING"):
                    self.assertIn(u, user.get_old_accounts())
            with self.assertLogs(level="WARNING"):
                self.assertIsNone(user.get_new_account())

        for user in USERS:
            user.old_account = None
            user.save()

    def test_conflicting_dn(self):
        TEST_USERNAME = "test/conflicting_dn"
        user1 = CRIUser(
            username=TEST_USERNAME + "1",
            uid=40,
            ldap_dn=LDAPUser(login=TEST_USERNAME + "2").build_dn(),
        )
        user1.save()
        user2 = CRIUser(username=TEST_USERNAME + "2", uid=41)
        with self.assertLogs(level="WARNING"):
            user2.save()
        self.assertFalse(user2.ldap_dn)
        user1.delete()
        user2.delete()
