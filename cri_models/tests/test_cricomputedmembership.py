from django.test import TestCase
from django.utils import timezone
from django.contrib.auth import get_user_model
from django.core.exceptions import ValidationError

from datetime import timedelta

from ..models import CRIGroup, CRIMembership, CRIComputedMembership


class TestCRIComputedMembership(TestCase):
    fixtures = ("tests/groups.yaml",)

    @classmethod
    def setUpTestData(cls):
        User = get_user_model()

        for index, name in enumerate(("user1", "user2", "user3")):
            User(
                username=f"test/{name}",
                email="{name}@example.org",
                uid=42 + index,
            ).save()

        for index, name in enumerate(("group1", "group2", "group3", "group4")):
            CRIGroup(
                name=f"test/{name}",
                slug=f"test-{name}",
                kind="other",
                gid=42 + index,
            ).save()

    def _create_membership(self, user, group):
        mbship = CRIMembership(
            user=user,
            group=group,
            graduation_year=2000,
            begin_at=timezone.now().date(),
        )
        mbship.save()
        return mbship

    def _compare_membership(self, mbship1, mbship2):
        self.assertEqual(mbship1.user, mbship2.user)
        self.assertEqual(mbship1.graduation_year, mbship2.graduation_year)
        self.assertEqual(mbship1.begin_at, mbship2.begin_at)
        self.assertEqual(mbship1.end_at, mbship2.end_at)

    def test_is_current_in_interval(self):
        today = timezone.now().date()
        oneday = timedelta(days=1)

        mbship = CRIComputedMembership(begin_at=today - oneday, end_at=today + oneday)
        self.assertTrue(mbship.is_current())

    def test_is_current_after_interval(self):
        today = timezone.now().date()
        oneday = timedelta(days=1)

        mbship = CRIComputedMembership(begin_at=today + oneday, end_at=today + oneday)
        self.assertFalse(mbship.is_current())

    def test_is_current_before_interval(self):
        today = timezone.now().date()
        oneday = timedelta(days=1)

        mbship = CRIComputedMembership(begin_at=today - oneday, end_at=today - oneday)
        self.assertFalse(mbship.is_current())

    def test_is_current_single_day_interval(self):
        today = timezone.now().date()

        mbship = CRIComputedMembership(begin_at=today, end_at=today)
        self.assertTrue(mbship.is_current())

    def test_clean_fields_invalid_interval(self):
        User = get_user_model()

        today = timezone.now().date()
        oneday = timedelta(days=1)

        user1 = User.objects.get(username="test/user1")
        group1 = CRIGroup.objects.get(name="test/group1")

        mbship = CRIComputedMembership(
            user=user1, group=group1, begin_at=today + oneday, end_at=today
        )

        with self.assertRaises(ValidationError):
            mbship.clean_fields()

    def test_simple_membership(self):
        User = get_user_model()

        user1 = User.objects.get(username="test/user1")
        group1 = CRIGroup.objects.get(name="test/group1")

        mbship = self._create_membership(user1, group1)

        cmbship = CRIComputedMembership.objects.get(group=group1, user=user1)

        self._compare_membership(mbship, cmbship)

        mbship.delete()
        try:
            with self.assertRaises(CRIComputedMembership.DoesNotExist):
                cmbship.refresh_from_db()
        except:  # noqa: E722 # pragma: nocover
            cmbship.delete()

    def test_children_membership_before(self):
        User = get_user_model()

        user1 = User.objects.get(username="test/user1")
        group1 = CRIGroup.objects.get(name="test/group1")
        group2 = CRIGroup.objects.get(name="test/group2")

        group1.children.add(group2)
        mbship = self._create_membership(user1, group2)
        cmbship = CRIComputedMembership.objects.get(group=group1, user=user1)
        self._compare_membership(mbship, cmbship)

        group1.children.clear()
        with self.assertRaises(CRIComputedMembership.DoesNotExist):
            CRIComputedMembership.objects.get(group=group1, user=user1)
        mbship.delete()

    def test_children_membership_after(self):
        User = get_user_model()

        user1 = User.objects.get(username="test/user1")
        group1 = CRIGroup.objects.get(name="test/group1")
        group2 = CRIGroup.objects.get(name="test/group2")

        mbship = self._create_membership(user1, group2)
        group1.children.add(group2)
        cmbship = CRIComputedMembership.objects.get(group=group1, user=user1)
        self._compare_membership(mbship, cmbship)

        mbship.delete()
        with self.assertRaises(CRIComputedMembership.DoesNotExist):
            CRIComputedMembership.objects.get(group=group1, user=user1)

    def test_children_membership_change(self):
        User = get_user_model()

        user1 = User.objects.get(username="test/user1")
        group1 = CRIGroup.objects.get(name="test/group1")
        group2 = CRIGroup.objects.get(name="test/group2")
        group1.children.add(group2)

        mbship = self._create_membership(user1, group2)
        cmbship = CRIComputedMembership.objects.get(group=group1, user=user1)
        self._compare_membership(mbship, cmbship)

        mbship.graduation_year += 1
        mbship.save()

        cmbship = CRIComputedMembership.objects.get(group=group1, user=user1)
        self._compare_membership(mbship, cmbship)

        group1.children.clear()
        mbship.delete()

    def test_children_membership_merge(self):
        User = get_user_model()
        oneday = timedelta(days=1)

        user1 = User.objects.get(username="test/user1")
        group1 = CRIGroup.objects.get(name="test/group1")
        group2 = CRIGroup.objects.get(name="test/group2")
        group3 = CRIGroup.objects.get(name="test/group3")

        group1.children.add(group2)
        group1.children.add(group3)

        mbship2 = self._create_membership(user1, group2)
        mbship2.begin_at -= oneday
        mbship2.end_at = mbship2.begin_at + oneday
        mbship2.save()
        mbship3 = self._create_membership(user1, group3)
        mbship3.end_at = mbship2.end_at + oneday
        mbship3.save()

        cmbship = CRIComputedMembership.objects.get(group=group1, user=user1)
        self.assertEqual(mbship2.begin_at, cmbship.begin_at)
        self.assertEqual(mbship3.end_at, cmbship.end_at)

        mbship2.delete()
        mbship3.delete()
        group1.children.clear()

    def test_children_membership_merge_with_begin_at_equals_none(self):
        User = get_user_model()
        oneday = timedelta(days=1)

        user1 = User.objects.get(username="test/user1")
        group1 = CRIGroup.objects.get(name="test/group1")
        group2 = CRIGroup.objects.get(name="test/group2")
        group3 = CRIGroup.objects.get(name="test/group3")

        group1.children.add(group2)
        group1.children.add(group3)

        mbship2 = self._create_membership(user1, group2)
        mbship2.end_at = mbship2.begin_at + oneday
        mbship2.save()
        mbship3 = self._create_membership(user1, group3)
        mbship3.begin_at = None
        mbship3.end_at = mbship2.begin_at + oneday
        mbship3.save()

        cmbship = CRIComputedMembership.objects.get(group=group1, user=user1)
        self.assertEqual(None, cmbship.begin_at)

        mbship2.delete()
        mbship3.delete()
        group1.children.clear()

    def test_children_membership_merge_with_end_at_equals_none(self):
        User = get_user_model()
        oneday = timedelta(days=1)

        user1 = User.objects.get(username="test/user1")
        group1 = CRIGroup.objects.get(name="test/group1")
        group2 = CRIGroup.objects.get(name="test/group2")
        group3 = CRIGroup.objects.get(name="test/group3")

        group1.children.add(group2)
        group1.children.add(group3)

        mbship2 = self._create_membership(user1, group2)
        mbship2.end_at = mbship2.begin_at + oneday
        mbship2.save()
        mbship3 = self._create_membership(user1, group3)
        mbship3.end_at = None
        mbship3.save()

        cmbship = CRIComputedMembership.objects.get(group=group1, user=user1)
        self.assertEqual(None, cmbship.end_at)

        mbship2.delete()
        mbship3.delete()
        group1.children.clear()
