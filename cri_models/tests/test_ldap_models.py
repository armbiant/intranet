from .. import ldap


class TestLDAPModels(ldap.test.LDAPTestCase):
    def _create_user(self, commit=False, **kwargs):
        args = {
            "login": "test",
            "first_name": "test",
            "last_name": "test",
            "full_name": "test",
            "gid": 42,
            "uid": 42,
            "home": "/home/test",
        }
        args.update(kwargs)
        user = ldap.LDAPUser(**args)
        if commit:
            user.save()
        return user

    def _create_group(self, commit=False, **kwargs):
        args = {"name": "test"}
        args.update(kwargs)
        group = ldap.LDAPGroup(**args)
        if commit:
            group.save()
        return group

    def test_user_str(self):
        user = self._create_user()
        self.assertEqual(str(user), user.login)

    def test_user_creation_and_removal(self):
        user = self._create_user(commit=True)
        found_user = ldap.LDAPUser.objects.get(login=user.login)
        self.assertEqual(user, found_user)
        user.delete()
        with self.assertRaises(ldap.LDAPUser.DoesNotExist):
            ldap.LDAPUser.objects.get(login=user.login)

    def test_user_sasl_password(self):
        user = self._create_user()
        self.assertFalse(user.is_auth_with_sasl())
        user.set_sasl("test")
        self.assertTrue(user.is_auth_with_sasl())

    def test_group_str(self):
        group = self._create_group()
        self.assertEqual(str(group), group.name)

    def test_group_creation_and_removal(self):
        group = self._create_group(commit=True)
        found_group = ldap.LDAPGroup.objects.get(name=group.name)
        self.assertEqual(group, found_group)
        group.delete()
        with self.assertRaises(ldap.LDAPGroup.DoesNotExist):
            ldap.LDAPGroup.objects.get(name=group.name)

    def test_posixgroup_creation_and_removal(self):
        group = self._create_group(commit=True, gid=42)
        found_group = ldap.LDAPGroup.objects.get(name=group.name)
        self.assertIn("posixGroup", found_group.classes)
        self.assertEqual(group, found_group)
        group.delete()
        with self.assertRaises(ldap.LDAPGroup.DoesNotExist):
            ldap.LDAPGroup.objects.get(name=group.name)

    def test_group_gid_update(self):
        group = self._create_group(commit=True)
        found_group = ldap.LDAPGroup.objects.get(name=group.name)
        self.assertEqual(group, found_group)
        self.assertNotIn("posixGroup", found_group.classes)

        found_group.gid = 42
        found_group.save()
        found_group = ldap.LDAPGroup.objects.get(name=group.name)
        self.assertIn("posixGroup", found_group.classes)

        found_group.gid = None
        found_group.save()
        found_group = ldap.LDAPGroup.objects.get(name=group.name)
        self.assertNotIn("posixGroup", found_group.classes)

        found_group.delete()
        with self.assertRaises(ldap.LDAPGroup.DoesNotExist):
            ldap.LDAPGroup.objects.get(name=group.name)

    def test_group_membership(self):
        user = self._create_user(commit=True)

        group = self._create_group()
        group.members = [user.dn]
        group.save()

        found_group = ldap.LDAPGroup.objects.get(name=group.name)
        self.assertEqual(group, found_group)
        self.assertIn(user.dn, found_group.members)
        self.assertIn(user, found_group.get_members())

        group.members = []
        self.assertEqual([], list(group.get_members()))

        group.delete()
        with self.assertRaises(ldap.LDAPGroup.DoesNotExist):
            ldap.LDAPGroup.objects.get(name=group.name)

        user.delete()
        with self.assertRaises(ldap.LDAPUser.DoesNotExist):
            ldap.LDAPUser.objects.get(login=user.login)

    def test_group_invalid_member_dn(self):
        group = self._create_group(name="test")
        group.members = ["cn=invalid"]
        group.save()

        found_group = ldap.LDAPGroup.objects.get(name=group.name)
        self.assertEqual(group, found_group)
        self.assertEqual([], list(group.get_members()))

        group.delete()
        with self.assertRaises(ldap.LDAPGroup.DoesNotExist):
            ldap.LDAPGroup.objects.get(name=group.name)
