from django.test import TestCase

from xmlrpc.client import ServerProxy
from xml.parsers.expat import ExpatError

from cri_intranet.tests import MockProxy
from .. import moodle


TEST_COHORT = {
    "id": 42,
    "idnumber": "test",
    "name": "test",
    "description": "test",
    "descriptionformat": 2,
    "visible": 1,
}


class TestMoodleCohort(TestCase):
    def _get_proxy(self):
        return MockProxy(
            ServerProxy(
                "https://moodle.invalid/test",
                allow_none=True,
                use_builtin_types=True,
            )
        )

    def _compare_cohort_with_dict(self, cohort, cohort_dict):
        for k, v in cohort_dict.items():
            if k == "id":
                k = "obj_id"
            self.assertEqual(getattr(cohort, k), v)

    def test_get_cohort_empty(self):
        overrides = {"core_cohort_get_cohorts": lambda cohort_ids=None: []}
        proxy = self._get_proxy()
        with MockProxy.Override(ServerProxy, overrides):
            self.assertFalse(list(moodle.MoodleCohort.all(proxy)))
            with self.assertRaises(moodle.MoodleCohort.DoesNotExist):
                moodle.MoodleCohort.get(proxy, cohort_id=0)
            with self.assertRaises(moodle.MoodleCohort.DoesNotExist):
                moodle.MoodleCohort.get(proxy, cohort_idnumber="test")

    def test_get_cohort(self):
        cohorts_data = [TEST_COHORT.copy()]
        overrides = {"core_cohort_get_cohorts": lambda cohort_ids=None: cohorts_data}
        proxy = self._get_proxy()
        with MockProxy.Override(ServerProxy, overrides):
            cohorts = list(moodle.MoodleCohort.all(proxy))
            self.assertEqual(len(cohorts), len(cohorts_data))
            self._compare_cohort_with_dict(cohorts[0], TEST_COHORT)
            self._compare_cohort_with_dict(
                moodle.MoodleCohort.get(proxy, cohort_id=TEST_COHORT["id"]),
                TEST_COHORT,
            )
            self._compare_cohort_with_dict(
                moodle.MoodleCohort.get(proxy, cohort_idnumber=TEST_COHORT["idnumber"]),
                TEST_COHORT,
            )

    def test_get_cohort_missing(self):
        cohorts_data = [TEST_COHORT.copy()]

        def get_cohorts(cohort_ids=None):
            if cohort_ids is None:
                return []
            return [cohort for cohort in cohorts_data if cohort.get("id") in cohort_ids]

        overrides = {"core_cohort_get_cohorts": get_cohorts}
        proxy = self._get_proxy()
        with MockProxy.Override(ServerProxy, overrides):
            with self.assertRaises(moodle.MoodleCohort.DoesNotExist):
                moodle.MoodleCohort.get(proxy, cohort_id=0)
            with self.assertRaises(moodle.MoodleCohort.DoesNotExist):
                moodle.MoodleCohort.get(proxy, cohort_idnumber="invalid")

    def test_create_cohort(self):
        overrides = {"core_cohort_create_cohorts": lambda cohorts_info: cohorts_info}
        proxy = self._get_proxy()
        with MockProxy.Override(ServerProxy, overrides):
            self._compare_cohort_with_dict(
                moodle.MoodleCohort.create(proxy, TEST_COHORT.copy()),
                TEST_COHORT,
            )

    def test_create_cohort_bad_response_empty(self):
        overrides = {"core_cohort_create_cohorts": lambda _cohorts_info: []}
        proxy = self._get_proxy()
        with MockProxy.Override(ServerProxy, overrides):
            with self.assertRaises(moodle.MoodleCohort.BadResponse):
                moodle.MoodleCohort.create(proxy, TEST_COHORT.copy())

    def test_create_cohort_bad_response_multiple(self):
        overrides = {"core_cohort_create_cohorts": lambda _cohorts_info: [None] * 2}
        proxy = self._get_proxy()
        with MockProxy.Override(ServerProxy, overrides):
            with self.assertRaises(moodle.MoodleCohort.BadResponse):
                moodle.MoodleCohort.create(proxy, TEST_COHORT.copy())

    def test_update_cohort(self):
        cohorts_data = [TEST_COHORT.copy()]
        updates = []

        def update_cohort(cohort_info_list):
            updates.extend(cohort_info_list)

        overrides = {
            "core_cohort_get_cohorts": lambda _cohort_ids=None: cohorts_data,
            "core_cohort_update_cohort": update_cohort,
        }
        proxy = self._get_proxy()

        with MockProxy.Override(ServerProxy, overrides):
            cohort = moodle.MoodleCohort.get(proxy, cohort_id=TEST_COHORT["id"])
            fields = (
                ("name", "test2"),
                ("idnumber", "test2"),
                ("description", "test2"),
                ("descriptionformat", 3),
                ("visible", 0),
            )
            for name, value in fields:
                self.assertNotEqual(getattr(cohort, name), value)
                setattr(cohort, name, value)
                self.assertEqual(updates[-1].get(name), value)

    def test_cohort_get_members(self):
        cohorts_data = [TEST_COHORT.copy()]
        members = []
        overrides = {
            "core_cohort_get_cohorts": lambda _cohort_ids=None: cohorts_data,
            "core_cohort_get_cohort_members": lambda _cohort_ids: [members],
        }
        proxy = self._get_proxy()
        with MockProxy.Override(ServerProxy, overrides):
            cohort = moodle.MoodleCohort.get(proxy, cohort_id=TEST_COHORT["id"])
            self.assertEqual(cohort.members, [])
            members = [42]
            self.assertEqual(cohort.members, [])
            cohort.refresh()
            self.assertEqual(cohort.members, members)

    def test_cohort_get_members_bad_response(self):
        cohorts_data = [TEST_COHORT.copy()]
        overrides = {
            "core_cohort_get_cohorts": lambda _cohort_ids=None: cohorts_data,
            "core_cohort_get_cohort_members": lambda _cohort_ids: None,
        }
        proxy = self._get_proxy()
        with MockProxy.Override(ServerProxy, overrides):
            cohort = moodle.MoodleCohort.get(proxy, cohort_id=TEST_COHORT["id"])
        overrides = {"core_cohort_get_cohort_members": lambda _cohort_ids: [None]}
        with MockProxy.Override(ServerProxy, overrides):
            with self.assertRaises(moodle.MoodleCohort.BadResponse):
                cohort.get_members()

    def test_cohort_add_members(self):
        def add_members(members):
            for member in members:
                for field_name in ("cohorttype", "usertype"):
                    field = member.get(field_name, {})
                    if "type" not in field or "value" not in field:
                        raise TypeError("add_members: invalid member list")
            return {}

        cohorts_data = [TEST_COHORT.copy()]
        overrides = {
            "core_cohort_get_cohorts": lambda _cohort_ids=None: cohorts_data,
            "core_cohort_add_cohort_members": add_members,
        }
        proxy = self._get_proxy()
        with MockProxy.Override(ServerProxy, overrides):
            cohort = moodle.MoodleCohort.get(proxy, cohort_id=TEST_COHORT["id"])
            self.assertEqual(cohort.add_members([]), [])
            self.assertEqual(cohort.add_members([42]), [])

    def test_cohort_add_members_bad_response(self):
        cohorts_data = [TEST_COHORT.copy()]
        overrides = {
            "core_cohort_get_cohorts": lambda _cohort_ids=None: cohorts_data,
            "core_cohort_add_cohort_members": lambda _members: None,
        }
        proxy = self._get_proxy()
        with MockProxy.Override(ServerProxy, overrides):
            cohort = moodle.MoodleCohort.get(proxy, cohort_id=TEST_COHORT["id"])
            with self.assertRaises(moodle.MoodleCohort.BadResponse):
                cohort.add_members([42])

    def test_cohort_delete_members(self):
        def delete_members(members):
            for member in members:
                if "cohortid" not in member or "userid" not in member:
                    raise TypeError("delete_members: invalid member list")
            # Moodle returns an invalid XML when the operation succeed without error.
            raise ExpatError

        cohorts_data = [TEST_COHORT.copy()]
        overrides = {
            "core_cohort_get_cohorts": lambda cohort_ids=None: cohorts_data,
            "core_cohort_delete_cohort_members": delete_members,
        }
        proxy = self._get_proxy()
        with MockProxy.Override(ServerProxy, overrides):
            cohort = moodle.MoodleCohort.get(proxy, cohort_id=TEST_COHORT["id"])
            self.assertEqual(cohort.delete_members([]), [])
            self.assertEqual(cohort.delete_members([42]), [])
