from .. import kerberos, ldap, models


class TestKerberosPrincipal(ldap.test.LDAPTestCase):
    fixtures = ("tests/groups.yaml",)

    @classmethod
    def setUpClass(cls):
        try:
            p, _ = kerberos.Principal.get_or_create("test/user1")
            p.set_password("test/user1")
            p.save()
            for i in range(2, 5):
                kerberos.Principal.get_or_create(f"test/user{i}")
            super().setUpClass()
        except:  # noqa: E722 # pragma: nocover
            cls.tearDownClass()
            raise

    @classmethod
    def tearDownClass(cls):
        super().tearDownClass()
        principals = ["test/delete"]
        principals += [f"test/user{i}" for i in range(1, 5)]
        for principal in principals:
            try:
                kerberos.Principal.get(principal).delete()
            except kerberos.Principal.DoesNotExist:
                pass

    @classmethod
    def setUpTestData(cls):
        User = models.CRIUser
        user1 = User(username="test/user1", email="test@example.org", uid=11)
        user1.save()
        user1.kerberosprincipal_set.get_or_create(principal="test/user1")

        user2 = User(username="test/user2", email="test@example.org", uid=12)
        user2.save()
        user2.kerberosprincipal_set.get_or_create(principal="test/user2")

        user3 = User(username="test/user3", email="test@example.org", uid=13)
        user3.set_password("test/user3")
        user3.save()
        user3.kerberosprincipal_set.get_or_create(principal="test/user3")

        user4 = User(username="test/user4", email="test@example.org", uid=14)
        user4.set_password("test/user4")
        user4.save()
        user4.kerberosprincipal_set.get_or_create(principal="test/user4")

        user5 = User(username="test/user5", email="test@example.org", uid=15)
        user5.set_password("test/user5")
        user5.save()
        invalid_principal, _ = user5.kerberosprincipal_set.get_or_create(
            principal="invalid/invalid"
        )
        user5.primary_principal = invalid_principal
        user5.save()

    def test_saved_kerberos_principal_change(self):
        principal = kerberos.Principal.get("test/user1")
        with self.assertRaises(ValueError):
            principal.principal = "test"

    def test_kerberos_principal_change(self):
        principal = kerberos.Principal(principal="test")
        principal.principal = "primary/instance@REALM"
        self.assertEqual(principal.primary, "primary")
        self.assertEqual(principal.instance, "instance")
        self.assertEqual(principal.realm, "REALM")

    def test_kerberos_initial_password_dummy(self):
        with self.assertWarns(RuntimeWarning):
            self.assertTrue(
                kerberos.utils.kerberos_insecure_check_password(
                    "test/user1", "test/user1"
                )
            )

    def test_initial_out_of_date(self):
        user = models.CRIUser.objects.get(username="test/user1")
        self.assertTrue(user.kerberosprincipal_set.first().out_of_date)

    def test_kerberos_change_password(self):
        user = models.CRIUser.objects.get(username="test/user2")
        user.set_password("new_password")
        user.save()
        self.assertFalse(user.kerberosprincipal_set.first().out_of_date)
        with self.assertWarns(RuntimeWarning):
            self.assertTrue(
                kerberos.utils.kerberos_insecure_check_password(
                    "test/user2", "new_password"
                )
            )

    def test_check_password_out_of_date(self):
        user = models.CRIUser.objects.get(username="test/user3")
        self.assertTrue(user.check_password("test/user3"))
        self.assertFalse(user.kerberosprincipal_set.first().out_of_date)

    def test_check_password_change_kerberos_password(self):
        user = models.CRIUser.objects.get(username="test/user4")
        self.assertTrue(user.check_password("test/user4"))
        with self.assertWarns(RuntimeWarning):
            self.assertTrue(
                kerberos.utils.kerberos_insecure_check_password(
                    "test/user4", "test/user4"
                )
            )

    def test_kerberos_password_update_primary_failure(self):
        user = models.CRIUser.objects.get(username="test/user5")

        with self.assertRaises(kerberos.Principal.DoesNotExist):
            user.set_password("failure")
            user.save()

        user.refresh_from_db()
        self.assertTrue(user.check_password("test/user5"))
        self.assertFalse(user.check_password("failure"))

    def test_kerberos_principal_refresh_from_db(self):
        principal = kerberos.Principal.get("test/user1")
        old_kvno = principal.kvno
        principal.kvno += 1
        principal.refresh_from_db()
        self.assertEqual(old_kvno, principal.kvno)

    def test_unsaved_kerberos_principal_refresh_from_db(self):
        PRINCIPAL = "test/unsaved_delete"
        principal = kerberos.Principal(PRINCIPAL)
        with self.assertRaises(RuntimeError):
            principal.refresh_from_db()

    def test_kerberos_principal_delete(self):
        PRINCIPAL = "test/delete"
        principal = kerberos.Principal(PRINCIPAL)
        principal.save()
        principal.refresh_from_db()
        self.assertEqual("test", principal.primary)
        self.assertEqual("delete", principal.instance)
        principal.delete()
        with self.assertRaises(RuntimeError):
            principal.refresh_from_db()
        with self.assertRaises(kerberos.Principal.DoesNotExist):
            kerberos.Principal.get(PRINCIPAL)

    def test_unsaved_kerberos_principal_delete(self):
        PRINCIPAL = "test/unsaved_delete"
        principal = kerberos.Principal(PRINCIPAL)
        with self.assertRaises(RuntimeError):
            principal.delete()
