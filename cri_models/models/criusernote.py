from django.db import models
from django.contrib.auth import get_user_model
from django.utils import timezone

from ckeditor.fields import RichTextField

import logging

from .crigroup import CRIGroup


_logger = logging.getLogger(__name__)


class CRIUserNoteScope(models.Model):
    name = models.CharField(max_length=64)

    readers = models.ManyToManyField(
        CRIGroup, related_name="criusernotescope_readers", blank=True
    )

    writers = models.ManyToManyField(
        CRIGroup, related_name="criusernotescope_writers", blank=True
    )

    external_ref_template = models.CharField(max_length=256, blank=True)

    external_ref_url_template = models.CharField(max_length=256, blank=True)

    class Meta:
        verbose_name = "CRI User note scope"

    def __str__(self):
        return self.name

    @classmethod
    def from_reader(cls, user):
        now = timezone.now()
        return cls.objects.filter(
            models.Q(readers__memberships__end_at__gte=now)
            | models.Q(readers__memberships__end_at__isnull=True),
            models.Q(readers__memberships__begin_at__lte=now)
            | models.Q(readers__memberships__begin_at__isnull=True),
            readers__memberships__user=user,
        )

    @classmethod
    def from_writer(cls, user):
        now = timezone.now()
        return cls.objects.filter(
            models.Q(writers__memberships__end_at__gte=now)
            | models.Q(writers__memberships__end_at__isnull=True),
            models.Q(writers__memberships__begin_at__lte=now)
            | models.Q(writers__memberships__begin_at__isnull=True),
            writers__memberships__user=user,
        )


class CRIUserNoteLabel(models.Model):
    slug = models.SlugField()

    class Meta:
        verbose_name = "CRI User note label"

    def __str__(self):
        return self.slug


class CRIUserNote(models.Model):
    EVENT_KIND_CHOICES = (
        (None, "not specified"),
        ("meeting", "meeting"),
        ("call", "call"),
        ("email", "email"),
        ("other", "other"),
    )

    user = models.ForeignKey(get_user_model(), on_delete=models.CASCADE)

    author = models.ForeignKey(
        get_user_model(),
        on_delete=models.CASCADE,
        related_name="criusernote_author",
    )

    event_title = models.CharField(max_length=128, blank=True)

    event_kind = models.CharField(max_length=32, choices=EVENT_KIND_CHOICES, blank=True)

    event_date = models.DateField(blank=True, null=True)

    created_at = models.DateTimeField(editable=False, blank=True, null=True)

    updated_at = models.DateTimeField(editable=False, blank=True, null=True)

    updated_by = models.ForeignKey(
        get_user_model(),
        on_delete=models.SET_NULL,
        related_name="criusernote_updated_by",
        editable=False,
        blank=True,
        null=True,
    )

    scope = models.ForeignKey(CRIUserNoteScope, null=True, on_delete=models.SET_NULL)

    labels = models.ManyToManyField(CRIUserNoteLabel, blank=True)

    content = RichTextField(blank=True)

    external_ref = models.CharField(max_length=64, blank=True)

    secret = models.BooleanField(default=False)

    class Meta:
        verbose_name = "CRI User note"
        ordering = ("-created_at",)
        get_latest_by = "created_at"

    def __str__(self):
        if not self.event_title:
            return f"[{self.pk}] {self.author} on {self.user}"
        return f"[{self.pk}] {self.author} on {self.user} ({self.event_title})"

    def get_external_ref(self):
        if not self.scope:
            return ""

        if not self.external_ref:
            return ""

        if not self.scope.external_ref_template:
            return self.external_ref

        return self.scope.external_ref_template.replace("{{ ref }}", self.external_ref)

    def get_external_ref_url(self):
        if not self.scope:
            return ""

        if not self.external_ref:
            return ""

        return self.scope.external_ref_url_template.replace(
            "{{ ref }}", self.external_ref
        )
