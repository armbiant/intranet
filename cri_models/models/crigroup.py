from django.db import models, transaction
from django.urls import reverse
from django.utils import timezone
from django.contrib.auth import get_user_model
from django.core.exceptions import ValidationError
from django.dispatch import receiver

from .. import ldap, fields, sync


class CRIAbstractMembership(models.Model):
    user = models.ForeignKey("CRIUser", on_delete=models.CASCADE)

    group = models.ForeignKey("CRIGroup", on_delete=models.CASCADE)

    graduation_year = fields.GraduationYearField(blank=True, null=True)

    begin_at = models.DateField(blank=True, null=True)

    end_at = models.DateField(blank=True, null=True)

    class Meta:
        abstract = True

    def __repr__(self):
        return f"<{type(self).__name__}: {self.group}/{self.user}>"

    def is_current(self):
        if self.begin_at and self.begin_at > timezone.now().date():
            return False
        if self.end_at and self.end_at < timezone.now().date():
            return False
        return True

    def clean_fields(self, exclude=None):
        super().clean_fields(exclude)
        if exclude is None or ("end_at" not in exclude and "begin_at" not in exclude):
            if self.end_at and self.begin_at and self.begin_at > self.end_at:
                raise ValidationError(
                    "'end_at' cannot have a value before 'begin_at'.",
                    code="invalid_interval",
                    params={"begin_at": self.begin_at, "end_at": self.end_at},
                )


class CRIMembership(CRIAbstractMembership):
    class Meta:
        verbose_name = "CRI Membership"

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._old_group_id = self.group_id
        self._old_user_id = self.user_id

    @transaction.atomic
    def save(self, *args, **kwargs):  # pylint: disable=arguments-differ
        super().save(*args, **kwargs)
        groups = CRIGroup.objects.filter(
            id__in=(
                self._old_group_id,
                self.group_id,
            )
        )
        if self._old_user_id and self._old_user_id != self.user_id:
            CRIComputedMembership.compute_entries(
                groups=groups,
                user=get_user_model().objects.get(pk=self._old_user_id),
            )
        CRIComputedMembership.compute_entries(
            groups=groups,
            user=self.user,
        )

    @transaction.atomic
    def delete(
        self, *args, update_computed_entries=True, **kwargs
    ):  # pylint: disable=arguments-differ
        super().delete(*args, **kwargs)
        if update_computed_entries:
            CRIComputedMembership.compute_entries(user=self.user)


class CRIComputedMembership(CRIAbstractMembership):
    user = models.ForeignKey(
        "CRIUser", on_delete=models.CASCADE, related_name="memberships"
    )

    group = models.ForeignKey(
        "CRIGroup", on_delete=models.CASCADE, related_name="memberships"
    )

    class Meta:
        verbose_name = "CRI Computed Membership"

    @classmethod
    def get_all_groups_to_update(cls, groups=None, user=None):
        """Returns groups to update and their parents"""
        if groups is not None:
            to_update = CRIGroup.objects.filter(
                models.Q(pk__in=[g.pk for g in groups])
                | models.Q(computed_children__in=groups)
            )
        else:
            to_update = CRIGroup.objects.all()
        if user:
            # Do not replace the individual queries with a unique Q object
            # query, PostgreSQL cannot optimize it efficiently.
            g1 = set(to_update.filter(primary_group_user=user))
            g2 = set(to_update.filter(crimembership__user=user))
            g3 = set(to_update.filter(memberships__user=user))
            to_update = g1 | g2 | g3

        return CRIGroup.objects.filter(
            models.Q(pk__in=[g.id for g in to_update])
            | models.Q(computed_children__in=to_update)
        ).distinct()

    @classmethod
    def empty_entries(cls, groups=None, user=None):
        """Delete all groups / users which will be computed"""
        to_delete = cls.objects.all()
        if groups is not None:
            to_delete = to_delete.filter(group__in=groups)
        if user:
            to_delete = to_delete.filter(user=user)
        to_delete.delete()

    @classmethod
    def _get_phony_memberships(cls, group, user=None):
        users = get_user_model().objects.filter(
            models.Q(primary_group=group)
            | models.Q(primary_group__computed_parents=group),
        )
        if user:
            users = users.filter(pk=user.pk)

        memberships = []
        for u in users:
            memberships.append(
                CRIMembership(
                    pk=f"invalid/{group}:{u}",  # required to make instance hashable
                    group=group,
                    user=u,
                    graduation_year=None,
                    begin_at=None,
                    end_at=None,
                )
            )
        return memberships

    @classmethod
    def get_merged_entry(cls, group, user=None):
        """Merged childen's entries for each (user, graduation_year) tuple into
        a single entry"""
        memberships = CRIMembership.objects.filter(
            models.Q(group=group) | models.Q(group__computed_parents=group)
        )
        if user is not None:
            memberships = memberships.filter(user=user)
        memberships = list(memberships) + cls._get_phony_memberships(group, user)

        entries = {}
        for entry in memberships:
            key = (entry.user, entry.graduation_year)
            entries.setdefault(key, set()).add(entry)

        new_entries = []
        for (u, graduation_year), entry_set in entries.items():
            begin_at_dates = set(entry.begin_at for entry in entry_set)
            end_at_dates = set(entry.end_at for entry in entry_set)
            if None in begin_at_dates:
                begin_at = None
            else:
                begin_at = min(filter(None, begin_at_dates))
            if None in end_at_dates:
                end_at = None
            else:
                end_at = max(filter(None, end_at_dates))
            new_entries.append(
                cls(
                    user=u,
                    group=group,
                    graduation_year=graduation_year,
                    begin_at=begin_at,
                    end_at=end_at,
                )
            )
        return new_entries

    @classmethod
    def create_computed_entries(cls, groups, user=None):
        """Create all computed entries for each user / group / computed groups"""
        new_entries = []
        for group in set(groups):
            new_entries += list(cls.get_merged_entry(group, user))
        cls.objects.bulk_create(new_entries)

    @classmethod
    def compute_entries(cls, groups=None, user=None):
        groups_to_update = set(cls.get_all_groups_to_update(groups=groups, user=user))
        cls.empty_entries(groups=groups_to_update, user=user)
        cls.create_computed_entries(groups_to_update, user)


class CRICurrentComputedMembershipManager(models.Manager):
    def get_queryset(self):
        now = timezone.now().date()
        return (
            super()
            .get_queryset()
            .filter(
                models.Q(end_at__gte=now) | models.Q(end_at__isnull=True),
                models.Q(begin_at__lte=now) | models.Q(begin_at__isnull=True),
            )
        )


class CRICurrentComputedMembership(CRIComputedMembership):
    objects = CRICurrentComputedMembershipManager()

    class Meta:
        proxy = True
        verbose_name = "CRI Current Computed Memberships"
        verbose_name_plural = "CRI Current Computed Memberships"


class CRIGroupLDAPSyncAdapter(sync.SyncedLDAPModelAdapter):
    sync_remote_model = ldap.LDAPGroup

    def get_sync_remote_obj_id(self, _remote_objects):
        return self.obj.ldap_dn

    def get_sync_local_data(self):
        SYNC_FIELDS = ("gid",)
        data = {
            "ldap_dn": self.obj.ldap_dn,
            "mail_aliases": set(a.email for a in self.obj.mail_aliases.all()),
            "managers": set(u.ldap_dn for u in self.obj.get_managers() if u.ldap_dn),
            "members": set(u.ldap_dn for u in self.obj.get_members() if u.ldap_dn),
            "name": self.obj.slug,
            **{f: getattr(self.obj, f) for f in SYNC_FIELDS},
        }
        return (self.obj.pk, data)

    @staticmethod
    def get_sync_remote_data(remote_obj):
        SYNC_FIELDS = ("name", "gid")
        data = {
            "mail_aliases": set(remote_obj.mail_aliases),
            "managers": set(remote_obj.managers),
            "members": set(remote_obj.members),
            **{f: getattr(remote_obj, f) for f in SYNC_FIELDS},
        }
        return (remote_obj.dn, data)


class CRIGroup(sync.SyncedModelMixin, models.Model):
    KIND_CHOICES = (
        ("assistants", "assistants"),
        ("association", "association"),
        ("campus", "campus"),
        ("class", "class"),
        ("course", "course"),
        ("curriculum", "curriculum"),
        ("department", "department"),
        ("laboratory", "laboratory"),
        ("major", "major"),
        ("other", "other"),
        ("program", "program"),
        ("semester", "semester"),
        ("specialization", "specialization"),
        ("system", "system"),
        ("track", "track"),
        ("year", "year"),
    )

    slug = models.SlugField(unique=True)

    gid = fields.UnixIDField(
        unique=True,
        blank=True,
        null=True,
        help_text="Unix group ID",
        verbose_name="GID",
    )

    name = models.CharField(max_length=128)

    kind = models.CharField(max_length=128, choices=KIND_CHOICES, default="class")

    children = models.ManyToManyField(
        "self", blank=True, symmetrical=False, related_name="parents"
    )

    computed_children = models.ManyToManyField(
        "self",
        blank=True,
        editable=False,
        symmetrical=False,
        related_name="computed_parents",
    )

    next_group = models.ForeignKey(
        "self",
        on_delete=models.SET_NULL,
        blank=True,
        null=True,
        related_name="previous_groups",
        help_text=(
            "Group into which to move the current group members at the end of "
            "their membership"
        ),
    )

    managers = models.ManyToManyField(
        "CRIUser",
        blank=True,
        help_text="Users allowed to manage this group",
        related_name="managed_groups",
    )

    private = models.BooleanField(
        default=False, help_text="If true this group will only be shown to its members"
    )

    mail_aliases = models.ManyToManyField(
        "MailAlias",
        blank=True,
        help_text="Mail aliases that can be used by members of this group",
    )

    @property
    def ldap_dn(self):
        if not self.slug:
            raise ValueError(f"Group '{self}' has no slug")
        for adapter in self.get_sync_adapters():
            remote_model = adapter.get_sync_remote_model()
            if issubclass(remote_model, ldap.LDAPModel):
                return remote_model(name=self.slug).build_dn()
        raise ValueError(f"Group '{self}' has no LDAP counterpart")

    class Meta:
        ordering = ("kind", "name", "slug")
        verbose_name = "CRI Group"

    class SyncMeta:
        sync_adapters = (CRIGroupLDAPSyncAdapter,)

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse("crigroup_detail", kwargs={"slug": self.slug})

    def get_members(self):
        return (
            get_user_model()
            .objects.filter(
                models.Q(memberships__end_at__gte=timezone.now())
                | models.Q(memberships__end_at__isnull=True),
                models.Q(memberships__begin_at__lte=timezone.now())
                | models.Q(memberships__begin_at__isnull=True),
                memberships__group=self,
            )
            .distinct()
        )

    def get_managers(self):
        return (
            get_user_model()
            .objects.filter(
                models.Q(managed_groups=self)
                | models.Q(managed_groups__computed_children=self)
            )
            .distinct()
        )

    def compute_all_children(self):
        groups = set(self.children.all())
        children = set()
        while groups:
            group = groups.pop()
            if group not in children and group != self:
                groups.update(group.children.all())
                children.add(group)
        return children

    def update_computed_entries(self):
        to_update = set(self.computed_parents.all()) | set([self])
        for parent in to_update:
            children = parent.compute_all_children()
            parent.computed_children.set(children)
        to_update |= set(self.computed_parents.all())
        CRIComputedMembership.compute_entries(groups=to_update, user=None)

    @classmethod
    def get_groups_user_can_see(cls, user):
        if user.has_perm("cri_models.view_crigroup"):
            return cls.objects.all()
        return cls.objects.filter(
            models.Q(memberships__user=user) | models.Q(private=False)
        ).distinct()


@receiver(models.signals.m2m_changed, sender=CRIGroup.children.through)
def update_group_computed_entries(instance, action, reverse, model, pk_set, **_kwargs):
    if action in ("post_add", "post_remove", "post_clear"):
        if reverse:
            for obj in model.objects.filter(pk__in=pk_set):
                obj.update_computed_entries()
        else:
            instance.update_computed_entries()


class Campus(models.Model):
    group = models.OneToOneField(CRIGroup, on_delete=models.CASCADE)

    class Meta:
        verbose_name_plural = "campuses"
        ordering = ("group__name",)

    def __str__(self):
        return self.group.name

    @property
    def slug(self):
        # Removing this property breaks cri_api.serializers.CampusSerializer.
        return self.group.slug
