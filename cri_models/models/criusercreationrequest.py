from django.db import models, transaction
from django.conf import settings
from django.utils import timezone
from django.contrib.auth import get_user_model

from phonenumber_field.modelfields import PhoneNumberField

from .criuser import CRIUser, KerberosPrincipal
from .crigroup import CRIGroup

from .. import fields

import logging

_logger = logging.getLogger(__name__)


class CRIUserCreationRequest(models.Model):
    username = models.CharField(blank=True, max_length=100)

    email = models.EmailField(blank=True)

    first_name = models.CharField(
        max_length=100, blank=True, help_text="commonly used first name"
    )

    last_name = models.CharField(
        max_length=100, blank=True, help_text="commonly used last name"
    )

    uid = fields.UnixIDField(
        blank=True, null=True, help_text="Unix user ID", verbose_name="UID"
    )

    primary_group = models.ForeignKey(
        "CRIGroup",
        on_delete=models.SET_DEFAULT,
        blank=True,
        null=True,
        default=CRIUser.DEFAULT_GROUP_PK,
        related_name="primary_group_usercreationrequest",
        limit_choices_to={"gid__isnull": False},
    )

    password = models.CharField(
        max_length=100,
        blank=True,
        help_text="user initial password",
    )

    birthdate = models.DateField(blank=True, null=True)

    phone = PhoneNumberField(blank=True, null=True, help_text="Mobile phone number")

    old_account = models.OneToOneField(
        CRIUser,
        on_delete=models.CASCADE,
        blank=True,
        null=True,
        related_name="new_account_usercreationrequest",
        help_text="Past account of this user",
    )

    created_user = models.ForeignKey(
        CRIUser,
        on_delete=models.SET_NULL,
        editable=False,
        blank=True,
        null=True,
        related_name="creationrequest",
    )

    created_at = models.DateTimeField(editable=False, blank=True, null=True)

    created_by = models.ForeignKey(
        get_user_model(),
        on_delete=models.SET_NULL,
        editable=False,
        blank=True,
        null=True,
        related_name="creationrequest_creator",
    )

    class Meta:
        verbose_name = "CRI User creation request"

    class CreationFailed(RuntimeError):
        pass

    class MissingRequiredField(CreationFailed):
        pass

    def __str__(self):
        if self.username:
            return self.username
        if self.email:
            return self.email
        if self.uid:
            return f"uid:{self.uid}"
        return f"pk:{self.pk}"

    def get_data(self):
        fields = (
            "birthdate",
            "email",
            "first_name",
            "last_name",
            "old_account",
            "phone",
            "primary_group",
            "uid",
            "username",
        )
        data = {
            field: getattr(self, field)
            for field in fields
            if getattr(self, field) is not None
        }

        if not data.get("username") and data.get("email", "").endswith("@epita.fr"):
            data["username"] = data.get("email", "").split("@epita.fr")[0]

        for name in ("first_name", "last_name"):
            if data.get(name):
                data[f"legal_{name}"] = data[name] = data[name].title()

        if data.get("uid") is None and data.get("primary_group"):
            next_group = (
                CRIGroup.objects.filter(gid__gt=data["primary_group"].gid)
                .order_by("gid")
                .first()
            )

            qs = CRIUser.objects.filter(uid__gt=data["primary_group"].gid)
            if next_group:
                qs = qs.filter(uid__lt=next_group.gid)

            previous_user = qs.order_by("-uid").first()
            if previous_user:
                uid = previous_user.uid + 1
            else:
                uid = data["primary_group"].gid + 1

            if not next_group or uid < next_group.gid:
                data["uid"] = uid

        return data

    @transaction.atomic()
    def create(self, creator=None):
        need_save = False

        data = self.get_data()

        for required_attr in ("username", "primary_group", "uid", "last_name"):
            if not data.get(required_attr):
                raise self.MissingRequiredField(f"field '{required_attr}' is missing!")

        user = CRIUser.objects.create(**data)

        for realm in settings.KERBEROS_REALMS:
            if not settings.KERBEROS_REALMS[realm]["NEWPRINC_ON_USER_CREATION"]:
                continue
            principal, _ = KerberosPrincipal.objects.get_or_create(
                principal=f'{data["username"]}@{realm}',
                user=user,
            )
            principal.sync()
            if realm == settings.KERBEROS_DEFAULT_REALM:
                user.primary_principal = principal
                need_save = True

        if self.password:
            user.set_password(self.password)
            need_save = True

        if need_save:
            user.save()

        user.sync()

        self.created_user = user
        self.created_at = timezone.now()
        if creator:
            self.created_by = creator
        self.save()

        return user
