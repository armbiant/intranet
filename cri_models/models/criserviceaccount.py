from django.db import models
from django.contrib.auth import get_user_model

from .criuser import CRIUser

import logging

_logger = logging.getLogger(__name__)


class CRIServiceAccount(CRIUser):
    owner = models.ForeignKey(
        get_user_model(),
        on_delete=models.CASCADE,
        related_name="criserviceaccount_owner",
        limit_choices_to={"criserviceaccount": None},
    )

    class Meta:
        verbose_name = "CRI Service Account"
        verbose_name_plural = "CRI Service Accounts"

    class SyncMeta:
        sync_adapters = ()

    # pylint: disable=arguments-differ
    def sync(self, *args, **kwargs):
        return self.criuser_ptr.sync(*args, **kwargs)

    def _lock_fields(self):
        self.is_staff = False
        self.is_superuser = False

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._lock_fields()

    # pylint: disable=arguments-differ
    def refresh_from_db(self, *args, **kwargs):
        super().refresh_from_db(*args, **kwargs)
        self._lock_fields()

    def is_auth_allowed(self):
        if not self.is_active:
            _logger.info("User '%s' is disabled", self.username)
            return False

        _logger.debug(
            "User '%s' is a service account, checking if owner is allowed to "
            "authenticate",
            self.username,
        )
        return self.owner.is_auth_allowed()

    def get_managers(self):
        return get_user_model().objects.filter(pk=self.owner.pk)
