from django.db import models, transaction
from django.core.exceptions import ValidationError
from django.contrib.auth.models import AbstractUser
from django.utils import timezone
from django.urls import reverse

from phonenumber_field.modelfields import PhoneNumberField

from .crigroup import CRIGroup, CRIComputedMembership
from .. import ldap, kerberos, fields, sync

from datetime import datetime
from unidecode import unidecode
import logging

_logger = logging.getLogger(__name__)


class CRIUserLDAPSyncAdapter(sync.SyncedLDAPModelAdapter):
    sync_remote_model = ldap.LDAPUser

    def get_sync_remote_obj_id(self, _remote_objects):
        return self.obj.ldap_dn

    def get_sync_local_data(self):
        SYNC_FIELDS = ("email", "first_name", "ldap_dn", "uid")
        data = {
            "full_name": self.obj.get_full_name(),
            "home": f"/home/{self.obj.username}",
            "last_name": self.obj.last_name or self.obj.username,
            "login": self.obj.username,
            "passwords": set(),
            **{f: getattr(self.obj, f) for f in SYNC_FIELDS},
        }
        if self.obj.primary_principal:
            data["passwords"].add(f"{{SASL}}{self.obj.primary_principal}")
        if self.obj.is_auth_allowed() and self.obj.password.startswith("argon2$"):
            data["passwords"].add(self.obj.password.replace("argon2$", "{ARGON2}$"))
        if self.obj.primary_group.gid:
            data["gid"] = self.obj.primary_group.gid
        if self.obj.is_active:
            data["ssh_keys"] = set(
                k.key.keydata for k in self.obj.sshpublickey_set.all()
            )
        else:
            data["ssh_keys"] = set()
        data["mail_aliases"] = set(a.email for a in self.obj.mail_aliases.all())
        return (self.obj.pk, data)

    @staticmethod
    def get_sync_remote_data(remote_obj):
        SYNC_FIELDS = (
            "email",
            "first_name",
            "full_name",
            "gid",
            "home",
            "last_name",
            "login",
            "uid",
        )
        data = {
            "ssh_keys": set(remote_obj.ssh_keys),
            "mail_aliases": set(remote_obj.mail_aliases),
            "passwords": set(remote_obj.passwords),
            **{f: getattr(remote_obj, f) for f in SYNC_FIELDS},
        }
        return (remote_obj.dn, data)

    @classmethod
    def get_sync_local_queryset(_cls, model):
        return model.objects.filter(ldap_dn__isnull=False).exclude(ldap_dn="")


class CRIUser(sync.SyncedModelMixin, AbstractUser):
    PICTURE_BASE = "https://photos.cri.epita.fr"
    LDAP_AUTH_DN_FIELD = "ldap_dn"
    REQUIRED_FIELDS = ("email", "uid")
    DEFAULT_GROUP_PK = 1

    username = models.CharField(primary_key=True, max_length=100)

    first_name = models.CharField(
        max_length=100, blank=True, help_text="commonly used first name"
    )

    last_name = models.CharField(
        max_length=100, blank=False, help_text="commonly used last name"
    )

    legal_first_name = models.CharField(
        max_length=100, blank=True, help_text="first name to be used on legal documents"
    )

    legal_last_name = models.CharField(
        max_length=100, blank=True, help_text="last name to be used on legal documents"
    )

    uid = fields.UnixIDField(unique=True, help_text="Unix user ID", verbose_name="UID")

    primary_group = models.ForeignKey(
        "CRIGroup",
        on_delete=models.SET_DEFAULT,
        default=DEFAULT_GROUP_PK,
        related_name="primary_group_user",
        limit_choices_to={"gid__isnull": False},
    )

    birthdate = models.DateField(blank=True, null=True)

    phone = PhoneNumberField(blank=True, null=True, help_text="Mobile phone number")

    old_account = models.OneToOneField(
        "self",
        on_delete=models.CASCADE,
        blank=True,
        null=True,
        related_name="new_account",
        help_text="Past account of this user",
    )

    ldap_dn = ldap.LDAPDNField(
        ldap.LDAPUser,
        max_length=254,
        blank=True,
        null=True,
        unique=True,
        verbose_name="LDAP DN",
    )

    primary_principal = models.ForeignKey(
        "KerberosPrincipal",
        on_delete=models.SET_NULL,
        null=True,
        blank=True,
        help_text="Primary kerberos principal of this user",
    )

    mail_aliases = models.ManyToManyField(
        "MailAlias", blank=True, help_text="Mail aliases that can be used by this user"
    )

    class Meta(AbstractUser.Meta):
        verbose_name = "CRI User"
        verbose_name_plural = "CRI Users"
        permissions = (
            ("impersonate", "Can impersonate users"),
            ("view_criuser_legal_identity", "Can view legal identies"),
            ("view_criuser_phone", "can view phone numbers"),
            ("view_criuser_birthdate", "Can view birthdates"),
            ("update_criuser_password", "Can change passwords"),
            ("export_data", "Can export user data"),
        )

    class SyncMeta:
        sync_adapters = (CRIUserLDAPSyncAdapter,)

    @property
    def picture_full(self):
        return f"{self.PICTURE_BASE}/{self.username}"

    @property
    def picture_thumb(self):
        return f"{self.PICTURE_BASE}/thumb/{self.username}"

    @property
    def picture_square(self):
        return f"{self.PICTURE_BASE}/square/{self.username}"

    @property
    def printable_name(self):
        return (
            " ".join(filter(None, (self.last_name.upper(), self.first_name)))
            or self.username
        )

    @property
    def fs_name(self):
        return unidecode(f"{self.printable_name}_{self.uid}").replace(" ", "_")

    @property
    def last_activity(self):
        last_activity = None

        last_session = self.legacysessionlogentry_set.all()
        last_entry = (
            self.connectionlogentry_set.all()
            .filter(status="success")
            .order_by("-created_at")
            .first()
        )
        if last_entry:
            last_activity = last_entry.created_at
            last_session = last_session.filter(end_at__gt=last_activity)

        last_session = last_session.order_by("-end_at").first()
        if last_session:
            last_activity = last_session.end_at

        return last_activity

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._old_primary_group_id = self.primary_group_id
        self._old_is_active = self.is_active

    def is_auth_allowed(self):
        if hasattr(self, "criserviceaccount"):
            return self.criserviceaccount.is_auth_allowed()
        if not self.is_active:
            _logger.info("User '%s' is disabled", self.username)
            return False
        return True

    def get_full_name(self):
        return super().get_full_name() or self.username

    def get_absolute_url(self):
        return reverse("criuser_detail", kwargs={"username": self.username})

    def get_old_accounts(self):
        old_accounts = []
        current = self
        while hasattr(current, "old_account") and current.old_account:
            current = current.old_account
            if current in old_accounts:
                _logger.warning(
                    "User '%s' is part of an old_accounts loop: %s",
                    str(self),
                    ", ".join(map(str, old_accounts)),
                )
                break
            old_accounts.append(current)
        return old_accounts

    def get_new_account(self):
        new_accounts = [self]
        current = self
        while hasattr(current, "new_account") and current.new_account:
            current = current.new_account
            if current in new_accounts:
                _logger.warning(
                    "User '%s' is part of an old_accounts loop: %s",
                    str(self),
                    ", ".join(map(str, new_accounts)),
                )
                return None
            new_accounts.append(current)
        if current is not self:
            return current

    def get_groups(self):
        return CRIGroup.objects.filter(
            models.Q(memberships__end_at__gte=timezone.now())
            | models.Q(memberships__end_at__isnull=True),
            models.Q(memberships__begin_at__lte=timezone.now())
            | models.Q(memberships__begin_at__isnull=True),
            memberships__user=self,
        ).distinct()

    def get_past_groups(self):
        return (
            CRIGroup.objects.filter(
                memberships__end_at__lt=timezone.now(), memberships__user=self
            )
            .exclude(
                models.Q(memberships__begin_at__gte=timezone.now())
                | models.Q(memberships__end_at__isnull=True)
            )
            .distinct()
        )

    def get_future_groups(self):
        return (
            CRIGroup.objects.filter(
                memberships__begin_at__gt=timezone.now(), memberships__user=self
            )
            .exclude(memberships__end_at__lte=timezone.now())
            .distinct()
        )

    def _sync_kerberos_password(self, password, raise_on_primary_error=True):
        principals = set(self.kerberosprincipal_set.filter(out_of_date=True))
        for principal in principals:
            with transaction.atomic():
                try:
                    principal.set_and_save_password(password)
                except Exception as e:
                    _logger.error(
                        "a problem occured during the password synchronization "
                        "of principal '%s' for user '%s': %s",
                        principal,
                        self.username,
                        e,
                    )
                    if raise_on_primary_error and principal == self.primary_principal:
                        raise

    def check_password(self, raw_password):
        if not super().check_password(raw_password):
            return False
        self._sync_kerberos_password(raw_password, raise_on_primary_error=False)
        return True

    def clean_fields(self, exclude=None):
        super().clean_fields(exclude)
        if exclude is None or "primary_principal" not in exclude:
            primary = self.primary_principal
            if primary and primary.user is not None and primary.user != self:
                raise ValidationError(
                    "Principal '%(principal)s' is the primary principal of"
                    "'%(user1)s' but is assigned to '%(user2)s'!",
                    code="principal_owned_by_another_user",
                    params={
                        "principal": primary.principal,
                        "user1": primary.user,
                        "user2": self,
                    },
                )

    @transaction.atomic
    # pylint: disable=arguments-differ
    def save(self, *args, raise_on_primary_error=True, **kwargs):
        if not self.ldap_dn:
            ldap_dn = ldap.LDAPUser(login=self.username).build_dn()
            if type(self).objects.filter(ldap_dn=ldap_dn).exists():
                _logger.warning(
                    "User '%s' saved without ldap_dn because another user "
                    "is already assigned '%s!",
                    str(self),
                    ldap_dn,
                )
            else:
                self.ldap_dn = ldap_dn
        if self.primary_principal:
            self.primary_principal.user = self
            self.primary_principal.save()
        else:
            principals = self.kerberosprincipal_set.all()
            if len(principals) == 1:
                self.primary_principal = principals[0]
        password = self._password
        super().save(*args, **kwargs)
        if self.primary_group_id != self._old_primary_group_id:
            CRIComputedMembership.compute_entries(
                groups=CRIGroup.objects.filter(
                    id__in=(self.primary_group_id, self._old_primary_group_id),
                ),
                user=self,
            )
        if password:
            self.kerberosprincipal_set.update(out_of_date=True)
            self._sync_kerberos_password(
                password, raise_on_primary_error=raise_on_primary_error
            )
            self.sync()
            self._password = None
        elif self.is_active != self._old_is_active:
            self.sync()
            KerberosPrincipal.partial_sync(self.kerberosprincipal_set.all())
        self._old_primary_group_id = self.primary_group_id
        self._old_is_active = self.is_active


class SSHPublicKey(models.Model):
    user = models.ForeignKey(CRIUser, on_delete=models.CASCADE)

    title = models.CharField(blank=True, max_length=128)

    key = fields.SSHPublicKeyField(unique=True)

    key_type = models.CharField(max_length=128, editable=False)

    key_length = models.PositiveIntegerField(editable=False)

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = "SSH public key"
        ordering = ("user", "title")
        unique_together = (("user", "title"),)

    def save(self, *args, do_sync=True, **kwargs):  # pylint: disable=arguments-differ
        if not self.title and self.key.comment:
            self.title = self.key.comment
        self.key_type = self.key.key_type.decode("ascii")
        self.key_length = self.key.bits
        super().save(*args, **kwargs)
        if do_sync:
            self.user.sync()

    def delete(self, *args, do_sync=True, **kwargs):  # pylint: disable=arguments-differ
        user = self.user
        super().delete(*args, **kwargs)
        if do_sync:
            user.sync()


class KerberosPrincipalSyncAdapter(sync.SyncedKerberosPrincipalAdapter):
    def get_sync_remote_obj_id(self, _remote_objects):
        return self.obj.principal

    def get_sync_local_data(self):
        data = {"principal": self.obj.principal}
        if self.obj.user:
            if self.obj.user.is_active:
                data["expire_at"] = None
            else:
                data["expire_at"] = datetime(1970, 1, 1)
        return self.obj.pk, data

    @staticmethod
    def get_sync_remote_data(remote_obj):
        SYNC_FIELDS = ("expire_at",)
        data = {f: getattr(remote_obj, f) for f in SYNC_FIELDS}
        return remote_obj.pk, data


class KerberosPrincipal(sync.SyncedModelMixin, models.Model):
    user = models.ForeignKey(CRIUser, on_delete=models.CASCADE, blank=True, null=True)

    principal = kerberos.fields.KerberosPrincipalField(max_length=100, unique=True)

    out_of_date = models.BooleanField(default=True)

    class SyncMeta:
        sync_adapters = (KerberosPrincipalSyncAdapter,)

    @transaction.atomic
    def save(self, *args, **kwargs):  # pylint: disable=arguments-differ
        super().save(*args, **kwargs)
        # If we moved a principal away from a user for which it was its primary
        # principal, we must also empty the user's primary_principal attribute.
        old_user = CRIUser.objects.filter(primary_principal=self).first()
        if old_user and self.user != old_user:
            old_user.primary_principal = None
            old_user.save()

    def set_and_save_password(self, password, update_out_of_date_status=True):
        principal = kerberos.Principal.get(self.principal)
        principal.set_password(password)
        principal.save()
        if update_out_of_date_status:
            self.out_of_date = False
            self.save()

    def __str__(self):
        return str(self.principal)
