#!/usr/bin/env python3

import yaml

from itertools import chain

CAMPUSES = (
    ("prs", "Paris"),
    ("lyn", "Lyon"),
    ("rns", "Rennes"),
    ("stg", "Strasbourg"),
    ("tls", "Toulouse"),
)

PREPA_CAMPUSES = tuple(dict(CAMPUSES).keys())
ING_CAMPUSES = ("prs",)

CURRICULUMS = (
    ("prepa", "Cycle préparatoire"),
    ("ing", "Cycle ingénieur"),
    ("inter", "Parcours internationaux"),
)

YEARS = (
    ("prepa", "sup", "INFO SUP", PREPA_CAMPUSES),
    ("prepa", "spe", "INFO SPÉ", PREPA_CAMPUSES),
    ("ing", "ing1", "ING1", ING_CAMPUSES),
    ("ing", "ing2", "ING2", ING_CAMPUSES),
    ("ing", "ing3", "ING3", ING_CAMPUSES),
)

SEMESTERS = (
    ("prepa-sup", "s1", "S1", PREPA_CAMPUSES),
    ("prepa-sup", "s1s", "S1#", ING_CAMPUSES),
    ("prepa-sup", "s2", "S2", PREPA_CAMPUSES),
    ("prepa-sup", "s2s", "S2#", ING_CAMPUSES),
    ("prepa-spe", "s3", "S3", PREPA_CAMPUSES),
    ("prepa-spe", "s3s", "S3#", ING_CAMPUSES),
    ("prepa-spe", "s4", "S4", PREPA_CAMPUSES),
    ("ing-ing1", "s5", "S5", ING_CAMPUSES),
    ("ing-ing1", "s6", "S6", ING_CAMPUSES),
    ("ing-ing2", "s7", "S7", ING_CAMPUSES),
    ("ing-ing2", "s8", "S8", ING_CAMPUSES),
    ("ing-ing3", "s9", "S9", ING_CAMPUSES),
    ("ing-ing3", "s10", "S10", ING_CAMPUSES),
)

MAJORS = (
    ("ing", "csi", "CSI"),
    ("ing", "rdi", "RDI"),
    ("ing", "gistre", "GISTRE"),
    ("ing", "gitm", "GITM"),
    ("ing", "image", "IMAGE"),
    ("ing", "mti", "MTI"),
    ("ing", "scia", "SCIA"),
    ("ing", "sigl", "SIGL"),
    ("ing", "srs", "SRS"),
    ("ing", "tcom", "TCOM"),
)

EXTRA_GROUPS = {
    "acu": {"name": "Assistants C/UNIX (ACU)", "kind": "assistants"},
    "yaka": {
        "name": "Yet another kind of assistant (YAKA)",
        "kind": "assistants",
    },
    "acdc": {"name": "Assistants C#/Caml (ACDC)", "kind": "assistants"},
    "asm": {"name": "Assistants salle machines (ASM)", "kind": "assistants"},
    "cri": {
        "name": "Centre des ressources informatiques (CRI)",
        "kind": "department",
    },
    "labo-3ie": {
        "name": "Institut d'innovation informatique de l'EPITA (3IE)",
        "kind": "laboratory",
    },
    "labo-lse": {
        "name": "Laboratoire sécurité & système de l'EPITA (LSE)",
        "kind": "laboratory",
    },
    "labo-lse-ia": {
        "name": "LSE - département IA",
        "kind": "other",
        "parents": ["labo-lse"],
    },
    "labo-lse-secu": {
        "name": "LSE - département sécurité",
        "kind": "other",
        "parents": ["labo-lse"],
    },
    "labo-lse-sys": {
        "name": "LSE - département système",
        "kind": "other",
        "parents": ["labo-lse"],
    },
    "labo-lrde": {
        "name": "Laboratoire de recherche et de développement de l'EPITA (LRDE)",
        "kind": "laboratory",
    },
    "labo-seal": {
        "name": "Sense explore analyse and learn (SEAL)",
        "kind": "laboratory",
    },
    "prepa-spe-api-s3-prs": {
        "name": "API S3 paris",
        "kind": "other",
        "parents": ["prepa-spe-s3-prs"],
    },
    "prepa-spe-api-s4-prs": {
        "name": "API S4 paris",
        "kind": "other",
        "parents": ["prepa-spe-s4-prs"],
    },
    "prepa-spe-s4-inter": {
        "name": "S4 internationnal",
        "kind": "other",
        "parents": ["prepa-spe-s4"],
    },
    "ing-ing1-arcs": {"name": "ING1 ARCS", "kind": "other"},
    "ing-ing1-arcs-s5-prs": {
        "name": "ING1 ARCS S5 Paris",
        "kind": "other",
        "parents": ["ing-ing1-arcs", "ing-ing1-s5-prs"],
    },
    "ing-ing1-arcs-s6-prs": {
        "name": "ING1 ARCS S6 Paris",
        "kind": "other",
        "parents": ["ing-ing1-arcs", "ing-ing1-s6-prs"],
    },
    "ing-app-i1-s5-prs": {
        "name": "Apprentis ING S5 Paris",
        "kind": "other",
        "parents": ["ing-ing1-s5-prs"],
    },
    "ing-app-x1-s5-prs": {
        "name": "Apprentis Experts S5 Paris",
        "kind": "other",
        "parents": ["ing-ing1-s5-prs"],
    },
    "ing-app-i1-s6-prs": {
        "name": "Apprentis ING S6 Paris",
        "kind": "other",
        "parents": ["ing-ing1-s6-prs"],
    },
    "ing-app-x1-s6-prs": {
        "name": "Apprentis Experts S6 Paris",
        "kind": "other",
        "parents": ["ing-ing1-s6-prs"],
    },
    "ing-app-i2-s7-prs": {
        "name": "Apprentis ING S7 Paris",
        "kind": "other",
        "parents": ["ing-ing2-s7-prs"],
    },
    "ing-app-x2-s7-prs": {
        "name": "Apprentis Experts S7 Paris",
        "kind": "other",
        "parents": ["ing-ing2-s7-prs"],
    },
    "ing-app-i2-s8-prs": {
        "name": "Apprentis ING S8 Paris",
        "kind": "other",
        "parents": ["ing-ing2-s8-prs"],
    },
    "ing-app-x2-s8-prs": {
        "name": "Apprentis Experts S8 Paris",
        "kind": "other",
        "parents": ["ing-ing2-s8-prs"],
    },
    "ing-app-i3-s9-prs": {
        "name": "Apprentis ING S9 Paris",
        "kind": "other",
        "parents": ["ing-ing3-s9-prs"],
    },
    "ing-app-x3-s9-prs": {
        "name": "Apprentis Experts S9 Paris",
        "kind": "other",
        "parents": ["ing-ing3-s9-prs"],
    },
    "ing-app-i3-s10-prs": {
        "name": "Apprentis ING S10 Paris",
        "kind": "other",
        "parents": ["ing-ing3-s10-prs"],
    },
    "ing-app-x3-s10-prs": {
        "name": "Apprentis Experts S10 Paris",
        "kind": "other",
        "parents": ["ing-ing3-s10-prs"],
    },
}


class Group:
    _LAST_PK = [6]
    _GROUPS = {}

    @classmethod
    def get(cls, slug):
        return cls._GROUPS[slug]

    @classmethod
    def all(cls):
        return cls._GROUPS.values()

    def __init__(self, slug, name, kind):
        self.slug = slug
        self.name = name
        self.kind = kind
        self.children = []
        type(self)._LAST_PK[0] += 1
        self.pk = type(self)._LAST_PK[0]
        type(self)._GROUPS[self.slug] = self

    def to_fixture(self):
        return [
            {
                "model": "cri_models.CRIGroup",
                "pk": self.pk,
                "fields": {
                    "slug": self.slug,
                    "name": self.name,
                    "kind": self.kind,
                    "children": sorted([c.pk for c in self.children]),
                },
            }
        ]


class Campus(Group):
    _LAST_CAMPUS_PK = 0

    def __init__(self, *args, **kwargs):
        super().__init__(*args, kind="campus", **kwargs)
        type(self)._LAST_CAMPUS_PK += 1
        self.campus_pk = type(self)._LAST_CAMPUS_PK

    def to_fixture(self):
        fixture = super().to_fixture()
        fixture.append(
            {
                "model": "cri_models.Campus",
                "pk": self.campus_pk,
                "fields": {"group": self.pk},
            }
        )
        return fixture


def generate_groups():
    for slug, name in CAMPUSES:
        Campus(slug=slug, name=name)

    for slug, name in CURRICULUMS:
        Group(slug=slug, name=name, kind="curriculum")

    for kind, data in (("year", YEARS), ("semester", SEMESTERS)):
        for parent, slug, name, campuses in data:
            new_slug = f"{parent}-{slug}"
            group = Group(slug=new_slug, name=name, kind=kind)
            Group.get(parent).children.append(group)
            for campus in map(Group.get, campuses):
                new_slug = f"{parent}-{slug}-{campus.slug}"
                new_name = f"{name} {campus.name}"
                tmp_group = Group(slug=new_slug, name=new_name, kind="other")
                try:
                    parent_group = Group.get(f"{parent}-{campus.slug}")
                except KeyError:
                    parent_group = campus
                parent_group.children.append(tmp_group)
                group.children.append(tmp_group)

    for kind, data in (("major", MAJORS),):
        for parent, slug, name in data:
            new_slug = f"{parent}-{slug}"
            group = Group(slug=new_slug, name=name, kind=kind)
            Group.get(parent).children.append(group)

    for slug, data in EXTRA_GROUPS.items():
        group = Group(slug=slug, name=data["name"], kind=data["kind"])
        for parent in data.get("parents", []):
            Group.get(parent).children.append(group)

    return Group.all()


def convert_to_fixtures(groups):
    return yaml.dump(list(chain(*[g.to_fixture() for g in groups])))


if __name__ == "__main__":
    print(convert_to_fixtures(generate_groups()))
