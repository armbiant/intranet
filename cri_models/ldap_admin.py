from django.contrib import admin

from . import ldap, ldap_forms


class LDAPModelAdmin(admin.ModelAdmin):
    def get_readonly_fields(self, request, obj=None):
        if obj:
            return list(super().get_readonly_fields(request, obj)) + list(
                ldap.LDAPModel.RO_FIELDS
            )
        return super().get_readonly_fields(request, obj)


@admin.register(ldap.LDAPGroup)
class LDAPGroupAdmin(LDAPModelAdmin):
    form = ldap_forms.LDAPGroupForm
    list_display = ("name", "gid", "dn")
    list_display_links = list_display
    search_fields = ("name",)


@admin.register(ldap.LDAPUser)
class LDAPUserAdmin(LDAPModelAdmin):
    list_display = (
        "login",
        "email",
        "first_name",
        "last_name",
        "uid",
        "gid",
        "dn",
    )
    list_display_links = list_display
    list_filter = ("gid",)
    search_fields = ("login", "email", "first_name", "last_name")
    exclude = ("ssh_keys",)
