# Generated by Django 3.2.5 on 2021-07-31 10:04

import cri_models.fields
from django.conf import settings
import django.contrib.auth.models
from django.db import migrations, models
import django.db.models.deletion
import ldapdb.models.fields


class Migration(migrations.Migration):

    dependencies = [
        ("cri_models", "0015_auto_20210730_1112"),
    ]

    operations = [
        migrations.CreateModel(
            name="CRIServiceAccount",
            fields=[
                (
                    "criuser_ptr",
                    models.OneToOneField(
                        auto_created=True,
                        on_delete=django.db.models.deletion.CASCADE,
                        parent_link=True,
                        primary_key=True,
                        serialize=False,
                        to="cri_models.criuser",
                    ),
                ),
                (
                    "owner",
                    models.ForeignKey(
                        limit_choices_to={"criserviceaccount": None},
                        on_delete=django.db.models.deletion.CASCADE,
                        related_name="criserviceaccount_owner",
                        to=settings.AUTH_USER_MODEL,
                    ),
                ),
            ],
            options={
                "verbose_name": "CRI Service Account",
                "verbose_name_plural": "CRI Service Accounts",
            },
            bases=("cri_models.criuser",),
            managers=[
                ("objects", django.contrib.auth.models.UserManager()),
            ],
        ),
    ]
