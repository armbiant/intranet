# Generated by Django 3.1.2 on 2020-10-18 11:35

import cri_models.fields
from django.db import migrations, models
import ldapdb.models.fields


class Migration(migrations.Migration):

    dependencies = [("cri_models", "0006_auto_20201009_1634")]

    operations = [
        migrations.AlterField(
            model_name="crigroup",
            name="kind",
            field=models.CharField(
                choices=[
                    ("assistants", "assistants"),
                    ("association", "association"),
                    ("campus", "campus"),
                    ("class", "class"),
                    ("course", "course"),
                    ("curriculum", "curriculum"),
                    ("department", "department"),
                    ("laboratory", "laboratory"),
                    ("major", "major"),
                    ("other", "other"),
                    ("program", "program"),
                    ("semester", "semester"),
                    ("specialization", "specialization"),
                    ("system", "system"),
                    ("track", "track"),
                    ("year", "year"),
                ],
                default="class",
                max_length=128,
            ),
        )
    ]
