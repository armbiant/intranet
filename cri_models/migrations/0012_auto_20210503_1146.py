# Generated by Django 3.1.7 on 2021-05-03 09:46

import cri_models.fields
from django.db import migrations, models
import ldapdb.models.fields


class Migration(migrations.Migration):

    dependencies = [
        ("cri_models", "0011_auto_20210503_1128"),
    ]

    operations = [
        migrations.AlterField(
            model_name="sshpublickey",
            name="key_type",
            field=models.CharField(editable=False, max_length=128),
        ),
    ]
