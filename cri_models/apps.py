from django.apps import AppConfig


class CRIModelsConfig(AppConfig):
    name = "cri_models"
    verbose_name = "CRI models"
