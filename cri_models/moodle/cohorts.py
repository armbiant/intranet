import xml.parsers.expat

from .base import MoodleObject


class MoodleCohort(MoodleObject):
    def __init__(self, proxy, obj_id, cohort_info):
        super().__init__(proxy, obj_id)
        self.cohort_info = cohort_info
        if not hasattr(self, "_members"):
            self._members = None

    @property
    def idnumber(self):
        return self.cohort_info.get("idnumber")

    @idnumber.setter
    def idnumber(self, value):
        self.update({"idnumber": value})
        return value

    @property
    def name(self):
        return self.cohort_info.get("name")

    @name.setter
    def name(self, value):
        self.update({"name": value})
        return value

    @property
    def descriptionformat(self):
        return self.cohort_info.get("descriptionformat")

    @descriptionformat.setter
    def descriptionformat(self, value):
        self.update({"descriptionformat": value})
        return value

    @property
    def description(self):
        return self.cohort_info.get("description")

    @description.setter
    def description(self, value):
        self.update({"description": value})
        return value

    @property
    def visible(self):
        return self.cohort_info.get("visible")

    @visible.setter
    def visible(self, value):
        self.update({"visible": value})
        return value

    @property
    def members(self):
        if self._members is None:
            self._members = self.get_members()
        return self._members

    def refresh(self):
        self._members = None
        self.get(self.proxy, cohort_id=self.obj_id)

    def update(self, cohort_info):
        new_cohort_info = self.cohort_info.copy()
        new_cohort_info.update(cohort_info)
        if new_cohort_info.get("id") != self.obj_id:
            raise ValueError("Cannot change object id")
        if self.cohort_info != new_cohort_info:
            self.proxy.core_cohort_update_cohort([new_cohort_info])
            self.cohort_info = new_cohort_info

    def delete(self):
        self.proxy.core_cohort_delete_cohorts([self.obj_id])
        super().delete()

    def get_members(self):
        r = self.proxy.core_cohort_get_cohort_members([self.obj_id])
        if not isinstance(r, list) or len(r) != 1 or not isinstance(r[0], list):
            raise self.BadResponse("core_cohort_get_cohort_members", r)
        return r[0]

    def add_members(self, members):
        if not members:
            return []
        r = self.proxy.core_cohort_add_cohort_members(
            [
                {
                    "cohorttype": {"type": "id", "value": self.obj_id},
                    "usertype": {"type": "id", "value": user_id},
                }
                for user_id in members
            ]
        )
        if not isinstance(r, dict):
            raise self.BadResponse("core_cohort_add_cohort_members", r)
        return r.get("warnings", [])

    def delete_members(self, members):
        if not members:
            return []
        try:
            r = self.proxy.core_cohort_delete_cohort_members(
                [{"cohortid": self.obj_id, "userid": user_id} for user_id in members]
            )
            return r.get("warnings", [])
        except xml.parsers.expat.ExpatError:
            # Moodle returns an invalid XML when the operation succeed without
            # error.
            return []

    @classmethod
    def create(cls, proxy, cohort_info):
        if "categorytype" not in cohort_info:
            cohort_info["categorytype"] = {"type": "system", "value": None}
        cohorts = proxy.core_cohort_create_cohorts([cohort_info])
        if not cohorts:
            raise cls.BadResponse("core_cohort_create_cohorts: no cohort returned")
        if len(cohorts) > 1:
            raise cls.BadResponse(
                "core_cohort_create_cohorts: multiple objects returned",
                cohorts,
            )
        return cls(proxy=proxy, obj_id=cohorts[0].get("id"), cohort_info=cohorts[0])

    @classmethod
    def get(cls, proxy, cohort_id=None, *, cohort_idnumber=None):
        if cohort_id is not None:
            cohorts = list(cls.all(proxy, [cohort_id]))
            if not cohorts:
                raise cls.DoesNotExist("cohort_id={cohort_id}")
            if len(cohorts) > 1:
                raise cls.MultipleObjectsReturned("cohort_id={cohort_id}")
            return cohorts[0]

        if cohort_idnumber is not None:
            matches = []
            for cohort in cls.all(proxy):
                if cohort.idnumber == cohort_idnumber:
                    matches.append(cohort)
            if not matches:
                raise cls.DoesNotExist("cohort_idnumber={cohort_idnumber}")
            if len(matches) > 1:
                raise cls.MultipleObjectsReturned("cohort_idnumber={cohort_idnumber}")
            return matches[0]

        raise TypeError("You must provide either an id or an idnumber value")

    @classmethod
    def all(cls, proxy, ids=None):
        if ids is None:
            ids = []
        for cohort_info in proxy.core_cohort_get_cohorts(ids):
            if not isinstance(cohort_info, dict):
                raise cls.BadResponse("core_cohort_get_cohorts", cohort_info)
            cohort_info["categorytype"] = {"type": "system", "value": None}
            yield cls(
                proxy=proxy,
                obj_id=cohort_info.get("id"),
                cohort_info=cohort_info,
            )
