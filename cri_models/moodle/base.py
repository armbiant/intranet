class MoodleObject:
    _CACHE = {}
    _id = None

    class DoesNotExist(Exception):
        pass

    class MultipleObjectReturned(Exception):
        pass

    class BadResponse(Exception):
        pass

    # pylint: disable=unused-argument
    def __new__(cls, proxy, obj_id, *_args, **_kwargs):
        obj = cls._CACHE.setdefault(cls, {}).get(obj_id)
        if obj is None:
            obj = super().__new__(cls)
            cls._CACHE[cls][obj_id] = obj
        return obj

    def __init__(self, proxy, obj_id):
        self.proxy = proxy
        self._id = obj_id

    @property
    def obj_id(self):
        return self._id

    def delete(self):
        if self._id is not None:
            self._CACHE.setdefault(type(self), {}).pop(self._id, None)
            self._id = None
