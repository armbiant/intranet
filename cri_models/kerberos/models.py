from django.conf import settings

from kadmin import PasswordReuseError

from .base import KerberosContext, KerberosModel
from .utils import (
    analyze_principal,
    KerberosPrincipalStr,
    kerberos_insecure_check_password,
)
from . import fields


class Principal(KerberosModel):
    expire_at = fields.KerberosField(db_column="expire")

    kvno = fields.KerberosIntegerField()

    maxlife = fields.KerberosTimeDeltaField()

    maxrenewlife = fields.KerberosTimeDeltaField()

    policy = fields.KerberosField(
        # python-kadmin has various policy related-crash
        editable=False
    )

    passwod_expire_at = fields.KerberosDateTimeField(db_column="pwexpire")

    failures_count = fields.KerberosDateTimeField(db_column="failures", editable=False)

    keys = fields.KerberosField(editable=False)

    last_failure = fields.KerberosDateTimeField(editable=False)

    last_pwd_change = fields.KerberosDateTimeField(editable=False)

    updated_at = fields.KerberosDateTimeField(db_column="mod_date", editable=False)

    updated_by = fields.KerberosField(db_column="mod_name", editable=False)

    @property
    def principal(self):
        return KerberosPrincipalStr(
            primary=self.primary, instance=self.instance, realm=self.realm
        )

    @principal.setter
    def principal(self, value):
        if self.pk:
            raise ValueError("You cannot rename a saved Kerberos principal")
        (self.primary, self.instance, self.realm) = analyze_principal(value)

    # pylint:disable=too-many-arguments
    def __init__(
        self,
        principal=None,
        *,
        primary=None,
        instance=None,
        realm=None,
        _pk=None,
        **kwargs,
    ):
        super().__init__(_pk=_pk, **kwargs)
        if self.pk:
            (self.primary, self.instance, self.realm) = analyze_principal(self.pk)
        else:
            (self.primary, self.instance, self.realm) = analyze_principal(
                principal, primary=primary, instance=instance, realm=realm
            )

    def __eq__(self, other):
        if type(self) != type(other):  # pylint: disable=unidiomatic-typecheck
            return False
        if other is self:
            return True
        if self.pk is None:
            return False
        return self.pk == other.pk

    def __str__(self):
        return str(self.principal)

    def _get_field_value(self, fieldname):
        if self.pk and not self._instance:
            self.refresh_from_db()
        return getattr(self._instance, fieldname, super()._get_field_value(fieldname))

    def _set_field_value(self, fieldname, value):
        if self.pk and not self._instance:
            self.refresh_from_db()
        if hasattr(self._instance, fieldname):
            setattr(self._instance, fieldname, value)
        super()._set_field_value(fieldname, value)

    def save(self, force_insert=False, force_update=False, update_fields=None):
        if force_insert and force_update:
            raise ValueError("Cannot force both insert and updating in model saving.")

        if update_fields is None:
            update_fields = (f.name for f in self._meta.get_fields() if f.editable)

        with KerberosContext(reuse_context=True):
            if (self.pk or force_update) and not force_insert:
                values = {
                    f.name: getattr(self, f.name)
                    for f in self._meta.get_fields()
                    if self._is_field_modified(f.db_column or f.name)
                }
                if not self.pk:
                    self.pk = self.principal
                self.refresh_from_db()
                for fname, value in values.items():
                    if fname in update_fields:
                        setattr(self, fname, value)
                if hasattr(self, "_password"):
                    raw_password = getattr(self, "_password")
                    if raw_password:
                        try:
                            self._instance.change_password(raw_password)
                        except PasswordReuseError:
                            pass
                    else:
                        self._instance.randomize_key()
                    del self._password
                if values:
                    self._instance.commit()
                for fname, value in values.items():
                    if fname not in update_fields:
                        setattr(self, fname, value)
            if not self.pk or force_insert:
                principal = KerberosPrincipalStr(self.principal)
                self._get_kadm(principal.realm).add_principal(principal)
                self.save(force_update=True, update_fields=update_fields)

    def delete(self):
        if not self.pk:
            raise RuntimeError("This object has not been saved yet")
        with KerberosContext(reuse_context=True):
            principal = KerberosPrincipalStr(self.pk)
            self._get_kadm(principal.realm).delete_principal(self.pk)
        self.pk = None
        self._instance = None

    def refresh_from_db(self):
        if self.pk:
            with KerberosContext(reuse_context=True):
                principal = KerberosPrincipalStr(self.pk)
                self._instance = self._get_kadm(principal.realm).get_principal(self.pk)
            if self._instance is None:
                raise self.DoesNotExist(self.pk)
        else:
            raise RuntimeError("This object has not been saved yet")

    def set_password(self, raw_password):
        self._password = raw_password

    def insecure_check_password(self, raw_password):
        return kerberos_insecure_check_password(self.principal, raw_password)

    @classmethod
    def all(cls, realm=None):
        realms = settings.KERBEROS_REALMS.keys() if realm is None else [realm]
        with KerberosContext(reuse_context=True):
            for r in realms:
                for principal in cls._get_kadm(r).principals():
                    yield cls(_pk=principal)

    @classmethod
    def get(cls, pk, realm=None):
        with KerberosContext(reuse_context=True):
            obj = cls(_pk=pk)
            obj.refresh_from_db()
        return obj

    @classmethod
    def get_or_create(cls, pk):
        with KerberosContext(reuse_context=True):
            try:
                obj = cls.get(pk)
                return obj, False
            except cls.DoesNotExist:
                pass
            obj = cls(pk)
            obj.save(force_insert=True)
        return obj, True
