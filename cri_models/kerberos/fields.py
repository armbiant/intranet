from .utils import KerberosPrincipalStr
from django.db import models as django_models
from django.db.models import fields as django_fields


class KerberosPrincipalField(django_models.CharField):
    def __init__(self, *args, **kwargs):
        kwargs["max_length"] = min(kwargs.get("max_length", 256), 256)
        super().__init__(*args, **kwargs)

    def from_db_value(self, value, _expression, _connection):
        return self.to_python(value)

    def get_prep_value(self, value):
        return super().get_prep_value(value) or None

    def to_python(self, value):
        if not value:
            return ""
        if isinstance(value, KerberosPrincipalStr):
            return value
        return KerberosPrincipalStr(value)


class PrincipalComponentLookup(django_models.Lookup):
    prepare_rhs = False

    def as_sql(self, compiler, connection):
        lhs, lhs_params = self.process_lhs(compiler, connection)
        rhs, rhs_params = self.process_rhs(compiler, connection)
        params = (lhs_params + rhs_params) * len(self.patterns)
        expressions = [
            "{field} LIKE {pattern}".format(
                field=lhs, pattern=p.replace("%", "%%").format(value=rhs)
            )
            for p in self.patterns
        ]
        return "({})".format(" OR ".join(expressions)), params


@KerberosPrincipalField.register_lookup
class PrimaryLookup(PrincipalComponentLookup):
    lookup_name = "primary"
    patterns = ("CONCAT({value}, '/%@%')", "CONCAT({value}, '@%')")


@KerberosPrincipalField.register_lookup
class InstanceLookup(PrincipalComponentLookup):
    lookup_name = "instance"
    patterns = ("CONCAT('%/', {value}, '@%')",)


@KerberosPrincipalField.register_lookup
class RealmLookup(PrincipalComponentLookup):
    lookup_name = "realm"
    patterns = ("CONCAT('%@', {value})",)


class KerberosField(django_fields.Field):
    is_relation = False
    primary_key = False

    def __init__(self, name=None, db_column=None, editable=True):
        super().__init__(name=name, db_column=db_column, editable=editable)

    def to_kerberos(self, value):
        return value

    def to_python(self, value):
        return value


class KerberosIntegerField(KerberosField):
    def to_kerberos(self, value):
        return value or 0


class KerberosTimeDeltaField(KerberosField):
    pass


class KerberosDateTimeField(KerberosField):
    pass
