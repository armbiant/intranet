import logging
import xmlrunner.extra.djangotestrunner


class CRIIntranetTestRunner(xmlrunner.extra.djangotestrunner.XMLTestRunner):
    RUNNING_TESTS = False

    def run_tests(self, *args, **kwargs):  # pylint: disable=arguments-differ
        type(self).RUNNING_TESTS = True
        result = super().run_tests(*args, **kwargs)
        type(self).RUNNING_TESTS = False
        return result


class NotInATestFilter(logging.Filter):
    def filter(self, _record):
        return not CRIIntranetTestRunner.RUNNING_TESTS


class MockProxy:
    _proxy_contexts = []

    class Override:
        def __init__(self, cls_or_name, attr_dict=None):
            if isinstance(cls_or_name, type):
                self.clsname = cls_or_name.__name__
            elif isinstance(cls_or_name, str):
                self.clsname = cls_or_name
            else:
                raise TypeError("cls_or_name must be a class or a string")

            self.attr_dict = attr_dict

        def __enter__(self):
            MockProxy.push_context()
            if self.attr_dict is not None:
                MockProxy.proxy_override_bulk(self.clsname, self.attr_dict)
            return self

        def __exit__(self, *_args, **_kwargs):
            MockProxy.pop_context()

    @classmethod
    def push_context(cls):
        cls._proxy_contexts.append({})

    @classmethod
    def pop_context(cls):
        cls._proxy_contexts.pop()

    @classmethod
    def proxy_override(cls, clsname, attr, value):
        cls._proxy_contexts[-1].setdefault(clsname, {})[attr] = value

    @classmethod
    def proxy_override_bulk(cls, clsname, attr_dict):
        cls._proxy_contexts[-1].setdefault(clsname, {}).update(attr_dict)

    def __init__(self, obj, clsname=None):
        if clsname is None:
            clsname = obj.__class__.__name__

        self._obj = obj
        self._proxy_clsname = clsname

    def __getattribute__(self, attr):
        try:
            return super().__getattribute__(attr)
        except AttributeError:
            pass
        clsname = self._proxy_clsname
        for ctx in reversed(self._proxy_contexts):
            if clsname in ctx and attr in ctx[clsname]:
                return ctx[clsname][attr]
        return getattr(self._obj, attr)
