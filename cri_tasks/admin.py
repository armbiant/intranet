from django.contrib import admin

from . import models


@admin.register(models.CRITask)
class CRITaskAdmin(admin.ModelAdmin):
    list_display = ("id", "task_class", "task", "owner", "status")
    list_display_links = list_display
    list_filter = ("status", "task_class")
    search_fields = (
        "id",
        "task_class",
        "task__id",
        "task__name",
        "owner__username",
        "owner__first_name",
        "owner__last_name",
        "owner__email",
        "owner__uid",
    )
    autocomplete_fields = ("owner",)
    readonly_fields = (
        "parameters",
        "task_class",
        "status",
        "task",
        "created_at",
    )
    date_hierarchy = "created_at"
