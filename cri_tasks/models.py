from django.db import models, transaction
from django.contrib.auth import get_user_model

from django_q import models as django_q_models
from django_q.tasks import AsyncTask


class CRITask(models.Model):
    _task_classes = {}

    NEW = "new"
    PENDING = "pending"
    RUNNING = "running"
    FAILED = "failed"
    SUCCEEDED = "succeeded"
    STATUS_CHOICES = (
        (NEW, "New"),
        (PENDING, "Pending"),
        (RUNNING, "Running"),
        (FAILED, "Failed"),
        (SUCCEEDED, "Succeeded"),
    )

    task_class = models.CharField(max_length=256, editable=False)

    parameters = models.JSONField(blank=True, default=dict, editable=False)

    owner = models.ForeignKey(
        get_user_model(), on_delete=models.SET_NULL, blank=True, null=True
    )

    task = models.OneToOneField(
        django_q_models.Task,
        on_delete=models.CASCADE,
        blank=True,
        null=True,
        editable=False,
    )

    status = models.CharField(
        max_length=20, choices=STATUS_CHOICES, default=NEW, editable=False
    )

    created_at = models.DateTimeField(auto_now_add=True, editable=False)

    class Meta:
        verbose_name = "CRI Task"
        verbose_name_plural = "CRI Tasks"
        ordering = ("-created_at",)

    class TaskError(RuntimeError):
        pass

    def get_task_class(self):
        return self._task_classes.get(self.task_class)

    def get_task_instance(self):
        return self.get_task_class().objects.get(pk=self.pk)

    def save(self, *args, **kwargs):  # pylint: disable=arguments-differ
        if not self.task_class:
            self.task_class = type(self).__name__
        super().save(*args, **kwargs)
        if not self.status or self.status == self.NEW:
            self.schedule()

    def schedule(self, group=None):
        self.status = self.PENDING
        self.save()
        task = AsyncTask(self._run)
        if isinstance(self.parameters, dict):
            task.kwargs = self.parameters.copy()
        elif isinstance(self.paramerters, list):
            task.args = self.parameters.copy()
        else:
            task.args = (self.parameters,)
        task.kwargs["obj"] = self
        task.hook = self._hook
        task.group = group
        transaction.on_commit(task.run)

    def run(self, *_args, **_kwargs):
        pass

    def hook(self, _task):
        pass

    def get_result_display(self):
        return str(self.task.result)

    @classmethod
    def register_task_class(cls, task_class):
        cls._task_classes[task_class.__name__] = task_class
        return task_class

    @staticmethod
    def _run(*args, obj, **kwargs):
        obj.refresh_from_db()
        obj.status = obj.RUNNING
        obj.save()
        transaction.commit()
        try:
            with transaction.atomic():
                return obj.run(*args, **kwargs)
        except obj.TaskError as e:
            obj.status = obj.FAILED
            obj.save()
            return str(e)

    @staticmethod
    def _hook(task):
        obj = task.kwargs.get("obj")
        obj.refresh_from_db()
        if obj.status != obj.FAILED:
            obj.status = obj.SUCCEEDED if task.success else obj.FAILED
        obj.task = task
        obj.save()
        transaction.commit()
        with transaction.atomic():
            obj.hook(task)
