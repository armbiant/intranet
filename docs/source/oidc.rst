.. _oidc:

**************
OpenID Connect
**************


Public test client credentials
==============================

The following credentials are available for development purpose only:

  - ``ACCESS_KEY=125070``
  - ``SECRET_KEY=f6ff8d394e6185d41834b19210979b897852680cf34700ae4ecb24ea``

This client is allowed to request any scope, the following URI are accepted as
``redirect_uri``:

  - http://localhost:8000/complete/epita/
  - http://localhost:8080/complete/epita/
  - http://127.0.0.1:8000/complete/epita/
  - http://127.0.0.1:8080/complete/epita/
  - http://[::1]:8000/complete/epita/
  - http://[::1]:8080/complete/epita/
  - https://localhost:8000/complete/epita/
  - https://localhost:8080/complete/epita/
  - https://127.0.0.1:8000/complete/epita/
  - https://127.0.0.1:8080/complete/epita/
  - https://[::1]:8000/complete/epita/
  - https://[::1]:8080/complete/epita/


Available scopes and claims
===========================

The CRI intranet OIDC provider give you access to the following claims, sorted
by scope:

  - **profile**

    - ``name``: the user full name as a string (example: "Xavier Loginard").
    - ``given_name``: the user first name, may be empty (example: "Xavier").
    - ``family_name``: the user last name (example: "Loginard").
    - ``preferred_username``: the user login (example: "xavier.loginard").
    - ``zoneinfo``: always "Europe/Paris".

  - **picture**

    - ``picture``: URI of the user picture, may be empty.
    - ``picture_square``: URI of the user picture as a square centered on the
      face, may be empty.
    - ``picture_thumb``: URI the user picture thumbnail, may be empty.

  - **email**

    - ``email``: the user EPITA mail address, may be empty (example:
      "xavier.loginard@epita.fr").
    - ``email_verified``: true when the email is set, false otherwise.

  - **phone**

    - ``phone_number``: the user mobile phone number, may be empty.
    - ``phone_number_verified``: always false since no phone verification is
      ever done.

  - **birthdate**

    - ``birthdate``: the user birthdate as a YYYY-MM-DD string, may be empty
      (example: "1884-01-01").

  - **legal_identity**

    - ``legal_first_name``: the user first as written on official documents,
      may be empty.
    - ``legal_last_name``: the user first as written on official documents, may
      be empty.

  - **epita**

    - ``uid``: the user ID, as an integer.
    - ``gid``: the user primary group ID, as an integer.
    - ``old_logins``: list of the user previous accounts username, in case of
      username update, as a list, may be empty.
    - ``new_login``: the user new account username, may be empty.
    - ``groups``: the list of groups the user is a member of, may be empty.
      Each group appears as a mapping containing the following fields :
      ``slug``, ``name``, ``gid``, ``kind``, ``private``.
    - ``campuses``: the list of the campuses of the user, may be empty. Each
      campus appears as the slug of the corresponding group.
    - ``graduation_years``: the list of graduation years (promo) associated
      with the user, may be empty.

  - **roles**
    - ``roles``: the list of the user roles as specified in the client
      configuration, may be empty.


This list is summarized in the tables below:

+--------------------+----------------+----------------+-----------------------+
| profile            | picture        | email          | phone                 |
+====================+================+================+=======================+
| name               | picture        | email          | phone_number          |
+--------------------+----------------+----------------+-----------------------+
| given_name         | picture_square | email_verified | phone_number_verified |
+--------------------+----------------+----------------+-----------------------+
| family_name        | picture_thumb  |                |                       |
+--------------------+----------------+----------------+-----------------------+
| preferred_username |                |                |                       |
+--------------------+----------------+----------------+-----------------------+
| picture            |                |                |                       |
+--------------------+----------------+----------------+-----------------------+
| zoneinfo           |                |                |                       |
+--------------------+----------------+----------------+-----------------------+

+-----------+------------------+------------------+------------------+
| birthdate | legal_identity   | epita            | roles            |
+===========+==================+==================+==================+
| birthdate | legal_first_name | uid              | roles            |
+-----------+------------------+------------------+------------------+
|           | legal_last_name  | gid              |                  |
+-----------+------------------+------------------+------------------+
|           |                  | old_logins       |                  |
+-----------+------------------+------------------+------------------+
|           |                  | new_login        |                  |
+-----------+------------------+------------------+------------------+
|           |                  | groups           |                  |
+-----------+------------------+------------------+------------------+
|           |                  | campuses         |                  |
+-----------+------------------+------------------+------------------+
|           |                  | graduation_years |                  |
+-----------+------------------+------------------+------------------+
