.. _quickstart:

***********
Quick start
***********


Starting the application
========================

You will need to have ``docker`` and ``docker-compose`` installed to run the
developement environment.

The provided ``docker/docker-compose.yml`` file mounts the source files into
the docker container so you don't have to rebuild the image each time you want
to test your modification.


Generating secrets
------------------

You will need to perform this step only once to generate the various secrets
needed by this application.

You will be asked to provide a value for the Azure OAuth2 secret, you can leave
it empty if you do not know it.

.. code-block:: console

  $ pipenv install --dev --pre
  $ cd docker/
  $ ./gen-secrets.sh
  Please input a value for azuread-oauth2-secret:


Starting the docker-compose stack
---------------------------------

.. code-block:: console

  $ cd docker/
  $ DOCKER_BUILDKIT=1 COMPOSE_DOCKER_CLI_BUILD=1 docker-compose up --build


Using the application
=====================

You may now head to http://localhost:8000/ to start using the application.
You may sign in as ``admin`` with the password written in
``docker/secrets/kerberos-admin``.


Format the code with our standard
=================================

We use ``python/black`` as a code formatter. It can be enabled through
git pre-commit with the following command:

.. code-block:: console

  $ pipenv run pre-commit install


Check your coding style
-----------------------

We use ``pylint`` to check the coding style. It is configured with
``gitlab-ci`` and produce warnings in gitlab. To check if the project
matches `pylint`'s recommendations you can use the following command:

.. code-block:: console

  $ pipenv run prospector --profile base
