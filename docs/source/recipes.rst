*******
Recipes
*******

You will need to have followed the instructions in the :ref:`quickstart` page
and have the website up and running before being able to follow these recipes.


Creating new migrations
-----------------------

.. code-block:: console

  $ cd docker/
  $ docker-compose exec -T intranet_dev python3 manage.py makemigrations


Starting an interactive IPython session
---------------------------------------

.. code-block:: console

  $ cd docker/
  $ docker-compose exec -T intranet_dev python3 manage.py shell


Building the documentation
--------------------------

.. code-block:: console

  $ cd docker/
  $ docker-compose exec -T intranet_dev sphinx-apidoc -M -e -o docs/source/apidoc/ . '*migrations/*'
  $ docker-compose exec -T intranet_dev make -C docs html

You will find the documentation in ``docs/build/html/index.html``.
