import unittest
from django.test import Client

from cri_models.fields import SSHPubKey
from cri_models.ldap.test import LDAPTestCase
from cri_models.models import CRIUser, SSHPublicKey


class TestCRISSHKeyShortcutView(LDAPTestCase):
    fixtures = ("tests/groups.yaml",)

    @classmethod
    def _add_databases_failures(cls):
        pass

    @classmethod
    def _remove_databases_failures(cls):
        pass

    @classmethod
    def setUpTestData(cls):
        CRIUser(username="test", email="test@example.org", uid=11).save()

    def test_non_existant_user(self):
        client = Client()

        response = client.get("/non-existant.keys")

        self.assertEqual(response.content.decode(), "\n")

    def test_no_key(self):
        client = Client()
        CRIUser(username="test")

        response = client.get("/test.keys")

        self.assertEqual(response.content.decode(), "\n")

    def test_single_key_with_comment(self):
        client = Client()
        user = CRIUser(username="test")
        ssh_key = "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIOrvjSGhfjaWn/ANO1uQENVx+XW78XVSlvR64m7iYf8+"  # noqa: E501
        comment = "test_key"
        SSHPublicKey(user=user, key=SSHPubKey(f"{ssh_key} {comment}")).save(
            do_sync=False
        )

        response = client.get("/test.keys")

        self.assertEqual(response.content.decode(), ssh_key + " test\n")

    def test_single_key_no_comment(self):
        client = Client()
        user = CRIUser(username="test")
        ssh_key = "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIOrvjSGhfjaWn/ANO1uQENVx+XW78XVSlvR64m7iYf8+"  # noqa: E501
        SSHPublicKey(user=user, key=SSHPubKey(ssh_key)).save(do_sync=False)

        response = client.get("/test.keys")

        self.assertEqual(response.content.decode(), ssh_key + " test\n")

    @unittest.expectedFailure
    def test_single_key_with_options(self):
        client = Client()
        user = CRIUser(username="test")
        options = "no-port-forwarding"
        ssh_key = "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIOrvjSGhfjaWn/ANO1uQENVx+XW78XVSlvR64m7iYf8+"  # noqa: E501
        # Options are not allowed so this raises an exception
        SSHPublicKey(user=user, key=SSHPubKey(f"{options} {ssh_key}")).save(
            do_sync=False
        )

        response = client.get("/test.keys")

        self.assertEqual(response.content.decode(), ssh_key + " test\n")

    def test_multiple_keys(self):
        client = Client()
        user = CRIUser(username="test")
        ssh_key_1 = "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIOrvjSGhfjaWn/ANO1uQENVx+XW78XVSlvR64m7iYf8+"  # noqa: E501
        comment_1 = "test_key_1"
        SSHPublicKey(user=user, key=SSHPubKey(f"{ssh_key_1} {comment_1}")).save(
            do_sync=False
        )
        ssh_key_2 = "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIPhWx8VR9STySYloHUwpCTH8kayMmzVqgs5EGHb0ar0Y"  # noqa: E501
        comment_2 = "test_key_2"
        SSHPublicKey(user=user, key=SSHPubKey(f"{ssh_key_2} {comment_2}")).save(
            do_sync=False
        )

        response = client.get("/test.keys")

        self.assertEqual(
            response.content.decode(), f"{ssh_key_1} test\n{ssh_key_2} test\n"
        )
