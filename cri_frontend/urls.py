"""cri_intranet URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.urls import path, reverse_lazy
from django.views.generic import RedirectView

from cri_frontend import views

urlpatterns = [
    # This path is kept to avoid breaking bookmarks on the old intranet
    path(
        "accounts/profile/",
        RedirectView.as_view(url=reverse_lazy("criuser_self"), permanent=True),
    ),
    path("", views.IndexView.as_view(), name="index"),
    path("maps/", views.NotAvailableView.as_view(), name="maps"),
    path("search/", views.SearchFormView.as_view(), name="search"),
    path("me/", views.CRIUserSelfView.as_view(), name="criuser_self"),
    path("users/", views.UserlistAction.as_view(), name="userlist_action"),
    path(
        "users/<str:username>/",
        views.CRIUserView.as_view(),
        name="criuser_detail",
    ),
    path(
        "users/<str:username>/notes/",
        views.CRIUserNotesView.as_view(),
        name="criusernote_list",
    ),
    path(
        "users/<str:username>/notes/create/",
        views.CRIUserNoteCreateView.as_view(),
        name="criusernote_create",
    ),
    path(
        "users/<str:username>/notes/<int:pk>/edit",
        views.CRIUserNoteUpdateView.as_view(),
        name="criusernote_update",
    ),
    path(
        "users/<str:username>/notes/<int:pk>/delete",
        views.CRIUserNoteDeleteView.as_view(),
        name="criusernote_delete",
    ),
    path(
        "users/<str:username>/password/",
        views.CRIUserChangePasswordView.as_view(),
        name="criuser_password",
    ),
    path(
        "users/<str:username>/oidc/",
        views.CRIUserOIDCView.as_view(),
        name="criuser_oidc",
    ),
    path(
        "oidc/consent/<int:pk>/delete",
        views.OIDCConsentDeleteView.as_view(),
        name="criuser_oidc_delete_consent",
    ),
    path(
        "users/<str:username>/ssh-keys/",
        views.CRIUserSSHKeysView.as_view(),
        name="criuser_ssh_keys",
    ),
    path(
        "ssh-keys/<int:pk>/delete",
        views.SSHKeyDeleteView.as_view(),
        name="criuser_ssh_keys_delete",
    ),
    path(
        "users/<str:username>/logs/",
        views.CRIUserConnectionLogView.as_view(),
        name="criuser_logs",
    ),
    path(
        "users/<str:username>/tasks/",
        views.CRIUserTasksView.as_view(),
        name="criuser_tasks",
    ),
    path(
        "group/<slug:slug>/",
        views.CRIGroupView.as_view(),
        name="crigroup_detail",
    ),
    path(
        "password/change/done/",
        views.IndexView.as_view(),
        name="password_change_done",
    ),
    path(
        "<str:username>.keys",
        views.CRIUserSSHKeysShortcutView.as_view(),
        name="criuser_ssh_keys_shortcut",
    ),
]
