from django import template


from cri_tasks.models import CRITask


register = template.Library()


@register.filter
def processing_tasks_count(tasks):
    return tasks.exclude(status=CRITask.FINISHED).count()
