from django.conf import settings

from cri_models.algolia import index


def search(request):
    if request.user.is_anonymous:
        # Avoid key generation for unauthenticated users
        return {}

    indices = [
        (index.CRIUserIndex.get_index_name(), "Users"),
        (index.CRIGroupIndex.get_index_name(), "Groups"),
    ]

    return {
        "algolia_app_id": settings.ALGOLIA.get("APPLICATION_ID"),
        "algolia_search_key": index.get_user_search_key(request.user),
        "algolia_indices": indices,
    }
