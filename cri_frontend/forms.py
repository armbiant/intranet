from django import forms
from django.conf import settings
from django.core.exceptions import ValidationError
from django.forms.fields import FileField
from django.utils import timezone

from crispy_forms import layout as crispy_layout, bootstrap
from crispy_forms.helper import FormHelper as CrispyFormHelper

from algoliasearch.search_client import SearchClient

from cri_models import models as cri_models
from cri_models.fields import SSHPubKey
from cri_models.algolia import index


class CRIUserNoteForm(forms.ModelForm):
    class Meta:
        model = cri_models.CRIUserNote
        fields = (
            "event_kind",
            "event_title",
            "event_date",
            "external_ref",
            "scope",
            "content",
            "secret",
            "labels",
        )
        widgets = {"scope": forms.RadioSelect()}

    def save(self, commit=True):
        obj = super().save(commit=False)

        obj.user = self._user
        if obj.id:
            obj.updated_at = timezone.now()
            obj.updated_by = self._author
        else:
            obj.created_at = timezone.now()
            obj.author = self._author

        if commit:
            obj.save()

        return obj

    def __init__(self, *args, user, author, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields["scope"].queryset = cri_models.CRIUserNoteScope.from_writer(author)
        self._user = user
        self._author = author
        self.helper = CrispyFormHelper()
        self.helper.layout = crispy_layout.Layout(
            crispy_layout.Row(
                crispy_layout.Column(
                    bootstrap.InlineRadios("scope"),
                    css_class="form-group col-md-12 mb-0",
                )
            ),
            crispy_layout.Row(
                crispy_layout.Column(
                    "event_title", css_class="form-group col-md-8 mb-0"
                ),
                crispy_layout.Column(
                    "event_date", css_class="form-group col-md-4 mb-0"
                ),
            ),
            crispy_layout.Row(
                crispy_layout.Column(
                    bootstrap.InlineRadios("event_kind"),
                    css_class="form-group col-md-8 mb-0",
                ),
                crispy_layout.Column(
                    "external_ref", css_class="form-group col-md-4 mb-0"
                ),
            ),
            crispy_layout.Row(
                crispy_layout.Column("content", css_class="form-group col-md-12 mb-0")
            ),
            crispy_layout.Row(
                crispy_layout.Column("secret", css_class="form-group col-md-6 mb-0")
            ),
            crispy_layout.ButtonHolder(
                crispy_layout.Row(
                    bootstrap.StrictButton(
                        '<i class="far fa-comment-alt"></i> Add note',
                        type="submit",
                        css_class="btn btn-outline-primary btn-block btn-lg",
                    )
                ),
                css_class="mx-1",
            ),
        )


class SSHPublicKeyForm(forms.ModelForm):
    class Meta:
        model = cri_models.SSHPublicKey
        fields = ("title", "key")

    def __init__(self, user, *args, **kwargs):
        self.user = user
        super().__init__(*args, **kwargs)

    def _clean_field(self, name, field):
        # value_from_datadict() gets the data from the data dictionaries.
        # Each widget type knows how to retrieve its own data, because some
        # widgets split data over several HTML fields.
        if field.disabled:
            value = self.get_initial_for_field(field, name)
        else:
            value = field.widget.value_from_datadict(
                self.data, self.files, self.add_prefix(name)
            )
        try:
            if isinstance(field, FileField):
                initial = self.get_initial_for_field(field, name)
                value = field.clean(value, initial)
            else:
                value = field.clean(value)
            self.cleaned_data[name] = value
            if hasattr(self, "clean_%s" % name):
                value = getattr(self, "clean_%s" % name)()
                self.cleaned_data[name] = value
        except ValidationError as e:
            self.add_error(name, e)

    def _clean_fields(self):
        # We force field cleaning order to be able to use the key comment field as title
        # if the later is empty.
        for name in ("key", "title"):
            field = self.fields.get(name)
            getattr(self, f"_clean_{name}", self._clean_field)(name, field)

    def clean_title(self):
        key = self.cleaned_data.get("key")
        field = self.fields["title"]
        value = self.cleaned_data.get("title")
        if not value and key:
            try:
                value = field.clean(key.comment)
            except ValidationError as e:
                self.add_error("title", e)
                value = key.comment
        self.data = self.data.copy()
        self.data["title"] = value
        if cri_models.SSHPublicKey.objects.filter(user=self.user, title=value).exists():
            raise ValidationError("SSH public key with this title already exists.")
        return value

    def clean_key(self):
        return SSHPubKey.clean(self.cleaned_data["key"])

    def save(self, commit=True):
        obj = super().save(commit=False)
        obj.user = self.user
        if commit:
            obj.save()
        return obj


class ExportForm(forms.Form):
    EXPORT_TYPE_CHOICES = (("regular", "Regular"), ("moodle", "Moodle"))

    EXPORT_FORMAT_CHOICES = (
        ("xlsx", "xlsx"),
        ("csv", "csv"),
        ("tsv", "tsv"),
        ("json", "json"),
    )

    export_type = forms.ChoiceField(choices=EXPORT_TYPE_CHOICES, initial="regular")

    export_format = forms.ChoiceField(choices=EXPORT_FORMAT_CHOICES, initial="xlsx")


class SearchForm(forms.Form):
    omnisearch = forms.CharField(required=False)

    year = forms.ModelChoiceField(
        queryset=cri_models.CRIGroup.objects.all(),
        empty_label="",
        required=False,
    )

    semester = forms.ModelChoiceField(
        queryset=cri_models.CRIGroup.objects.all(),
        empty_label="",
        required=False,
    )

    campus = forms.ModelChoiceField(
        queryset=cri_models.CRIGroup.objects.all(),
        empty_label="",
        required=False,
    )

    login_list = forms.CharField(widget=forms.widgets.Textarea, required=False)

    uid_list = forms.CharField(
        label="UID list", widget=forms.widgets.Textarea, required=False
    )

    email_list = forms.CharField(widget=forms.widgets.Textarea, required=False)

    excluded_login_list = forms.CharField(widget=forms.widgets.Textarea, required=False)

    excluded_uid_list = forms.CharField(
        label="Excluded UID list",
        widget=forms.widgets.Textarea,
        required=False,
    )

    excluded_email_list = forms.CharField(widget=forms.widgets.Textarea, required=False)

    def __init__(self, *args, user, **kwargs):
        super().__init__(*args, **kwargs)
        self.user = user
        self.groups = cri_models.CRIGroup.get_groups_user_can_see(user)
        for field in ("year", "semester", "campus"):
            self.fields[field].queryset = self.groups.filter(kind=field)
        self.helper = CrispyFormHelper()
        self.helper.layout = crispy_layout.Layout(
            bootstrap.TabHolder(
                bootstrap.Tab(
                    "Filters",
                    crispy_layout.Row(
                        crispy_layout.Column(
                            "year", css_class="form-group col-md-4 mb-0"
                        ),
                        crispy_layout.Column(
                            "semester", css_class="form-group col-md-4 mb-0"
                        ),
                        crispy_layout.Column(
                            "campus", css_class="form-group col-md-4 mb-0"
                        ),
                    ),
                    crispy_layout.Row(
                        crispy_layout.Column(
                            "login_list", css_class="form-group col-md-4 mb-0"
                        ),
                        crispy_layout.Column(
                            "uid_list", css_class="form-group col-md-4 mb-0"
                        ),
                        crispy_layout.Column(
                            "email_list", css_class="form-group col-md-4 mb-0"
                        ),
                    ),
                ),
                bootstrap.Tab(
                    "Exclusions",
                    crispy_layout.Row(
                        crispy_layout.Column(
                            "excluded_login_list",
                            css_class="form-group col-md-4 mb-0",
                        ),
                        crispy_layout.Column(
                            "excluded_uid_list",
                            css_class="form-group col-md-4 mb-0",
                        ),
                        crispy_layout.Column(
                            "excluded_email_list",
                            css_class="form-group col-md-4 mb-0",
                        ),
                    ),
                ),
            ),
            crispy_layout.ButtonHolder(
                crispy_layout.Row(
                    bootstrap.StrictButton(
                        '<i class="fas fa-search"></i> Search',
                        name="search",
                        type="submit",
                        css_class="btn btn-outline-primary btn-block btn-lg",
                    )
                ),
                css_class="px-4 pb-3",
            ),
        )

    def _algolia_search(self, query):
        client = SearchClient.create(
            settings.ALGOLIA.get("APPLICATION_ID"),
            index.get_user_search_key(self.user),
        )

        indices = {
            index.CRIUserIndex.get_index_name(): cri_models.CRIUser,
            index.CRIGroupIndex.get_index_name(): cri_models.CRIGroup,
        }

        queries = []
        for index_name in indices:
            queries.append({"indexName": index_name, "query": query})
        results = client.multiple_queries(queries)

        hits = {}
        for r in results.get("results", []):
            index_name = r.get("index")
            for hit in r.get("hits", []):
                hits.setdefault(indices.get(index_name), []).append(hit.get("objectID"))

        results = {}
        for model, values in hits.items():
            if not model:
                continue
            results[model] = model.objects.filter(pk__in=filter(None, values))

        return (
            results.get(cri_models.CRIGroup, None),
            results.get(cri_models.CRIUser, cri_models.CRIUser.objects.none()),
        )

    def _get_uid_list(self, uid_string):
        uid_list = []
        for uid_str in uid_string.split():
            try:
                uid_list.append(int(uid_str))
            except ValueError:
                pass
        return uid_list

    def search(self):
        cleaned_data = self.cleaned_data.copy()
        omnisearch = cleaned_data.pop("omnisearch")

        if not any(cleaned_data.values()):
            if omnisearch:
                return self._algolia_search(omnisearch)
            return (None, cri_models.CRIUser.objects.none())

        queryset = cri_models.CRIUser.objects.all().order_by("username")

        for field in ("year", "semester", "campus"):
            if cleaned_data.get(field):
                queryset = queryset.filter(
                    models.Q(memberships__end_at__gte=timezone.now)
                    | models.Q(end_at__isnull=True),
                    models.Q(memberships__begin_at__lte=timezone.now)
                    | models.Q(begin_at__isnull=True),
                    memberships__group=cleaned_data.get(field),
                )

        for model_field, form_field in (
            ("username", "login_list"),
            ("email", "email_list"),
        ):
            if cleaned_data.get(form_field):
                queryset = queryset.filter(
                    **{f"{model_field}__in": cleaned_data.get(form_field).split()}
                )

            if cleaned_data.get(f"excluded_{form_field}"):
                queryset = queryset.exclude(
                    **{
                        f"{model_field}__in": cleaned_data.get(
                            f"excluded_{form_field}"
                        ).split()
                    }
                )

        if cleaned_data.get("uid_list"):
            uid_list = self._get_uid_list(cleaned_data.get("uid_list"))
            queryset = queryset.filter(uid__in=uid_list)

        if cleaned_data.get("excluded_uid_list"):
            uid_list = self._get_uid_list(cleaned_data.get("excluded_uid_list"))
            queryset = queryset.exclude(uid__in=uid_list)

        return (None, queryset.distinct())
