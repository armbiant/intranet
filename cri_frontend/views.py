from django.contrib.auth.mixins import LoginRequiredMixin
from django.core.exceptions import SuspiciousOperation
from django.contrib.auth import get_user_model
from django.contrib import messages
from django.shortcuts import get_object_or_404
from django.http import HttpResponse
from django.shortcuts import redirect
from django.views import generic, View
from django.urls import reverse
from django.db.models import Q
from django.utils import timezone
from django.conf import settings

from django.contrib.auth import views as auth_views, forms as auth_forms

from import_export.formats import base_formats
from oidc_provider import (
    models as oidc_provider_models,
    settings as oidc_provider_settings,
)

from . import models, forms
from cri_models import models as cri_models, import_export
from cri_auth import models as cri_auth_models
from cri_tasks import models as cri_tasks_models
from cri_cards import views as cri_cards_views


class MappingFromAttr:
    def __init__(self, obj):
        self._obj = obj

    def __getitem__(self, key):
        return getattr(self._obj, key)

    def __contains__(self, key):
        return hasattr(self, key)


class IndexView(generic.ListView):
    template_name = "cri_frontend/index.html"
    model = models.Shortcut

    def get_queryset(self):
        return super().get_queryset().select_related()


class NotAvailableView(generic.TemplateView):
    template_name = "cri_frontend/not_available.html"


class CRIUserDetailMixin:
    model = cri_models.CRIUser
    slug_field = "username"
    slug_url_kwarg = "username"

    def dispatch(self, *args, **kwargs):
        criuser = self.get_object()
        new_account = criuser.get_new_account()

        if new_account is None:
            return super().dispatch(*args, **kwargs)

        if new_account == self.request.user:
            return super().dispatch(*args, **kwargs)

        if criuser == self.request.user:
            return super().dispatch(*args, **kwargs)

        if self.request.user.has_perm("cri_models.view_criuser"):
            return super().dispatch(*args, **kwargs)

        return redirect(
            self.request.resolver_match.url_name,
            **{
                **self.request.resolver_match.kwargs,
                self.slug_url_kwarg: getattr(new_account, self.slug_field),
            },
        )

    def get_notes(self):
        criuser = self.get_object()
        return cri_models.CRIUserNote.objects.filter(
            Q(user=self.request.user, secret=False)
            | Q(author=self.request.user)
            | Q(scope__in=cri_models.CRIUserNoteScope.from_reader(self.request.user)),
            user=criuser,
        ).order_by("scope", "-created_at")

    def get_context_data(self, *args, **kwargs):  # pylint: disable=arguments-differ
        context = super().get_context_data(*args, **kwargs)
        context.update(
            {
                "criuser": self.get_object(),
                "note_list": self.get_notes(),
                "writable_scopes": cri_models.CRIUserNoteScope.from_writer(
                    self.request.user
                ),
            }
        )
        return context


class CRIUserDetailView(CRIUserDetailMixin, generic.DetailView):
    pass


class CRIUserView(LoginRequiredMixin, CRIUserDetailView):
    template_name = "cri_frontend/criuser_detail.html"

    def get_context_data(self, *args, **kwargs):  # pylint: disable=arguments-differ
        context = super().get_context_data(*args, **kwargs)
        criuser = self.get_object()

        computed_memberships = (
            cri_models.CRIComputedMembership.objects.filter(user=criuser)
            .select_related()
            .order_by("group__slug")
        )
        if not self.request.user.has_perm("cri_models.view_crigroup"):
            computed_memberships = computed_memberships.filter(
                Q(group__private=False) | Q(group__in=self.request.user.get_groups())
            )

        future = computed_memberships.filter(begin_at__gt=timezone.now())
        current = computed_memberships.exclude(
            Q(end_at__lt=timezone.now()) | Q(begin_at__gt=timezone.now())
        )
        past = computed_memberships.filter(end_at__lt=timezone.now())

        context["memberships_tabs"] = (
            ("Future memberships", future),
            ("Current memberships", current),
            ("Past memberships", past),
        )

        return context


class CRIUserSelfView(CRIUserView):
    def get_object(self, queryset=None):
        return self.request.user


class CRIUserNotesView(CRIUserView):
    template_name = "cri_frontend/criusernote_list.html"

    def is_user_allowed(self):
        if self.request.user.is_anonymous:
            return False
        if self.get_notes():
            return True
        if cri_models.CRIUserNoteScope.from_writer(self.request.user):
            return True
        return False

    def dispatch(self, request, *args, **kwargs):
        if not self.is_user_allowed():
            return self.handle_no_permission()
        self.object = self.get_object()
        return super().dispatch(request, *args, **kwargs)

    def get_context_data(self, *args, **kwargs):  # pylint: disable=arguments-differ
        context = super().get_context_data(*args, **kwargs)
        criuser = self.get_object()

        context.update(
            {
                "extra_links": self.get_extra_links(),
                "form": forms.CRIUserNoteForm(user=criuser, author=self.request.user),
            }
        )

        return context

    def get_extra_links(self):
        criuser = self.get_object()
        extra_links = []
        for extra_link in settings.NOTES_EXTRA_LINKS:
            el = extra_link.copy()
            keys = ["title"]
            if el.get("type") == "date":
                el["content"] = getattr(criuser, el.get("content"), None)
            else:
                keys.append("content")
            for key in keys:
                el[key] = el.get(key, "").format_map(MappingFromAttr(criuser))
            extra_links.append(el)
        return extra_links


class CRIUserNoteFormMixin(LoginRequiredMixin, generic.detail.SingleObjectMixin):
    template_name = "cri_frontend/criusernote_form.html"
    model = cri_models.CRIUserNote
    form_class = forms.CRIUserNoteForm

    def is_user_allowed(self):
        writable_scopes = cri_models.CRIUserNoteScope.from_writer(self.request.user)
        if self.object:
            if self.object.author == self.request.user:
                return True
            if self.object.scope in writable_scopes:
                return True
        elif writable_scopes:
            return True
        return False

    def dispatch(self, request, *args, **kwargs):
        if not self.is_user_allowed():
            return self.handle_no_permission()
        return super().dispatch(request, *args, **kwargs)

    def get_success_url(self):
        return reverse("criusernote_list", kwargs={"username": self.criuser.username})

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs.update({"user": self.criuser, "author": self.request.user})
        return kwargs

    def get_context_data(self, *args, **kwargs):  # pylint: disable=arguments-differ
        context = super().get_context_data(*args, **kwargs)
        context["criuser"] = self.criuser
        return context


class CRIUserNoteCreateView(CRIUserNoteFormMixin, generic.CreateView):
    def dispatch(self, request, *args, **kwargs):
        self.object = None
        self.criuser = get_object_or_404(
            get_user_model(), username=kwargs.get("username")
        )
        return super().dispatch(request, *args, **kwargs)


class CRIUserNoteUpdateView(CRIUserNoteFormMixin, generic.UpdateView):
    def dispatch(self, request, *args, **kwargs):
        self.object = self.get_object()
        self.criuser = self.object.user
        return super().dispatch(request, *args, **kwargs)


class CRIUserNoteDeleteView(LoginRequiredMixin, generic.DeleteView):
    model = cri_models.CRIUserNote
    template_name = "cri_frontend/criusernote_confirm_delete.html"

    def is_user_allowed(self):
        if self.object.author == self.request.user:
            return True
        return self.object.scope in self.writable_scopes

    def dispatch(self, *args, **kwargs):  # pylint: disable=arguments-differ
        self.object = self.get_object()
        self.criuser = self.object.user
        self.writable_scopes = cri_models.CRIUserNoteScope.from_writer(
            self.request.user
        )
        if not self.is_user_allowed():
            return self.handle_no_permission()
        return super().dispatch(*args, **kwargs)

    def get_context_data(self, *args, **kwargs):  # pylint: disable=arguments-differ
        context = super().get_context_data(*args, **kwargs)
        context.update(
            {
                "criuser": self.criuser,
                "note": self.object,
                "writable_scopes": self.writable_scopes,
            }
        )
        return context

    def get_success_url(self):
        return reverse("criusernote_list", kwargs={"username": self.criuser})


class CRIUserChangePasswordView(
    LoginRequiredMixin,
    CRIUserDetailMixin,
    auth_views.PasswordChangeView,
    generic.detail.SingleObjectMixin,
):
    template_name = "cri_frontend/criuser_password.html"
    model = cri_models.CRIUser
    slug_field = "username"
    slug_url_kwarg = "username"

    def is_user_allowed(self):
        if self.request.user.has_perm("cri_models.update_criuser_password"):
            return True
        user = self.get_object()
        if user == self.request.user:
            return True
        if hasattr(user, "criserviceaccount"):
            if self.request.user in user.criserviceaccount.get_managers():
                return True
        if user.is_superuser:
            # We do not allow admins to change other admins passwords via the
            # web interface.
            return False
        return False

    def dispatch(self, request, *args, **kwargs):
        if not self.is_user_allowed():
            return self.handle_no_permission()
        self.object = self.get_object()
        return super().dispatch(request, *args, **kwargs)

    def get_form_class(self):
        criuser = self.get_object()
        if self.request.user != criuser and self.request.user.has_perm(
            "cri_models.update_criuser_password"
        ):
            return auth_forms.SetPasswordForm
        if hasattr(criuser, "criserviceaccount"):
            if self.request.user in criuser.criserviceaccount.get_managers():
                return auth_forms.SetPasswordForm
        return super().get_form_class()

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs["user"] = self.get_object()
        return kwargs

    def form_valid(self, form):
        response = super().form_valid(form)
        messages.success(self.request, "Your password has been changed.")
        return response

    def get_success_url(self):
        criuser = self.get_object()
        return reverse("criuser_password", kwargs={"username": criuser.username})


class CRIUserOIDCView(LoginRequiredMixin, CRIUserDetailView):
    template_name = "cri_frontend/criuser_oidc.html"

    def is_user_allowed(self):
        if self.request.user.has_perm("cri_models.manage_criuser_oidc"):
            return True
        user = self.get_object()
        return user == self.request.user

    def dispatch(self, request, *args, **kwargs):
        if not self.is_user_allowed():
            return self.handle_no_permission()
        self.object = self.get_object()
        return super().dispatch(request, *args, **kwargs)

    def get_consent_with_claims(self):
        scope_claims_cls = oidc_provider_settings.get(
            "OIDC_EXTRA_SCOPE_CLAIMS", import_str=True
        )
        return [
            (
                uc,
                sorted(set(uc.scope)),
                scope_claims_cls(uc).create_response_dic(),
            )
            for uc in oidc_provider_models.UserConsent.objects.filter(user=self.object)
        ]

    def get_clients(self):
        return cri_auth_models.OIDCClientExtension.get_clients_managed_by_user(
            self.object
        )

    def get_context_data(self, *args, **kwargs):  # pylint: disable=arguments-differ
        context = super().get_context_data(*args, **kwargs)
        context.update(
            {
                "criuser": self.object,
                "user_consent_list": self.get_consent_with_claims(),
                "clients": self.get_clients(),
            }
        )
        return context


class OIDCConsentDeleteView(LoginRequiredMixin, generic.DeleteView):
    model = oidc_provider_models.UserConsent
    http_method_names = (
        "post",
        "put",
        "patch",
        "delete",
        "head",
        "options",
        "trace",
    )

    def is_user_allowed(self):
        if self.request.user.has_perm("cri_models.manage_criuser_oidc"):
            return True
        user = self.request.user
        userconsent = self.get_object()
        return userconsent.user == user

    def dispatch(self, *args, **kwargs):  # pylint: disable=arguments-differ
        if not self.is_user_allowed():
            return self.handle_no_permission()
        self.object = self.get_object()
        return super().dispatch(*args, **kwargs)

    def get_success_url(self):
        return reverse("criuser_oidc", kwargs={"username": self.object.user.username})


class CRIUserSSHKeysView(
    LoginRequiredMixin,
    CRIUserDetailMixin,
    generic.CreateView,
    generic.detail.SingleObjectMixin,
):
    template_name = "cri_frontend/criuser_ssh_keys.html"
    model = cri_models.SSHPublicKey
    form_class = forms.SSHPublicKeyForm

    def get_context_data(self, *args, **kwargs):  # pylint: disable=arguments-differ
        context = super().get_context_data(*args, **kwargs)
        context["criuser"] = self.get_object()
        if not self.is_user_allowed():
            context["form"] = None
        return context

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs["user"] = self.get_object()
        return kwargs

    def get_success_url(self):
        return reverse(
            "criuser_ssh_keys", kwargs={"username": self.get_object().username}
        )

    def get_queryset(self):
        return cri_models.CRIUser.objects.all()

    def get(self, *args, **kwargs):  # pylint: disable=arguments-differ
        self.object = self.get_object()
        return super().get(*args, **kwargs)

    def post(self, *args, **kwargs):  # pylint: disable=arguments-differ
        if not self.is_user_allowed():
            return self.handle_no_permission()
        return super().post(*args, **kwargs)

    def is_user_allowed(self):
        if self.request.user.has_perm("cri_models.add_sshpublickey"):
            return True
        user = self.get_object()
        if user == self.request.user:
            return True
        if hasattr(user, "criserviceaccount"):
            if self.request.user in user.criserviceaccount.get_managers():
                return True
        return False


class CRIUserSSHKeysShortcutView(View):
    def get(self, *args, **kwargs):  # pylint: disable=unused-argument,arguments-differ
        username = kwargs["username"]
        keys = cri_models.SSHPublicKey.objects.filter(user__username=username)
        return HttpResponse(
            "\n".join([key.key.key_without_comment + f" {username}" for key in keys])
            + "\n",
            content_type="text/plain",
        )


class CRIUserTasksView(
    LoginRequiredMixin,
    CRIUserDetailMixin,
    generic.detail.SingleObjectMixin,
    generic.ListView,
):
    template_name = "cri_frontend/criuser_tasks.html"
    paginate_by = 100

    def is_user_allowed(self):
        if self.request.user.has_perm("cri_tasks.view_critask"):
            return True
        user = self.get_object()
        return user == self.request.user

    def dispatch(self, request, *args, **kwargs):
        if not self.is_user_allowed():
            return self.handle_no_permission()
        self.object = self.get_object()
        return super().dispatch(request, *args, **kwargs)

    def get_object(self, queryset=None):
        if not queryset:
            # pylint: disable=protected-access
            queryset = self.model._default_manager.all()
        return super().get_object(queryset)

    def get_queryset(self):
        return cri_tasks_models.CRITask.objects.filter(owner=self.get_object())


class CRIUserConnectionLogView(LoginRequiredMixin, CRIUserDetailView):
    template_name = "cri_frontend/criuser_connection_log.html"

    def is_user_allowed(self):
        if self.request.user.has_perm("cri_auth.view_connectionlogentry"):
            return True
        user = self.get_object()
        return user == self.request.user

    def dispatch(self, request, *args, **kwargs):
        if not self.is_user_allowed():
            return self.handle_no_permission()
        self.object = self.get_object()
        return super().dispatch(request, *args, **kwargs)

    def get_context_data(self, *args, **kwargs):  # pylint: disable=arguments-differ
        context = super().get_context_data(*args, **kwargs)
        context.update(
            {
                "logs": cri_auth_models.ConnectionLogEntry.objects.filter(
                    user=self.object
                )[:50]
            }
        )
        return context


class SSHKeyDeleteView(LoginRequiredMixin, generic.DeleteView):
    model = cri_models.SSHPublicKey
    http_method_names = (
        "post",
        "put",
        "patch",
        "delete",
        "head",
        "options",
        "trace",
    )

    def is_user_allowed(self):
        key = self.get_object()
        if self.request.user.has_perm("cri_models.add_sshpublickey"):
            return True
        if key.user == self.request.user:
            return True
        if hasattr(key.user, "criserviceaccount"):
            if self.request.user in key.user.criserviceaccount.get_managers():
                return True
        return False

    def dispatch(self, *args, **kwargs):  # pylint: disable=arguments-differ
        if not self.is_user_allowed():
            return self.handle_no_permission()
        return super().dispatch(*args, **kwargs)

    def get_success_url(self):
        key = self.get_object()
        return reverse("criuser_ssh_keys", kwargs={"username": key.user.username})


class CRIGroupView(LoginRequiredMixin, generic.DetailView):
    template_name = "cri_frontend/crigroup_detail.html"
    model = cri_models.CRIGroup

    def is_user_allowed(self):
        user = self.request.user
        if user.has_perm("cri_models.view_crigroup"):
            return True
        group = self.get_object()
        groups = [] if user.is_anonymous else user.get_groups()
        return not group.private or group in groups

    def dispatch(self, *args, **kwargs):  # pylint: disable=arguments-differ
        if not self.is_user_allowed():
            return self.handle_no_permission()
        return super().dispatch(*args, **kwargs)

    # pylint: disable=arguments-differ
    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)

        context["memberships"] = (
            self.get_object()
            .memberships.all()
            .order_by("-graduation_year", "user__username")
        )
        context["subgroups"] = self.get_object().children.all()
        context["parents"] = self.get_object().parents.all()
        if not self.request.user.has_perm("cri_models.view_crigroup"):
            context["subgroups"] = context["subgroups"].filter(
                Q(private=False)
                | Q(pk__in=self.request.user.get_groups().values_list("pk", flat=True))
            )
            context["parents"] = context["parents"].filter(
                Q(private=False)
                | Q(pk__in=self.request.user.get_groups().values_list("pk", flat=True))
            )
        return context


class SearchFormView(LoginRequiredMixin, generic.FormView):
    template_name = "cri_frontend/search.html"
    form_class = forms.SearchForm

    # pylint: disable=arguments-differ
    def get_context_data(self, *args, form=None, **kwargs):
        c = super().get_context_data(*args, **kwargs)

        groups = None
        users = []
        if form and form.is_valid():
            groups, users = form.search()
        if groups is None:
            groups = cri_models.CRIGroup.get_groups_user_can_see(self.request.user)

        c.update(
            {
                "omnisearch_value": form.cleaned_data.get("omnisearch", "")
                if form
                else "",
                "users": users,
                "groups": groups,
            }
        )
        return c

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs.update({"user": self.request.user})
        if self.request.method in ("POST", "PUT"):
            if "keep-selected" in self.request.POST:
                user_list = "\n".join(self.request.POST.getlist("user_list"))
                kwargs.update({"data": {"login_list": user_list}})
        return kwargs

    def form_valid(self, form):
        return self.render_to_response(self.get_context_data(form=form))


class ExportView(LoginRequiredMixin, generic.FormView):
    EXPORT_TYPE_MAP = {
        "regular": import_export.CRIUserResource,
        "moodle": import_export.CRIUserMoodleResource,
    }

    EXPORT_FORMAT_MAP = {
        fmt().get_title(): fmt() for fmt in base_formats.DEFAULT_FORMATS
    }

    form_class = forms.ExportForm

    def is_user_allowed(self):
        return self.request.user.has_perm("cri_models.export_data")

    def dispatch(self, *args, **kwargs):  # pylint: disable=arguments-differ
        if not self.is_user_allowed():
            return self.handle_no_permission()
        return super().dispatch(*args, **kwargs)

    def form_valid(self, form):
        users = get_user_model().objects.filter(
            pk__in=self.request.POST.getlist("user_list", [])
        )
        file_format = self.EXPORT_FORMAT_MAP[form.cleaned_data["export_format"]]
        Resource = self.EXPORT_TYPE_MAP[form.cleaned_data["export_type"]]
        data = Resource(request=self.request).export(queryset=users)
        export_data = file_format.export_data(data)
        response = HttpResponse(
            export_data, content_type=file_format.get_content_type()
        )
        filename = f"user_export.{file_format.get_extension()}"
        response["Content-Disposition"] = f'attachment; filename="{filename}"'
        return response


class UserlistAction(LoginRequiredMixin, generic.View):
    dispatch_map = {
        "keep-selected": SearchFormView,
        "export": ExportView,
        "search": SearchFormView,
        "cards_issue": cri_cards_views.CardIssueFormView,
    }

    def get_dispatch_map(self):
        return self.dispatch_map

    def get_view(self):
        for name, view in self.get_dispatch_map().items():
            if name in self.request.POST:
                return view
        raise SuspiciousOperation("no action found")

    def get(self, *args, **kwargs):
        return SearchFormView.as_view()(self.request, *args, **kwargs)

    def post(self, *args, **kwargs):
        view = self.get_view()
        return view.as_view()(self.request, *args, **kwargs)
