from django.contrib import admin

from . import models


@admin.register(models.Shortcut)
class ShortcutAdmin(admin.ModelAdmin):
    list_display = ("title", "url")
    list_didplay_links = list_display
    search_fields = ("title", "url", "description")
