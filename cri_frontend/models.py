from django.db import models


class Shortcut(models.Model):
    title = models.CharField(max_length=100)

    description = models.TextField(blank=True)

    url = models.URLField()

    class Meta:
        ordering = ("title",)

    def __str__(self):
        return self.title
