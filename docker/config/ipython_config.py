# Configuration file for ipython.

c.InteractiveShellApp.extensions = ["autoreload"]
c.InteractiveShellApp.exec_lines = [
    "%autoreload 2",
    "from cri_models.models import *",
]
