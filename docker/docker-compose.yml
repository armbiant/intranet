version: '3.2'
services:
    s3_dev:
        image: scality/s3server
        hostname: s3_dev
        restart: always
        environment:
            MINIO_ACCESS_KEY_FILE: s3-access
            MINIO_SECRET_KEY_FILE: s3-secret
            S3DATAPATH: /data
            S3METADATAPATH: /metadata
            ENDPOINT: s3-dev
        volumes:
            - s3_dev_data:/data
            - s3_dev_metadata:/metadata
        secrets:
            - s3-access
            - s3-secret
        ports:
            - $S3_PORT:8000

    db_dev:
        image: postgres:12-alpine
        hostname: db_dev
        restart: always
        shm_size: 256M
        environment:
            POSTGRES_USER: intranet_dev
            POSTGRES_DB: intranet_dev
            POSTGRES_PASSWORD_FILE: /run/secrets/postgres-passwd
        secrets:
            - postgres-passwd
        volumes:
            - db_dev_data:/var/lib/postgresql/data

    db_dev_adminer:
        image: adminer
        hostname: db_adminer_dev
        restart: always
        environment:
            ADMINER_DEFAULT_SERVER: db_dev
        ports:
            - $ADMINER_PORT:8080
        depends_on:
            - db_dev

    intranet_dev: &intranet_dev
        image: intranet_dev
        build:
            context: ../
            dockerfile: ./docker/Dockerfile
            args:
                DJANGO_UID: 1000
                DJANGO_GID: 1000
                BUILDKIT_INLINE_CACHE: 1
        restart: always
        environment:
            DEV: 1
            S3_ENDPOINT: "http://s3-dev:8000/"
            S3_BUCKET: cri-intranet
            S3_ACCESS_KEY_FILE: /run/secrets/s3-access
            S3_SECRET_KEY_FILE: /run/secrets/s3-secret
            S3_CUSTOM_DOMAIN: 'localhost:$S3_PORT/cri-intranet'
            S3_SECURE_URLS: 0
            DJANGO_DEBUG: 1
            DJANGO_LOG_LEVEL: info
            DJANGO_DEFAULT_ADMIN_PASSWORD_FILE: /run/secrets/kerberos-admin
            DJANGO_SECRET_KEY_FILE: /run/secrets/django-secret-key
            DJANGO_IGNORE_LOCAL_SETTINGS: 1
            DJANGO_DB_NAME: intranet_dev
            DJANGO_DB_USER: intranet_dev
            DJANGO_DB_HOST: db_dev
            DJANGO_DB_PASSWORD_FILE: /run/secrets/postgres-passwd
            DJANGO_LDAP_URI: 'ldap://ldap_dev/'
            DJANGO_LDAP_USER: 'cn=admin,dc=dev,dc=cri,dc=epita,dc=fr'
            DJANGO_LDAP_PASSWORD_FILE: /run/secrets/ldap-admin
            DJANGO_LDAP_BASE_DN: 'dc=dev,dc=cri,dc=epita,dc=fr'
            DJANGO_ALGOLIA_APP_ID: 8EMV73ZWET
            DJANGO_ALGOLIA_API_KEY_FILE: /run/secrets/algolia-api-key
            DJANGO_ALGOLIA_SEARCH_API_KEY_FILE: /run/secrets/algolia-search-api-key
            DJANGO_ALGOLIA_INDEX_SUFFIX: "DEV"
            AZUREAD_DOMAINS: epita.fr
            AZUREAD_OAUTH2_KEY: 1c1690db-5ab9-41eb-aab5-a8bed726c5f0
            AZUREAD_OAUTH2_SECRET_FILE: /run/secrets/azuread-oauth2-secret
            KRB5_KTNAME: /run/secrets/spnego-keytab
            KRB5_CONFIG: /app/intranet/docker/config/krb5-dev.conf
            SPNEGO_HOSTNAME: localhost
            KERBEROS_ADMIN_KEYTAB: /run/secrets/intranet-keytab
            KERBEROS_DEFAULT_REALM: DEV.CRI.EPITA.FR
            DJANGO_CACHE_URL: 'redis://redis_dev:6379/0'
            DJANGO_Q_THREADS: 1
        links:
            - s3_dev:s3-dev
        depends_on:
            - s3_dev
            - db_dev
            - ldap_dev
            - kadmin_dev
            - redis_dev
        volumes:
            - type: bind
              source: ../
              target: /app/intranet
            - type: bind
              source: ./volumes/ipython
              target: /app/.ipython
            - type: bind
              source: ./config/cors.json
              target: /app/cors.json
        secrets:
            - s3-access
            - s3-secret
            - postgres-passwd
            - spnego-keytab
            - intranet-keytab
            - django-secret-key
            - kerberos-admin
            - ldap-admin
            - azuread-oauth2-secret
            - algolia-api-key
            - algolia-search-api-key

    django_q_worker_dev:
        <<: *intranet_dev
        command: ./manage.py qcluster --verbosity 3

    redis_dev:
        image: redis
        hostname: redis_dev
        restart: always
        volumes:
            - redis:/data

    reverse_dev:
        image: nginx
        ports:
            - $REVERSE_PORT:80
        restart: always
        volumes:
            - type: bind
              source: ./config/nginx.conf
              target: /etc/nginx/nginx.conf
        depends_on:
            - intranet_dev
            - s3_dev

    kadmin_dev:
        image: 'registry.cri.epita.fr/cri/docker/kerberos:master'
        hostname: kadmin_dev
        restart: always
        env_file:
            - kerberos.env
        environment:
            KADMIN_LAUNCH: 1
            LDAP_SETUP: 1
            KRB5_ADMIN_PASSWORD_FILE: /run/secrets/kerberos-admin
        ports:
            - 464:464
            - 749:749
        depends_on:
            - ldap_dev
        secrets:
            - ldap-admin
            - kerberos-admin

    kdc_dev:
        image: 'registry.cri.epita.fr/cri/docker/kerberos:master'
        hostname: kdc_dev
        restart: always
        env_file:
            - kerberos.env
        environment:
            KADMIN_LAUNCH: 0
            LDAP_SETUP: 0
        ports:
            - 88:88
        depends_on:
            - ldap_dev
            - kadmin_dev
        secrets:
            - ldap-admin

    kerberos_service_init_dev:
        image: 'registry.cri.epita.fr/cri/docker/kerberos-service-init:master'
        hostname: kerberos_service_init_dev
        restart: "no"
        env_file:
            - kerberos.env
        environment:
            SERVICES: ldap spnego intranet
            LDAP_PRINCIPAL: host/ldap_sasl_dev ldap/localhost
            SPNEGO_PRINCIPAL: HTTP/localhost
            INTRANET_PRINCIPAL: intranet/admin
            KRB5_PRINCIPAL_PASSWORD_FILE: /run/secrets/kerberos-admin
        depends_on:
            - kadmin_dev
        secrets:
            - kerberos-admin
        volumes:
            - ./secrets/spnego.keytab:/container/keytabs/spnego.keytab
            - ./secrets/ldap.keytab:/container/keytabs/ldap.keytab
            - ./secrets/intranet.keytab:/container/keytabs/intranet.keytab

    ldap_sasl_dev:
        image: 'registry.cri.epita.fr/cri/docker/ldap:master'
        hostname: ldap_sasl_dev
        command: /usr/sbin/saslauthd -d -a kerberos5
        environment:
            LDAP_DOMAIN: dev.cri.epita.fr
            KRB5_KTNAME: /run/secrets/ldap-keytab
            KRB5_REALM: DEV.CRI.EPITA.FR
            KRB5_KDC: kdc_dev
            KRB5_TRACE: /dev/stderr
        volumes:
            - ldap_dev_sasl:/var/run/saslauthd
        secrets:
            - ldap-keytab

    ldap_dev:
        image: 'registry.cri.epita.fr/cri/docker/ldap/master'
        hostname: ldap_dev
        environment:
            LDAP_ADMIN_PASSWORD_FILE: /run/secrets/ldap-admin
            LDAP_CONFIG_PASSWORD_FILE: /run/secrets/ldap-config
            LDAP_ORGANISATION: CRI
            LDAP_BASE_DN: dc=dev,dc=cri,dc=epita,dc=fr
            LDAP_DOMAIN: dev.cri.epita.fr
            KRB5_KTNAME: /run/secrets/ldap-keytab
            LDAP_RFC2307BIS_SCHEMA: 'true'
            KRB5_REALM: DEV.CRI.EPITA.FR
            KRB5_KDC: kdc_dev
        depends_on:
            - ldap_sasl_dev
        volumes:
            - ldap_dev_config:/etc/ldap/slapd.d
            - ldap_dev_data:/var/lib/ldap
            - ldap_dev_sasl:/var/run/saslauthd
        secrets:
            - ldap-keytab
            - ldap-admin
            - ldap-config
        ports:
            - 389:389


volumes:
    s3_dev_data:
    s3_dev_metadata:
    db_dev_data:
    ldap_dev_config:
    ldap_dev_data:
    ldap_dev_sasl:
    redis:

secrets:
    s3-access:
       file: ./secrets/s3-access

    s3-secret:
       file: ./secrets/s3-secret

    postgres-passwd:
       file: ./secrets/postgres-passwd

    azuread-oauth2-secret:
        file: ./secrets/azuread-oauth2-secret

    algolia-search-api-key:
        file: ./secrets/algolia-search-api-key

    algolia-api-key:
        file: ./secrets/algolia-api-key

    django-secret-key:
       file: ./secrets/django-secret-key

    ldap-keytab:
       file: ./secrets/ldap.keytab

    spnego-keytab:
       file: ./secrets/spnego.keytab

    intranet-keytab:
       file: ./secrets/intranet.keytab

    ldap-admin:
        file: ./secrets/ldap-admin

    ldap-config:
        file: ./secrets/ldap-config

    kerberos-admin:
        file: ./secrets/kerberos-admin
