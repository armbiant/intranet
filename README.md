# CRI Intranet

## Developper setup

### Initial setup

You will need `docker` and `docker-compose` installed on your computer.

```sh
$ pipenv install --dev
$ cd docker/
$ ./gen_secrets.sh
$ docker-compose up --build
```

### Regular usage

```sh
$ cd docker/
$ DOCKER_BUILDKIT=1 COMPOSE_DOCKER_CLI_BUILD=1 docker-compose up --build
```

### Formatting

We use `python/black` as a code formatter. It can be enabled through
git pre-commit with the following command:

```sh
$ pipenv run pre-commit install
```

### Linting

We also use `pylint` to check the coding style. It is configured with
`gitlab-ci` and produce warnings in gitlab. To check if the project
matches `pylint`'s recommendations you can use the following command:

```sh
$ pipenv run prospector --profile base
```

### Services

#### Adminer
    URL: http://localhost:8010/
    System: 'PostgreSQL'
    User: 'intranet_dev'
    Password in: docker/secrets/postgres-passwd
    Database: 'intranet_dev'

#### Website
    URL: http://localhost:8000/
    Username: admin
    Password in: docker/secrets/kerberos-admin

### SPNEGO
If you want to test that SPNEGO is working properly on your machine you need
to get a ticket for a user that have a kerberos principal, for instance `admin`
has by default a relation to the principal `admin/admin`.

Then you can launch your favorite browser, configure it for SPNEGO support and
specify the location of the keytab used by SPNEGO.

#### Getting a ticket from kerberos

To get a kerberos ticket from admin/admin type the following command.
```sh
$ KRB5_CONFIG=docker/config/krb5.conf kinit admin/admin < docker/secrets/kerberos-admin
```

In general type the following command.
```sh
$ KRB5_CONFIG=docker/config/krb5.conf kinit MY_KERBEROS_USER
```

#### For chromium
Launch chromium with the following command.
```sh
$ KRB5_CONFIG=docker/config/krb5.conf KRB5_KTNAME="docker/secrets/spnego.keytab" chromium --auth-server-whitelist="localhost"
```

### For firefox
Launch firefox with the following command.
```sh
$ KRB5_CONFIG=docker/config/krb5.conf KRB5_KTNAME="docker/secrets/spnego.keytab" firefox
```

Go to `about:config` then change `network.negotiate-auth.trusted-uris` to
`localhost`.
