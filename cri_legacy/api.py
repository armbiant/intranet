from django.contrib.auth import get_user_model
from django.core.exceptions import PermissionDenied

from rest_framework import serializers, generics, permissions

from . import models

import ipaddress


class LegacySessionPingSerializer(serializers.ModelSerializer):
    login = serializers.CharField(source="username")

    class Meta:
        model = models.LegacySessionLogEntry
        fields = ("login", "image")


class LegacySessionPing(generics.CreateAPIView):
    ALLOWED_SUBNETS = [
        ipaddress.ip_network("127.0.0.0/8"),
        ipaddress.ip_network("192.168.0.0/16"),
        ipaddress.ip_network("10.0.0.0/8"),
        ipaddress.ip_network("163.5.0.0/16"),
    ]

    queryset = models.LegacySessionLogEntry.objects.all()
    serializer_class = LegacySessionPingSerializer
    permission_classes = [permissions.AllowAny]

    def perform_create(self, serializer):
        try:
            user = get_user_model().objects.get(
                username=serializer.validated_data.get("username")
            )
        except get_user_model().DoesNotExist:
            user = None
        ip = self.request.META.get("REMOTE_ADDR")
        if not ip:
            raise PermissionDenied
        ip = ipaddress.ip_address(ip)
        if not any(ip in subnet for subnet in self.ALLOWED_SUBNETS):
            raise PermissionDenied
        extra_data = {"ip": self.request.META.get("REMOTE_ADDR"), "user": user}
        serializer.save(**extra_data)
