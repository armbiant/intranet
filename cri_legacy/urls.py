from django.urls import include, path
from django.views.generic import TemplateView

from . import api


urlpatterns = [
    path(
        "oidc/.well-known/openid-configuration",
        TemplateView.as_view(
            content_type="application/json",
            template_name="cri_legacy/openid-configuration.json",
        ),
    ),
    path(
        "oidc/",
        include("oidc_provider.urls", namespace="oidc_provider_legacy"),
    ),
    path("api/presencesessions/ping/", api.LegacySessionPing.as_view()),
]
