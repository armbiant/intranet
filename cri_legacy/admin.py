from django.contrib import admin

from . import models


class LegacyGroupAdmin(admin.ModelAdmin):
    list_display = ("name",)
    list_display_links = list_display
    filter_horizontal = ("members",)
    search_fields = ("name",)


@admin.register(models.LegacyClassGroup)
class LegacyClassGroupAdmin(LegacyGroupAdmin):
    pass


@admin.register(models.LegacyRole)
class LegacyRoleAdmin(LegacyGroupAdmin):
    pass


@admin.register(models.LegacySessionLogEntry)
class LegacySessionLogEntryAdmin(admin.ModelAdmin):
    list_display = ("start_at", "end_at", "user", "username", "ip", "image")
    list_filter = ("image",)
    date_hierarchy = "start_at"
    search_fields = (
        "user__username",
        "user__first_name",
        "user__last_name",
        "user__email",
        "username",
        "ip",
        "image",
    )
