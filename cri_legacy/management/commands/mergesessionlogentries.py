from django.core.management.base import BaseCommand

from ... import models


class Command(BaseCommand):
    help = "Merge legacy session log entries"

    def handle(self, *args, **options):
        models.LegacySessionLogEntry.merge_sessions()
