from django.core.management.base import BaseCommand
from django.db.utils import IntegrityError, DataError
from django.conf import settings

from getpass import getpass
from sshpubkeys import SSHKey

from ... import models
from cri_models import models as cri_models

import datetime
import requests
import uuid
import csv


LEGACY_USERS_ENDPOINT = "https://cri.epita.fr/api/users"
SSH_KEYS_ENDPOINT = "https://cri.epita.fr/api/users/{login}/ssh_keys"


def parse_date(value):
    return datetime.date.fromisoformat(value)


class Command(BaseCommand):
    help = "Import legacy users"

    def load_cg_map(self):
        self.CG_MAP = {}
        with open(settings.LEGACY_CLASSGROUPS_MAP_FILE, "r") as f:
            cr = csv.DictReader(f)
            for data in cr:
                if "name" not in data or "promo" not in data:
                    raise ValueError("Invalid CSV")
                self.CG_MAP.setdefault(data["name"], [])
                for slug_field in [k for k in data.keys() if k.startswith("slug")]:
                    begin_at_field = "begin_at" + slug_field.replace("slug", "")
                    end_at_field = "end_at" + slug_field.replace("slug", "")
                    if begin_at_field not in data or end_at_field not in data:
                        raise ValueError("Invalid CSV")
                    if data[slug_field]:
                        self.CG_MAP[data["name"]].append(
                            (
                                cri_models.CRIGroup.objects.get(slug=data[slug_field]),
                                data["promo"],
                                parse_date(data[begin_at_field]),
                                parse_date(data[end_at_field]),
                            )
                        )

    def handle(self, *args, **options):
        self.load_cg_map()

        with requests.Session() as session:
            login = input("login: ")
            password = getpass("password: ")
            session.auth = (login, password)
            next_url = LEGACY_USERS_ENDPOINT
            max_count = "?"
            count = 0
            while next_url:
                r = session.get(next_url)
                if r.status_code != 200:
                    raise RuntimeError(repr(r))
                data = r.json()
                next_url = data.get("next")
                max_count = data.get("count", "?")
                for user_data in data.get("results", []):
                    count += 1
                    try:
                        self.import_user(user_data, session)
                    except Exception as e:
                        print(f"ERROR: {user_data}: {e}")
                print(f"Importing... {count}/{max_count}")

        for group in cri_models.CRIGroup.objects.all():
            group.update_computed_entries()

    def import_user(self, user_data, session):
        login = user_data.get("login")
        if not login:
            raise ValueError("missing login")

        GID_MAP = {
            5000: "wheel",
            6001: "users",
            7000: "administratives",
            10016: "students",
            10017: "students",
            10018: "students",
            10019: "students",
            10020: "students",
            10021: "students",
            10022: "students",
            10023: "students",
            10024: "students",
            10025: "students",
            9000: "users",
            8000: "users",
            6000: "teachers",
            8001: "users",
        }

        FIELD_MAP = {
            "first_name": "firstname",
            "last_name": "lastname",
            "legal_first_name": "firstname",
            "legal_last_name": "lastname",
            "phone": "telephone",
            "email": "mail",
            "uid": "uidNumber",
            "birthdate": "birthdate",
        }

        values = {
            field: user_data.get(legacy_field)
            for field, legacy_field in FIELD_MAP.items()
            if user_data.get(legacy_field) is not None
        }

        values["primary_group"] = cri_models.CRIGroup.objects.get(
            slug=GID_MAP.get(user_data.get("gidNumber"), "users")
        )

        user, _created = cri_models.CRIUser.objects.update_or_create(
            username=login, defaults=values
        )

        primary = user.username.replace(".", "_")
        (principal, _created,) = cri_models.KerberosPrincipal.objects.get_or_create(
            user=user,
            principal=f"{primary}@CRI.EPITA.NET",
            defaults={"out_of_date": False},
        )
        if not user.primary_principal:
            user.primary_principal = principal
            user.save()

        for cg_name in user_data.get("class_groups", []):
            cg, _created = models.LegacyClassGroup.objects.get_or_create(name=cg_name)
            cg.members.add(user)
            self.add_to_group(user, cg_name)

        self.import_ssh_keys(user, session)

    def add_to_group(self, user, cg_name):
        if cg_name not in self.CG_MAP:
            raise ValueError(f"unknown class group: {cg_name}")
        memberships = []
        for group, graduation_year, begin_at, end_at in self.CG_MAP[cg_name]:
            memberships.append(
                cri_models.CRIMembership(
                    user=user,
                    group=group,
                    graduation_year=graduation_year,
                    begin_at=begin_at,
                    end_at=end_at,
                )
            )
        cri_models.CRIMembership.objects.bulk_create(memberships)

    def import_ssh_keys(self, user, session):
        r = session.get(SSH_KEYS_ENDPOINT.format(login=user.username))
        if r.status_code != 200:
            raise RuntimeError(repr(r))
        for key in map(SSHKey, r.json().get("keys", [])):
            try:
                cri_models.SSHPublicKey.objects.update_or_create(
                    key=key,
                    defaults={
                        "user": user,
                        "key_type": key.key_type,
                        "key_length": key.bits,
                    },
                )
            except DataError:
                title = f"title truncated: {key.comment}"[:128]
                cri_models.SSHPublicKey.objects.update_or_create(
                    key=key,
                    defaults={
                        "user": user,
                        "key_type": key.key_type,
                        "key_length": key.bits,
                        "title": title,
                    },
                )
            except IntegrityError:
                title = f"duplicate title {uuid.uuid4()}: {key.comment}"[:128]
                cri_models.SSHPublicKey.objects.update_or_create(
                    key=key,
                    defaults={
                        "user": user,
                        "key_type": key.key_type,
                        "key_length": key.bits,
                        "title": title,
                    },
                )
