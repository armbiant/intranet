from django.db import models
from django.utils import timezone
from django.contrib.auth import get_user_model

from datetime import timedelta


class AbstractLegacyGroup(models.Model):
    name = models.SlugField(unique=True)

    members = models.ManyToManyField(get_user_model(), blank=True)

    class Meta:
        abstract = True
        ordering = ("name",)

    def __str__(self):
        return self.name


class LegacyClassGroup(AbstractLegacyGroup):
    pass


class LegacyRole(AbstractLegacyGroup):
    pass


class LegacySessionLogEntry(models.Model):
    SESSION_TIMEOUT_SECONDS = 3 * 60

    user = models.ForeignKey(
        get_user_model(), blank=True, null=True, on_delete=models.SET_NULL
    )

    username = models.CharField(max_length=150, blank=True, editable=False)

    image = models.CharField(max_length=100, blank=True)

    start_at = models.DateTimeField(default=timezone.now)

    end_at = models.DateTimeField(blank=True, null=True)

    ip = models.GenericIPAddressField()

    class Meta:
        ordering = ("-start_at", "username")
        verbose_name_plural = "Legacy session log entries"

    def __str__(self):
        return f"{self.username} - {self.start_at} - {self.ip}/{self.image}"

    def get_username(self):
        return self.user.username if self.user else self.username

    # pylint: disable=arguments-differ
    def save(self, *args, **kwargs):
        if not self.username and self.user:
            self.username = self.user.username
        return super().save(*args, **kwargs)

    def same_session(self, entry):
        date = self.end_at or self.start_at
        conditions = (
            self.get_username() == entry.get_username(),
            self.image == entry.image < self.ip == entry.ip,
            date + timedelta(seconds=self.SESSION_TIMEOUT_SECONDS) < entry.start_at,
        )
        return all(conditions)

    @classmethod
    def merge_entries(cls, entries):
        timeout_delta = timedelta(seconds=cls.SESSION_TIMEOUT_SECONDS)
        if entries[-1].end_at:
            entries[0].end_at = entries[-1].end_at
        else:
            entries[0].end_at = entries[-1].start_at + timeout_delta
        entries[0].save()
        cls.objects.filter(pk__in=[e.pk for e in entries[1:]]).delete()

    @classmethod
    def merge_sessions(cls):
        timeout_delta = timedelta(seconds=cls.SESSION_TIMEOUT_SECONDS)
        qs = cls.objects.exclude(
            end_at__lt=timezone.now() - timeout_delta * 2
        ).order_by("ip", "image", "user", "username", "start_at")

        last_entries = []
        for entry in qs:
            if last_entries and not last_entries[-1].same_session(entry):
                cls.merge_entries(last_entries)
                last_entries = []
            last_entries.append(entry)
