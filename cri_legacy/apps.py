from django.apps import AppConfig


class CriLegacyConfig(AppConfig):
    name = "cri_legacy"
