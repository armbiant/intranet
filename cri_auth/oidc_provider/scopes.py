from django.utils import timezone

from .base import CRIScope, CustomScopeClaims

from cri_models import models as cri_models
from cri_legacy import models as cri_legacy_models
from .. import models as cri_auth_models


class ProfileScope(CRIScope):
    scope = "profile"
    name = "Basic profile"
    desc = "Access to your basic information. Includes names and other information."

    def get_claims(self):
        return {
            "name": self.user.get_full_name(),
            "given_name": self.user.first_name,
            "family_name": self.user.last_name,
            "preferred_username": self.user.username,
            # TODO 'picture': ,
            "zoneinfo": timezone.get_current_timezone_name(),
        }


class EmailScope(CRIScope):
    scope = "email"
    name = "Email"
    desc = "Access to your EPITA email address."

    def get_claims(self):
        return {
            "email": self.user.email,
            "email_verified": bool(self.user.email),
        }


class PhoneScope(CRIScope):
    scope = "phone"
    name = "Phone number"
    desc = "Access to your phone number."

    def get_claims(self):
        return {
            "phone_number": str(self.user.phone or ""),
            "phone_number_verified": False,
        }


class RelatedAccountsScope(CRIScope):
    scope = "related_accounts"
    name = "Related accounts"
    desc = "Access to information about your old or new accounts."

    def get_claims(self):
        new_account = self.user.get_new_account()
        return {
            "old_logins": [u.username for u in self.user.get_old_accounts()],
            "new_login": new_account.username if new_account else None,
        }


class EPITAScope(CRIScope):
    scope = "epita"
    name = "EPITA"
    desc = (
        "Access to your EPITA-specific information. Include groups, UID, "
        "campuses and other information."
    )

    GROUP_FIELDS = ("slug", "gid", "name", "kind", "private")

    def _get_groups(self):
        client_extension = cri_auth_models.OIDCClientExtension.from_client(self.client)
        return client_extension.get_user_groups(self.user)

    def get_claims(self):
        groups = self._get_groups()
        graduation_years = list(
            cri_models.CRICurrentComputedMembership.objects.filter(
                user=self.user, group__in=groups, graduation_year__isnull=False
            )
            .distinct("graduation_year")
            .order_by("graduation_year")
            .values_list("graduation_year", flat=True)
        )
        if cri_auth_models.OIDCClientExtension.from_client(self.client).is_legacy:
            return {
                "login": self.user.username,
                "promo": max(graduation_years) if graduation_years else None,
                "uid_number": self.user.uid,
                "roles": list(
                    cri_legacy_models.LegacyRole.objects.filter(
                        members=self.user
                    ).values_list("name", flat=True)
                ),
                "class_groups": list(
                    cri_legacy_models.LegacyClassGroup.objects.filter(
                        members=self.user
                    ).values_list("name", flat=True)
                ),
            }
        return {
            "uid": self.user.uid,
            "gid": self.user.primary_group.gid,
            "groups": list(groups.values(*self.GROUP_FIELDS)),
            "campuses": list(
                groups.filter(kind="campus").values_list("slug", flat=True)
            ),
            "graduation_years": graduation_years,
        }


class RolesScope(CRIScope):
    scope = "roles"
    name = "Roles"
    desc = "Access to the roles this client assigned to you"

    def _get_roles(self):
        client_extension = cri_auth_models.OIDCClientExtension.from_client(self.client)
        return client_extension.get_user_roles(self.user)

    def get_claims(self):
        return {"roles": list(self._get_roles())}


class PictureScope(CRIScope):
    scope = "picture"
    name = "Picture"
    desc = "Access to your picture"

    BASE_URL = "https://photos.cri.epita.fr"

    def get_claims(self):
        return {
            "picture": f"{self.BASE_URL}/{self.user.username}",
            "picture_square": f"{self.BASE_URL}/square/{self.user.username}",
            "picture_thumb": f"{self.BASE_URL}/thumb/{self.user.username}",
        }


class BirthdateScope(CRIScope):
    scope = "birthdate"
    name = "Birthdate"
    desc = "Access to your birthdate"

    def get_claims(self):
        return {"birthdate": str(self.user.birthdate or "")}


class LegalIdentityScope(CRIScope):
    scope = "legal_identity"
    name = "Legal identity"
    desc = "Access to your identity as used on legal documents."

    def get_claims(self):
        return {
            "legal_first_name": self.user.legal_first_name,
            "legal_last_name": self.user.legal_last_name,
        }


class CRIScopeClaims(CustomScopeClaims):
    scope_classes = (
        ProfileScope,
        PictureScope,
        EmailScope,
        PhoneScope,
        RelatedAccountsScope,
        EPITAScope,
        RolesScope,
        BirthdateScope,
        LegalIdentityScope,
    )
