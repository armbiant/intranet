from django.core.exceptions import PermissionDenied

from .scopes import CRIScopeClaims
from .. import models as cri_auth_models

__all__ = ("CRIScopeClaims", "criuser_sub_generator", "after_userlogin_hook")


def criuser_sub_generator(user):
    return user.username


def after_userlogin_hook(request, user, client):
    client_extension = cri_auth_models.OIDCClientExtension.from_client(client)
    log_entry = cri_auth_models.ConnectionLogEntry(
        user=user,
        client=client,
        kind="oidc_client",
        ip=request.META.get("REMOTE_ADDR"),
        status="failure",
    )
    try:
        if not client_extension.is_user_allowed(user):
            raise PermissionDenied()
        log_entry.status = "success"
    finally:
        log_entry.save()
