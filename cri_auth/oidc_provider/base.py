from oidc_provider.lib.claims import ScopeClaims


class CRIScope:
    def __init__(self, client, user):
        self.client = client
        self.user = user

    def get_claims(self):
        return {}

    def __str__(self):
        return self.name


class CustomScopeClaims(ScopeClaims):
    scope_classes = tuple()

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.scope_classes = tuple(
            [
                s
                for s in type(self).scope_classes
                if s.scope in self.client.scope or not self.client.scope
            ]
        )

    def create_response_dic(self):
        dic = {}
        denied = set()
        unknown = set()
        scope_map = {s.scope: s for s in type(self).scope_classes}
        for scope in set(self.scopes) - set(["openid"]):
            scope_cls = scope_map.get(scope)
            if not scope_cls:
                unknown.add(scope)
            elif scope_cls not in self.scope_classes:
                denied.add(scope)
            else:
                dic.update(scope_cls(self.client, self.user).get_claims())

        if denied:
            dic["denied_scopes"] = list(denied)

        if unknown:
            dic["unknown_scopes"] = list(unknown)

        return dic

    def _scopes_registered(self):
        return [getattr(c, "scope") for c in self.scope_classes]

    @classmethod
    def get_scopes_info(cls, scopes=None):
        if scopes is None:
            scopes = []
        scopes_info = []

        for scope_cls in cls.scope_classes:
            if scope_cls.scope not in scopes:
                continue
            scopes_info.append(
                {
                    "scope": scope_cls.scope,
                    "name": scope_cls.name,
                    "description": scope_cls.desc,
                }
            )

        return scopes_info
