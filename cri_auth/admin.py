from django.contrib import admin

from . import models


class AuthGroupInline(admin.TabularInline):
    model = models.AuthGroup
    extra = 1
    autocomplete_fields = ("group",)


class OIDCClientExtensionInline(admin.TabularInline):
    model = models.OIDCClientExtension
    extra = 1
    autocomplete_fields = ("client",)


@admin.register(models.AuthGroupsClientsMapping)
class AuthGroupsClientsMappingAdmin(admin.ModelAdmin):
    list_display = (
        "name",
        "is_restricted",
    )
    list_display_links = list_display
    list_filter = ("is_restricted",)
    filter_horizontal = ("managers",)
    inlines = (
        AuthGroupInline,
        OIDCClientExtensionInline,
    )


@admin.register(models.OIDCClientExtension)
class OIDCClientExtensionAdmin(admin.ModelAdmin):
    list_display = ("client", "slug", "is_legacy")
    list_display_links = list_display
    list_filter = ("is_legacy",)
    search_fields = (
        "client__name",
        "client___redirect_uris",
        "client__client_id",
        "slug",
    )
    autocomplete_fields = ("client",)
    readonly_fields = ("groups_clients_mapping",)

    # pylint: disable=arguments-differ
    def has_add_permission(self, *_args, **_kwargs):
        return False


@admin.register(models.ConnectionLogEntry)
class ConnectionLogEntryAdmin(admin.ModelAdmin):
    list_display = ("created_at", "username", "ip", "kind", "status", "client")
    list_display_links = list_display
    list_filter = ("kind", "status", "client")
    date_hierarchy = "created_at"
    readonly_fields = list_display
    exclude = ("user",)
    search_fields = (
        "username",
        "user__username",
        "user__first_name",
        "user__last_name",
        "user__email",
        "ip",
        "client__name",
    )

    # pylint: disable=arguments-differ
    def has_add_permission(self, *_args, **_kwargs):
        return False

    # pylint: disable=arguments-differ
    def has_change_permission(self, *_args, **_kwargs):
        return False

    # pylint: disable=arguments-differ
    def has_delete_permission(self, *_args, **_kwargs):
        return False
