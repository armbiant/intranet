from django.core.checks import Error, register
from django.conf import settings


@register()
def check_spnego_hostname(app_configs, **_kwargs):
    if app_configs and "cri_auth" not in [app.name for app in app_configs]:
        return []
    errors = []
    spnego_hostname = getattr(settings, "SPNEGO_HOSTNAME", "")
    if not spnego_hostname:
        errors.append(
            Error("SPNEGO_HOSTNAME setting must be defined", id="cri_auth.E001")
        )
    if not isinstance(spnego_hostname, str):
        errors.append(
            Error("SPNEGO_HOSTNAME setting must be a string", id="cri_auth.E002")
        )
    return errors
