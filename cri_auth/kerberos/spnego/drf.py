from rest_framework import authentication as drf_authentication

from .constants import NEGOTIATE


class SPNEGOAuthentication(drf_authentication.BaseAuthentication):
    def authenticate(self, request):
        auth = request.META.get("HTTP_AUTHORIZATION", "").split()
        if not auth or auth[0].lower() != NEGOTIATE.lower():
            return None
        user = getattr(request, "spnego_user", None)
        return (user, None)

    def authenticate_header(self, request):
        return NEGOTIATE
