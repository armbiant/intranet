from django.contrib.auth.backends import ModelBackend
from django.core.exceptions import SuspiciousOperation
from django.contrib.auth import get_user_model
from django.conf import settings

import logging

from .constants import NEGOTIATE, HTTP_AUTHORIZATION
from ..gssapi import KerberosGSSAuthServer
from cri_auth.backends import AuthBackendMixin

from cri_models.kerberos.utils import KerberosPrincipalStr


class SPNEGOBackend(AuthBackendMixin, ModelBackend):
    class SPNEGOIncompleteAuth(Exception):
        def __init_(self, auth_data):
            self.auth_data = auth_data

    def __init__(self, *args, **kwargs):
        self.logger = logging.getLogger(__name__)
        super().__init__(*args, **kwargs)

    def user_from_principal(self, kerberos_principal):
        self.logger.debug("Looking up user with principal '%s'", kerberos_principal)
        krb_principal_data = KerberosPrincipalStr(kerberos_principal)
        try:
            user = get_user_model().objects.get(
                kerberosprincipal__principal=str(krb_principal_data)
            )
        except get_user_model().DoesNotExist:
            self.logger.debug("User with principal '%s' not found", kerberos_principal)
            return None

        self.logger.debug("Found user '%s'", user)
        return user

    def _gssapi_auth(self, request, authstr):
        service = f"HTTP@{settings.SPNEGO_HOSTNAME}"
        self.logger.debug("using service name %s", service)
        self.logger.debug("Negotiate authstr %r", authstr)

        with KerberosGSSAuthServer(service) as krbgssauth:
            result, response = krbgssauth.step(authstr)
            if result == KerberosGSSAuthServer.AUTH_GSS_CONTINUE:
                raise SPNEGOBackend.SPNEGOIncompleteAuth(auth_data=response)
            principal = krbgssauth.get_username()

        user = self.user_from_principal(principal)
        if user:
            user.auth_data = response
            user.auth_principal = principal
        return user

    def authenticate(self, request):  # pylint:disable=arguments-differ
        authorization_headers = request.headers.get(HTTP_AUTHORIZATION, "").split(",")

        for authorization in authorization_headers:
            auth_tuple = authorization.split(" ", 1)
            if not auth_tuple or auth_tuple[0] != NEGOTIATE:
                continue
            if len(auth_tuple) != 2:
                raise SuspiciousOperation("Malformed authorization header")
            user = self._gssapi_auth(request, auth_tuple[1])
            if user and self.user_can_authenticate(user):
                return user
        return None
