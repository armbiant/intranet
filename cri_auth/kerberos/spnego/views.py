from django.shortcuts import redirect

from urllib.parse import urlencode

from .constants import SPNEGO_REQUEST_STATUS, WWW_AUTHENTICATE, NEGOTIATE


class SPNEGOMixin:
    spnego_field_name = "spnego"

    def get_spnego_field_name(self):
        return self.spnego_field_name

    def get_spnego_disabled_url(self):
        params = self.request.GET.copy()
        params[self.get_spnego_field_name()] = 0
        return f"{self.request.path}?{urlencode(params)}"

    def is_spnego_enabled(self):
        return self.request.GET.get(self.get_spnego_field_name(), "1") != "0"

    def dispatch(self, request, *args, **kwargs):
        if self.is_spnego_enabled() and request.spnego_error:
            return redirect(self.get_spnego_disabled_url())

        response = super().dispatch(request, *args, **kwargs)

        if self.is_spnego_enabled() and not request.spnego_user:
            response[WWW_AUTHENTICATE] = NEGOTIATE
            response.status_code = SPNEGO_REQUEST_STATUS

        return response
