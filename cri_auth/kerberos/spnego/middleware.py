from django.utils.deprecation import MiddlewareMixin
from django.http import HttpResponse
from django.contrib.auth import authenticate

from .constants import (
    WWW_AUTHENTICATE,
    HTTP_AUTHORIZATION,
    NEGOTIATE,
    SPNEGO_REQUEST_STATUS,
)
from .backend import SPNEGOBackend
from .. import gssapi


class SPNEGOMiddleware(MiddlewareMixin):
    def process_exception(self, request, exception):
        if isinstance(exception, SPNEGOBackend.SPNEGOIncompleteAuth):
            response = HttpResponse("Unauthorized", status=SPNEGO_REQUEST_STATUS)
            response[WWW_AUTHENTICATE] = f"{NEGOTIATE} {exception.auth_data}"
            return response
        return None

    def process_request(self, request):
        request.spnego_user = None
        request.spnego_error = None
        authorization_headers = request.headers.get(HTTP_AUTHORIZATION, "")
        if NEGOTIATE.lower() in authorization_headers.lower():
            try:
                user = authenticate(request)
                if hasattr(user, "auth_data"):
                    request.spnego_user = user
            except gssapi.GSSAuthError as e:
                request.spnego_error = e

    def process_response(self, request, response):
        if request.spnego_user:
            response[WWW_AUTHENTICATE] = f"{NEGOTIATE} {request.spnego_user.auth_data}"
        return response
