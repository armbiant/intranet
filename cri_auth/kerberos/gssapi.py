import kerberos
import logging

_logger = logging.getLogger(__name__)


class GSSAuthError(Exception):
    def __init__(self, message):
        self.message = message

    def __str__(self):
        return self.message


class GSSAuthInitError(GSSAuthError):
    pass


class GSSAuthStepError(GSSAuthError):
    pass


class GSSAuthResponseError(GSSAuthStepError):
    pass


class GSSAuthUsernameError(GSSAuthError):
    pass


class KerberosRaiseContext:
    def __init__(self, exception_type):
        self.exception_type = exception_type

    def __enter__(self):
        return self

    def __exit__(self, exception_type, exception_value, traceback):
        if isinstance(exception_value, kerberos.KrbError):
            _logger.error("GSSAPI: %s", exception_value)
            raise self.exception_type(str(exception_value))


class KerberosGSSAuth:
    AUTH_GSS_CONTINUE = kerberos.AUTH_GSS_CONTINUE
    AUTH_GSS_COMPLETE = kerberos.AUTH_GSS_COMPLETE

    def __init__(self, service):
        self.service = service
        self.context = None

    @staticmethod
    def _init(service):
        raise NotImplementedError

    @staticmethod
    def _step(context, challenge):
        raise NotImplementedError

    @staticmethod
    def _get_response(context):
        raise NotImplementedError

    @staticmethod
    def _get_username(context):
        raise NotImplementedError

    @staticmethod
    def _clean(context):
        raise NotImplementedError

    def __enter__(self):
        _logger.debug("GSSAPI: initializing context (service=%s)", self.service)
        with KerberosRaiseContext(GSSAuthInitError):
            result, context = self._init(self.service)
        if result == self.AUTH_GSS_COMPLETE:
            self.context = context
        return self

    def step(self, challenge):
        _logger.debug("GSSAPI: auth step (challenge=%s)", challenge)
        with KerberosRaiseContext(GSSAuthStepError):
            result = self._step(self.context, challenge)
        with KerberosRaiseContext(GSSAuthResponseError):
            response = self._get_response(self.context)
        _logger.debug("GSSAPI: auth result=%s response=%s", result, response)
        return result, response

    def get_username(self):
        _logger.debug("GSSAPI: getting username")
        with KerberosRaiseContext(GSSAuthUsernameError):
            username = self._get_username(self.context)
        _logger.debug("GSSAPI: got username=%s", username)
        return username

    def __exit__(self, exception_type, exception_value, traceback):
        if self.context:
            _logger.debug("GSSAPI: cleaning context")
            self._clean(self.context)
            self.context = None


class KerberosGSSAuthClient(KerberosGSSAuth):
    _init = staticmethod(kerberos.authGSSClientInit)
    _step = staticmethod(kerberos.authGSSClientStep)
    _get_response = staticmethod(kerberos.authGSSClientResponse)
    _get_username = staticmethod(kerberos.authGSSClientUserName)
    _clean = staticmethod(kerberos.authGSSClientClean)


class KerberosGSSAuthServer(KerberosGSSAuth):
    _init = staticmethod(kerberos.authGSSServerInit)
    _step = staticmethod(kerberos.authGSSServerStep)
    _get_response = staticmethod(kerberos.authGSSServerResponse)
    _get_username = staticmethod(kerberos.authGSSServerUserName)
    _clean = staticmethod(kerberos.authGSSServerClean)
