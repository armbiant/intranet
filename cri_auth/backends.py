from django.contrib.auth import get_user_model, backends as auth_backends

import logging

_logger = logging.getLogger(__name__)


class AuthBackendMixin:
    def get_user(self, user_id):
        User = get_user_model()
        try:
            # pylint: disable=protected-access
            user = User._default_manager.get(pk=user_id)
        except User.DoesNotExist:
            _logger.error("user %s does not exist anymore", user_id)
            return None
        if not self.user_can_authenticate(user):
            _logger.debug(
                "Unable to authenticate '%s': user is not allowed.", user.username
            )
            return None
        return user

    def user_can_authenticate(self, user):
        return user.is_auth_allowed()


class CRIModelBackend(AuthBackendMixin, auth_backends.ModelBackend):
    pass
