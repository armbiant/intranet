from django.contrib import messages
from django.contrib.auth import (
    views as auth_views,
    forms as auth_forms,
    login,
    logout,
    BACKEND_SESSION_KEY,
)
from django.urls import reverse_lazy
from django.shortcuts import redirect

from cri_auth.kerberos.spnego import SPNEGOMixin
from . import forms


class CRILoginView(SPNEGOMixin, auth_views.LoginView):
    template_name = "cri_auth/login.html"
    form_class = forms.LoginForm

    def is_spnego_enabled(self):
        return super().is_spnego_enabled() and not self.request.user.is_authenticated

    def is_user_allowed_to_login(self, user):
        if user.kerberosprincipal_set.all().filter(out_of_date=True).exists():
            return (
                "One of your Kerberos principal is out of sync, you must sign-in with "
                "your login and password.",
            )

        if hasattr(user, "criserviceaccount"):
            return "This account is a service account, it is not allowed to sign-in."

        return None

    def spnego_login(self, user):
        message = self.is_user_allowed_to_login(user)
        if message is not None:
            messages.error(self.request, message)
            return False

        login(self.request, user)
        messages.success(
            self.request,
            f'You have been logged in with user {self.request.spnego_user} via \
              Kerberos SPNEGO. If you want to sign in without SPNEGO \
              <a class="alert-link" \
              href="{self.get_spnego_disabled_url()}">click here</a>.',
        )
        return True

    def get(self, request, *args, **kwargs):
        if not super().is_spnego_enabled():
            logout(request)
        elif not request.user.is_authenticated and request.spnego_user:
            if self.spnego_login(request.spnego_user):
                return redirect(self.get_success_url())
        return super().get(request, *args, **kwargs)


class CRILogoutView(auth_views.LogoutView):
    pass


class CRIPasswordChangeView(auth_views.PasswordChangeView):
    BACKEND_FORM_CLASS_OVERRIDES = {
        "social_core.backends.azuread.AzureADOAuth2": auth_forms.SetPasswordForm
    }

    template_name = "cri_auth/password_change.html"

    def get_form_class(self):
        return self.BACKEND_FORM_CLASS_OVERRIDES.get(
            self.request.session.get(BACKEND_SESSION_KEY),
            super().get_form_class(),
        )

    def form_valid(self, form):
        response = super().form_valid(form)
        messages.success(self.request, "Your password has been changed.")
        return response


class CRIPasswordResetView(auth_views.PasswordResetView):
    template_name = "cri_auth/password_reset.html"
    form_class = forms.PasswordResetForm
    success_url = reverse_lazy("login")


class CRIPasswordResetConfirmView(auth_views.PasswordResetConfirmView):
    template_name = "cri_auth/password_change.html"
    success_url = reverse_lazy("index")
    post_reset_login = True
    post_reset_login_backend = "cri_auth.backends.CRIModelBackend"

    def form_valid(self, form):
        response = super().form_valid(form)
        messages.success(self.request, "Your password has been changed.")
        return response
