import datetime

from django.test import TestCase
from django.contrib.auth import get_user_model

from cri_models.models import CRIMembership, CRIGroup

from .. import models


class TestAuthGroupsClientsMapping(TestCase):
    fixtures = ("tests/groups.yaml",)

    @classmethod
    def setUpTestData(cls):
        User = get_user_model()
        user1 = User(username="test1", email="test1@example.org", uid=11)
        user1.save()
        users = CRIGroup.objects.get(slug="users")
        CRIMembership(user=user1, group=users, begin_at=datetime.date.today()).save()

    def test_auth_groups_clients_mapping_get_user_roles(self):
        user = get_user_model().objects.get(username="test1")
        users = CRIGroup.objects.get(slug="users")

        agcm = models.AuthGroupsClientsMapping()
        agcm.save()
        models.AuthGroup(
            groups_clients_mapping=agcm, group=users, _roles="users"
        ).save()

        user_roles = agcm.get_user_roles(user)

        self.assertEqual(user_roles, set(("users",)))

    def test_auth_groups_clients_mapping_get_user_groups(self):
        user = get_user_model().objects.get(username="test1")
        users = CRIGroup.objects.get(slug="users")

        admins = CRIGroup.objects.get(slug="wheel")
        CRIMembership(user=user, group=admins, begin_at=datetime.date.today()).save()

        agcm = models.AuthGroupsClientsMapping()
        agcm.save()
        models.AuthGroup(groups_clients_mapping=agcm, group=users).save()
        models.AuthGroup(groups_clients_mapping=agcm, group=admins).save()

        user_groups = agcm.get_user_groups(user)

        self.assertEqual(set(user_groups), set((users, admins)))

    def test_auth_groups_clients_mapping_is_user_allowed_not_restricted(self):
        user = get_user_model().objects.get(username="test1")

        agcm = models.AuthGroupsClientsMapping(is_restricted=False)
        agcm.save()

        is_user_allowed = agcm.is_user_allowed(user)

        self.assertTrue(is_user_allowed)

    def test_auth_groups_clients_mapping_is_user_allowed_restricted(self):
        user = get_user_model().objects.get(username="test1")

        agcm = models.AuthGroupsClientsMapping(is_restricted=True)
        agcm.save()

        is_user_allowed = agcm.is_user_allowed(user)

        self.assertFalse(is_user_allowed)

    def test_auth_groups_clients_mapping_is_user_allowed_group_allowed(self):
        user = get_user_model().objects.get(username="test1")
        users = CRIGroup.objects.get(slug="users")

        agcm = models.AuthGroupsClientsMapping(is_restricted=True)
        agcm.save()
        models.AuthGroup(
            groups_clients_mapping=agcm, group=users, group_is_allowed=True
        ).save()

        is_user_allowed = agcm.is_user_allowed(user)

        self.assertTrue(is_user_allowed)

    def test_auth_groups_clients_mapping_is_user_allowed_group_denied(self):
        user = get_user_model().objects.get(username="test1")
        users = CRIGroup.objects.get(slug="users")

        agcm = models.AuthGroupsClientsMapping(is_restricted=True)
        agcm.save()
        models.AuthGroup(
            groups_clients_mapping=agcm, group=users, group_is_allowed=False
        ).save()

        is_user_allowed = agcm.is_user_allowed(user)

        self.assertFalse(is_user_allowed)

    def test_auth_groups_clients_mapping_is_user_allowed_group_manager(self):
        user = get_user_model().objects.get(username="test1")
        admins = CRIGroup.objects.get(slug="wheel")
        admins.managers.add(user)

        agcm = models.AuthGroupsClientsMapping(is_restricted=True)
        agcm.save()
        models.AuthGroup(
            groups_clients_mapping=agcm, group=admins, group_is_allowed=True
        ).save()

        is_user_allowed = agcm.is_user_allowed(user)

        self.assertTrue(is_user_allowed)

    def test_auth_groups_clients_mapping_get_mappings_managed_by_user_manager(
        self,
    ):
        user = get_user_model().objects.get(username="test1")

        agcm = models.AuthGroupsClientsMapping()
        agcm.save()
        agcm.managers.add(user)

        mappings_managed_by_user = agcm.get_mappings_managed_by_user(user)

        self.assertEqual(set(mappings_managed_by_user), set((agcm,)))

    def test_auth_groups_clients_mapping_get_mappings_managed_by_user_group_manager(
        self,
    ):
        user = get_user_model().objects.get(username="test1")
        users = CRIGroup.objects.get(slug="users")
        users.managers.add(user)

        agcm = models.AuthGroupsClientsMapping()
        agcm.save()
        models.AuthGroup(
            groups_clients_mapping=agcm,
            group=users,
            group_is_allowed=True,
            group_managers_can_manage_clients=True,
        ).save()

        mappings_managed_by_user = agcm.get_mappings_managed_by_user(user)

        self.assertEqual(set(mappings_managed_by_user), set((agcm,)))
