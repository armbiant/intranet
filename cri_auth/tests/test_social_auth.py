from django.test import TestCase
from django.contrib.auth import get_user_model

from social_core.exceptions import AuthFailed, AuthForbidden

from .. import pipeline


class TestSocialAuth(TestCase):
    fixtures = ("tests/groups.yaml",)

    @classmethod
    def setUpTestData(cls):
        User = get_user_model()
        User(username="test-not-staff1", email="test@example.org", uid=11).save()
        User(username="test-not-staff2", email="test@example.org", uid=12).save()
        User(username="test-not-staff3", email="testok@example.org", uid=13).save()

    def test_social_auth_pipeline_associate_by_email_not_found(self):
        with self.assertRaises(AuthFailed):
            pipeline.associate_by_email(None, uid="invalid@example.org")

    def test_social_auth_pipeline_associate_by_email_multiple_found(self):
        with self.assertRaises(AuthFailed):
            pipeline.associate_by_email(None, uid="test@example.org")

    def test_social_auth_pipeline_associate_by_email(self):
        result = pipeline.associate_by_email(None, uid="testok@example.org")
        self.assertIn("user", result)
        self.assertEqual(result["user"].username, "test-not-staff3")

    def test_social_auth_pipeline_forbid_staff(self):
        user = get_user_model()(username="test-staff", is_staff=True)
        with self.assertRaises(AuthForbidden):
            pipeline.protect_staff_accounts(None, user=user)

    def test_social_auth_pipeline_allow_non_staff(self):
        user = get_user_model()(username="test-non-staff", is_staff=False)
        self.assertIs(pipeline.protect_staff_accounts(None, user=user), None)
