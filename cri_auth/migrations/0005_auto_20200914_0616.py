# Generated by Django 3.1.1 on 2020-09-14 04:16

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ("oidc_provider", "0026_client_multiple_response_types"),
        ("cri_auth", "0004_connectionlogentry"),
    ]

    operations = [
        migrations.AlterModelOptions(
            name="connectionlogentry",
            options={"verbose_name_plural": "Connection log entries"},
        ),
        migrations.AlterField(
            model_name="connectionlogentry",
            name="client",
            field=models.ForeignKey(
                blank=True,
                null=True,
                on_delete=django.db.models.deletion.SET_NULL,
                to="oidc_provider.client",
            ),
        ),
    ]
