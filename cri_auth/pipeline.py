from django.contrib.auth import get_user_model

from social_core.exceptions import AuthFailed, AuthForbidden

import logging


_logger = logging.getLogger(__name__)


class StaffAccountLoginIsForbidden(AuthForbidden):
    def __str__(self):
        return "Social authentication is disabled on staff accounts."


class ServiceAccountLoginIsForbidden(AuthForbidden):
    def __str__(self):
        return "Social authentication is disabled on staff accounts."


class OutOfDatePrincipal(AuthForbidden):
    def __str__(self):
        return (
            "Social auth failed: one of your Kerberos principal is out of "
            " sync, you must sign-in with your login and password."
        )


def associate_by_email(backend, uid, **_kwargs):
    User = get_user_model()

    try:
        user = User.objects.get(email=uid)
    except User.DoesNotExist:
        _logger.warning('User with email "%s" not found.', uid)
        raise AuthFailed(backend, f'User with email "{uid}" not found.')
    except User.MultipleObjectsReturned:
        _logger.error('Multiple users with email "%s" found.', uid)
        raise AuthFailed(backend, f'Multiple users with email "{uid}" found.')

    return {"user": user}


def protect_staff_accounts(backend, user=None, **_kwargs):
    if user and user.is_staff:
        _logger.warning(
            'Cannot use social authentication on a staff account "%s".', user
        )
        raise StaffAccountLoginIsForbidden(backend)


def forbid_service_accounts(backend, user=None, **_kwargs):
    if user and hasattr(user, "criserviceaccount"):
        _logger.warning(
            'Cannot use social authentication on a service account "%s".', user
        )
        raise ServiceAccountLoginIsForbidden(backend)


def force_password_login_on_unsynced_principal(backend, request, user=None, **_kwargs):
    if request.session.get("bypass_principal_check", False):
        # This is a ugly hack but there is no other way to know wether the user is
        # trying to sign-in or to reset its password.
        return
    if user and user.kerberosprincipal_set.all().filter(out_of_date=True).exists():
        if user.password and user.has_usable_password():
            _logger.warning('User "%s" has out of date principals.', user)
            raise OutOfDatePrincipal(backend)
