from django.urls import path
from . import views

urlpatterns = [
    path("stop", views.ImpersonateStopView.as_view(), name="impersonate_stop"),
    path("<str:username>", views.ImpersonateView.as_view(), name="impersonate"),
]
