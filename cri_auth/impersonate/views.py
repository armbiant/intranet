from django.contrib.auth import get_user_model
from django.contrib.auth.decorators import permission_required
from django.utils.decorators import method_decorator
from django.core.exceptions import PermissionDenied
from django.views.generic.base import RedirectView
from django.conf import settings
from django.urls import reverse_lazy
from django.shortcuts import get_object_or_404

from . import stop_impersonation, impersonate


class ImpersonateStopView(RedirectView):
    url = reverse_lazy(settings.LOGIN_REDIRECT_URL)

    def post(self, request, *args, **kwargs):
        stop_impersonation(request)
        return super().get(request, args, kwargs)


class ImpersonateView(RedirectView):
    url = reverse_lazy(settings.LOGIN_REDIRECT_URL)

    # pylint: disable=arguments-differ
    @method_decorator(
        permission_required("cri_models.impersonate", raise_exception=True)
    )
    def post(self, request, username, *args, **kwargs):
        user = get_object_or_404(get_user_model(), username=username)
        if user.is_superuser:
            raise PermissionDenied("You cannot impersonate a superuser")
        impersonate(request, user)
        return super().get(request, args, kwargs)
