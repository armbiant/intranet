from django.contrib.auth import get_user_model

from django_auth_ldap.backend import LDAPBackend, _LDAPUser

import logging

from .backends import AuthBackendMixin


_logger = logging.getLogger(__name__)


class NoDNError(ValueError):
    pass


class _CRILDAPUser(_LDAPUser):
    def _using_simple_bind_mode(self):
        return (
            getattr(get_user_model(), "LDAP_AUTH_DN_FIELD", False)
            or super()._using_simple_bind_mode()
        )

    def _construct_simple_user_dn(self):
        UserModel = get_user_model()
        ldap_dn_field = getattr(UserModel, "LDAP_AUTH_DN_FIELD", None)

        if not ldap_dn_field:
            return super()._construct_simple_user_dn()

        ldap_dn = getattr(
            UserModel.objects.get(**{UserModel.USERNAME_FIELD: self._username}),
            ldap_dn_field,
        )

        if not ldap_dn:
            raise NoDNError

        return ldap_dn


class CRILDAPBackend(AuthBackendMixin, LDAPBackend):
    # pylint: disable=arguments-differ
    def authenticate(self, request, username, password):
        if not username or not password:
            _logger.debug(
                "Unable to authenticate '%s': empty username or password.",
                username,
            )
            return None
        try:
            ldap_user = _CRILDAPUser(self, username=username, request=request)
            user = self.authenticate_ldap_user(ldap_user, password)
        except NoDNError:
            _logger.warning(
                "Unable to authenticate '%s': user has no associated LDAP DN.",
                username,
            )
            return None
        except get_user_model().DoesNotExist:
            _logger.debug("Unable to authenticate '%s': user not found.", username)
            return None
        if user and not self.user_can_authenticate(user):
            _logger.debug("Unable to authenticate '%s': user is not allowed.", username)
        return user

    def get_user(self, user_id):
        user = super().get_user(user_id)
        if user:
            _LDAPUser(self, user=user)  # This sets user.ldap_user
        return user


class CRILDAPBackendWithPasswordUpdate(CRILDAPBackend):
    def authenticate(self, request, username, password):
        user = super().authenticate(request, username, password)
        if user:
            user.set_password(password)
            user.save(raise_on_primary_error=False)
        return user
