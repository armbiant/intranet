import itertools

from django.db import models
from django.dispatch import receiver
from django.contrib.auth import (
    get_user_model,
    signals as auth_signals,
    BACKEND_SESSION_KEY,
)
from django.utils import timezone
from django.conf import settings

from oidc_provider.models import Client as OIDCClient

from cri_models import ldap, sync
from cri_models.models import CRIGroup

import uuid


class AuthGroup(models.Model):
    groups_clients_mapping = models.ForeignKey(
        "AuthGroupsClientsMapping", on_delete=models.CASCADE
    )

    group = models.ForeignKey(CRIGroup, on_delete=models.CASCADE)

    clients_can_see_private_subgroups = models.BooleanField(default=False)

    group_managers_can_manage_clients = models.BooleanField(default=False)

    group_is_allowed = models.BooleanField(default=True)

    _roles = models.CharField(max_length=150, blank=True, verbose_name="roles")

    class Meta:
        pass

    @property
    def roles(self):
        return self._roles.split()

    @roles.setter
    def roles(self, roles):
        self._roles = " ".join(roles)


class AuthGroupsClientsMapping(models.Model):
    name = models.CharField(max_length=100)

    managers = models.ManyToManyField(get_user_model(), blank=True)

    groups = models.ManyToManyField(CRIGroup, through=AuthGroup, blank=True)

    is_restricted = models.BooleanField(
        default=False,
        help_text=(
            "If enabled, only managers and explicitly allowed groups will be "
            "allowed to sign in with the clients"
        ),
    )

    class Meta:
        verbose_name = "Groups-Clients mapping"

    def __str__(self):
        return self.name

    def get_managers(self):
        managers = set(self.managers.all())
        for authgroup in self.authgroup_set.filter(
            group_managers_can_manage_clients=True
        ):
            managers |= set(authgroup.group.get_managers())
        return managers

    def get_users(self):
        users = set()
        for authgroup in self.authgroup_set.filter(group_is_allowed=True):
            users |= set(authgroup.group.get_members())
        return users

    def get_users_by_roles(self):
        roles = {}
        for authgroup in self.authgroup_set.all():
            users = set(authgroup.group.get_members())
            for role in authgroup.roles:
                roles.setdefault(role, set()).update(users)
        return roles

    def get_user_roles(self, user):
        ag_list = AuthGroup.objects.filter(
            groups_clients_mapping=self, group__in=user.get_groups()
        ).distinct()
        return set(itertools.chain.from_iterable([ag.roles for ag in ag_list]))

    def get_user_groups(self, user):
        prefix = "computed_parents__authgroup"
        return (
            user.get_groups()
            .filter(
                models.Q(private=False)
                | models.Q(authgroup__groups_clients_mapping=self)
                | models.Q(
                    **{
                        f"{prefix}__groups_clients_mapping": self,
                        f"{prefix}__clients_can_see_private_subgroups": True,
                    }
                )
            )
            .distinct()
        )

    def is_user_allowed(self, user):
        if not self.is_restricted:
            return True
        prefix = "memberships__group__authgroup"
        return (
            get_user_model()
            .objects.filter(
                models.Q(authgroupsclientsmapping=self)
                | models.Q(
                    models.Q(
                        models.Q(memberships__begin_at__lte=timezone.now())
                        | models.Q(memberships__begin_at__isnull=True),
                        models.Q(memberships__end_at__gte=timezone.now())
                        | models.Q(memberships__end_at__isnull=True),
                    ),
                    **{
                        f"{prefix}__groups_clients_mapping": self,
                        f"{prefix}__group_is_allowed": True,
                    },
                )
                | models.Q(
                    managed_groups__authgroupsclientsmapping=self,
                )
                | models.Q(
                    managed_groups__computed_children__authgroupsclientsmapping=self,
                ),
                pk=user.pk,
            )
            .exists()
        )

    @classmethod
    def get_mappings_managed_by_user(cls, user):
        if user.has_perm("cri_auth.edit_authgroupsclientsmapping"):
            return cls.objects.all()
        prefix = "authgroup"
        return cls.objects.filter(
            models.Q(managers=user)
            | models.Q(
                **{
                    f"{prefix}__group_managers_can_manage_clients": True,
                    f"{prefix}__group__managers": user,
                }
            )
            | models.Q(
                **{
                    f"{prefix}__group_managers_can_manage_clients": True,
                    f"{prefix}__group__computed_parents__managers": user,
                }
            )
        ).distinct()


class CRIServiceLDAPSyncAdapter(sync.SyncedLDAPModelAdapter):
    sync_remote_model = ldap.LDAPGroup.scoped(f"ou=services,{settings.LDAP_BASE_DN}")

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._roles = self.obj.get_users_by_roles()

    def ldap_dn_from_obj(self, role=None):
        if self.obj.slug:
            if not role:
                return self.sync_remote_model(name=self.obj.slug).build_dn()
            model = self.sync_remote_model.scoped(self.ldap_dn_from_obj())
            return model(name=f"{role}_{self.obj.slug}").build_dn()
        return None

    def get_sync_local_obj_id_multi(self):
        return [m[0] for m in self.get_sync_remote_obj_id_multi(None)]

    def get_sync_remote_obj_id_multi(self, _remote_objects):
        dn = self.ldap_dn_from_obj(role=None)
        yield ((self.obj.pk, None), dn)
        for role in self._roles:
            dn = self.ldap_dn_from_obj(role=role)
            yield ((self.obj.pk, role), dn)

    def get_sync_local_data_multi(self):
        dn = self.ldap_dn_from_obj(self.obj)
        managers = set(u.ldap_dn for u in self.obj.get_managers() if u.ldap_dn)

        yield (
            (self.obj.pk, None),
            {
                "managers": managers,
                "members": set(u.ldap_dn for u in self.obj.get_users() if u.ldap_dn),
                "name": self.obj.slug,
            },
        )

        for role, members in self.obj.get_users_by_roles().items():
            name = f"{role}_{self.obj.slug}"
            yield (
                (self.obj.pk, role),
                {
                    "dn": dn,
                    "managers": managers,
                    "members": set(u.ldap_dn for u in members if u.ldap_dn),
                    "name": name,
                    "description": role,
                },
            )

    @staticmethod
    def get_sync_remote_data(remote_obj):
        SYNC_FIELDS = ("name",)
        data = {
            "managers": set(remote_obj.managers),
            "members": set(remote_obj.members),
            "description": remote_obj.description,
            **{f: getattr(remote_obj, f) for f in SYNC_FIELDS},
        }
        return (remote_obj.dn, data)


class AuthMethodMixin(sync.SyncedModelMixin, models.Model):
    groups_clients_mapping = models.ForeignKey(
        AuthGroupsClientsMapping, on_delete=models.PROTECT
    )

    slug = models.SlugField(unique=True, blank=True, default=uuid.uuid4)

    class Meta:
        abstract = True

    class SyncMeta:
        sync_adapters = (CRIServiceLDAPSyncAdapter,)

    def get_users(self):
        return self.groups_clients_mapping.get_users()

    def get_managers(self):
        return self.groups_clients_mapping.get_managers()

    def get_users_by_roles(self):
        return self.groups_clients_mapping.get_users_by_roles()

    def get_user_roles(self, user):
        return self.groups_clients_mapping.get_user_roles(user)

    def get_user_groups(self, user):
        return self.groups_clients_mapping.get_user_groups(user)

    def is_user_allowed(self, user):
        return self.groups_clients_mapping.is_user_allowed(user)


class OIDCClientExtension(AuthMethodMixin):
    client = models.OneToOneField(OIDCClient, on_delete=models.CASCADE)

    is_legacy = models.BooleanField(
        default=False,
        help_text=(
            "If enabled, the claims returned by the epita scope will match the old "
            "OIDC API. Do not use for new OIDC clients."
        ),
    )

    class Meta:
        verbose_name = "OIDC client extension"

    def __str__(self):
        return self.client.name

    @classmethod
    def get_clients_managed_by_user(cls, user):
        if user.has_perm("oidc_provider.view_client"):
            return OIDCClient.objects.all()
        agcm_managed_by_user = AuthGroupsClientsMapping.get_mappings_managed_by_user(
            user
        )
        return OIDCClient.objects.filter(
            models.Q(owner=user)
            | models.Q(
                oidcclientextension__groups_clients_mapping__in=agcm_managed_by_user
            )
        ).distinct()

    @classmethod
    def from_client(cls, client):
        related_name = cls._meta.get_field("client").remote_field.get_accessor_name()
        if not hasattr(client, related_name):
            return cls.objects.create(
                client=client,
                AuthGroupsClientsMapping=AuthGroupsClientsMapping.objects.create(),
            )
        return getattr(client, related_name)


class ConnectionLogEntry(models.Model):
    KIND_CHOICES = (
        ("password", "Sign-in with password"),
        ("kerberos_spnego", "Sign-in with Kerberos (SPNEGO)"),
        ("ldap_password", "Sign-in with LDAP (password)"),
        ("office365_oauth2", "Sign-in with Office365 SSO"),
        ("oidc_client", "OIDC authentication"),
    )

    STATUS_CHOICES = (
        ("success", "Authentication succeeded"),
        ("failure", "Authentication failed"),
    )

    user = models.ForeignKey(get_user_model(), null=True, on_delete=models.SET_NULL)

    username = models.CharField(max_length=150, blank=True, editable=False)

    created_at = models.DateTimeField(auto_now_add=True, editable=False)

    kind = models.CharField(max_length=20, blank=True, choices=KIND_CHOICES)

    client = models.ForeignKey(
        OIDCClient, blank=True, null=True, on_delete=models.SET_NULL
    )

    ip = models.GenericIPAddressField(unpack_ipv4=True, blank=True, null=True)

    status = models.CharField(max_length=20, choices=STATUS_CHOICES)

    class Meta:
        verbose_name_plural = "Connection log entries"
        ordering = ("-created_at",)
        get_latest_by = "created_at"

    def __str__(self):
        return f"{self.created_at} - {self.username} ({self.kind})"

    # pylint: disable=arguments-differ
    def save(self, *args, **kwargs):
        if not self.username:
            self.username = self.user.username
        super().save(*args, **kwargs)


@receiver(
    auth_signals.user_login_failed,
    dispatch_uid="add_connection_log_entry_on_failure",
)
def add_connection_log_entry_on_failure(credentials, request, **_kwargs):
    username = credentials.get("username")
    if username:
        ConnectionLogEntry.objects.create(
            user=get_user_model().objects.filter(username=username).first(),
            username=username,
            kind="password",
            ip=request.META.get("REMOTE_ADDR"),
            status="failure",
        )


@receiver(
    auth_signals.user_logged_in,
    dispatch_uid="add_connection_log_entry_on_success",
)
def add_connection_log_entry_on_success(user, request, **_kwargs):
    NO_ENTRY = object()

    BACKEND_MAP = {
        "cri_auth.kerberos.spnego.SPNEGOBackend": "kerberos_spnego",
        "cri_auth.ldap.CRILDAPBackendWithPasswordUpdate": "ldap_password",
        "cri_auth.backends.CRIModelBackend": "password",
        "social_core.backends.azuread.AzureADOAuth2": "office365_oauth2",
        "cri_auth.impersonate.backend.ImpersonateBackend": NO_ENTRY,
    }

    kind = BACKEND_MAP.get(request.session.get(BACKEND_SESSION_KEY))
    if kind is not NO_ENTRY:
        ConnectionLogEntry.objects.create(
            user=user,
            kind=kind,
            ip=request.META.get("REMOTE_ADDR"),
            status="success",
        )
