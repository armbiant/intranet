from django import forms
from django.contrib.auth import get_user_model, forms as auth_forms


def _get_users_by_username_or_email(username_or_email):
    return get_user_model().objects.filter(
        username__exact=username_or_email, is_active=True
    ) or get_user_model().objects.filter(
        email__iexact=username_or_email, is_active=True
    )


class LoginForm(auth_forms.AuthenticationForm):
    username = forms.CharField(max_length=254, label="Login or email")

    error_messages = {
        **auth_forms.AuthenticationForm.error_messages,
        "multiple_users": "There are multiple users matching this email",
        "invalid_login": (
            "Please enter a correct %(username)s and password. Note that both fields "
            "are case-sensitive. Make sure you are entering your CRI password and not "
            "the password of you email account!"
        ),
        "service_account": (
            "This account is a service account, it is not allowed to sign-in"
        ),
    }

    def clean(self):
        username_or_email = self.cleaned_data.get("username")
        users = _get_users_by_username_or_email(username_or_email)

        if len(users) > 1:
            raise forms.ValidationError(
                self.error_messages["multiple_users"],
                code="multiple_users",
                params={"username": self.username_field.verbose_name},
            )

        if users:
            user = users[0]

            if hasattr(user, "criserviceaccount"):
                raise forms.ValidationError(
                    self.error_messages["service_account"],
                    code="service_account",
                    params={"username": self.username_field.verbose_name},
                )

            self.cleaned_data["username"] = user.username

        return super().clean()


class PasswordResetForm(auth_forms.PasswordResetForm):
    email = forms.CharField(max_length=254, label="Login or email")

    def get_users(self, email):
        return _get_users_by_username_or_email(email)
