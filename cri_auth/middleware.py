from social_django.middleware import SocialAuthExceptionMiddleware


class CRIAuthExceptionMiddleware(SocialAuthExceptionMiddleware):
    def raise_exception(self, request, exception):
        return False
